# Concepts

Let us first discuss some independent concepts that are important to programming in general. We will look at these concepts first to establish some fundamentals and to dispel some preconceptions and expectations. We need to be clear of these concepts for the rest of the text to make sense.

## Simplicity

In this document, I will describe things as _simple_. I intend to apply the meaning as stated below. It is a concise meaning that reasonably covers what one would intuitively call "simple" and is easy enough to explain. This is not an official definition for '_simple_'.

> simple = dedicated + reduced + unoptimized

_specialized_: an expression intended for our goal, and nothing more. That is, the solution is specialized to our specific problem.
_reduced_: the most concise form of the expression. No unnecessary "moving parts" or prescribed rules and conditions.
_unoptimized_: the most straight-forward expression, not yet fine-tuned towards a particular target, or not yet fine-tuned for reprioritized for other properties.

I mention this definition to allow you to think about possible distance from simplicity, and moving towards simplicity in terms that are compatible with theory and technology.

> "Everything should be made as simple as possible, but not simpler." -- Albert Einstein

This quote is dead on. There are always trade-offs to be made. Simpler is better, unless there are other reasons that you value over simplicity. Let's take the basic example of functions. Writing code that is one long sequence of instructions, is "simpler" in terms of its existence: you read top-down, it is executed top-down, every statement present is executed in sequential order. We do not do this anymore, because there are benefits to be had that outweigh extreme simplicity, e.g. a basic form of reusability, conciseness, and deduplication.

And sometimes, even basic reusability is undesired. Appendix B describes an example of how going to even more straight-forward expression helped optimization, even though technically it hurt code reuse and deduplication.

`TODO: make real hyperlink to appendix.`

```TODO
simple = specialized + reduced + unoptimized, (given: a specific target).

specialized for a single purpose, as opposed to generalized for many, a broad class.

unoptimized = prevents premature optimization.
```

## Orthogonality

A particularly interesting property is _orthogonality_. Orthogonality is a very abstract notion, generalized to the point that it can be used in many different fields. Orthogonality, particularly in the case of programming language features, indicates whether or not two features influence each other's behavior. When two properties are completely orthogonal, you can reason about them as if reasoning about each feature independently. If these features are not orthogonal, you would need to take certain exceptional cases into account where behavior deviates due to the two properties interfering. So, as part of feature realization, conditions, deviation and prescribed behavior for interference points are defined.

An example of a reasonably orthogonal set of features are Go's basic syntax for function definition and calling, and the go-routine support. The basic syntax for a function: `func sayHello(name string) string {...}`. A function that takes a `name` argument and returns a hello-`string` (the body is not relevant here). This function can be called, it will execute and return the result. Go can run arbitrary functions or methods in a separate thread, a go-routine, modeled after the idea of coroutines. To execute a method in a separate thread, one prepends the keyword `go` to any function call, e.g. `go sayHello("your-username-here")`. The goroutine feature is almost completely orthogonal as any function is suitable, i.e. there are no special requirements for the function to be suitable. The only consideration is that the return type becomes meaningless. The returned value would disappear, as the function is not executed as a subroutine - a normal function call - but rather as a coroutine, called in an independent thread-of-control. There are further considerations when multiple threads need to synchronize or exchange data, but we will leave these out of consideration for this example.

`TODO: discuss an example of two features with heavy interference difficulty. Java 1.4 types and 1.5+ generics and type defs?`

Orthogonality, although not strictly required and not always applicable, is a good indicator for complication. If a feature is "_not quite_" orthogonal to existing features, but you can see possible improvements towards this goal, it is worth considering. If a feature is really not orthogonal and there are a lot of rules and conditions before the feature can be applied, this may be an indicator to go back to the drawing board. This may also indicate that the feature itself has mixed concerns, or that there is an overlap in the concerns of the new features and the concerns of the existing features. It deserves extra attention to see if these issues can be fixed.

Consider that any features need to be maintained in the future too. As more and more features are added, new features need to play nice with all previously existing features. However, the semantics of existing features cannot change either. The more non-orthogonal features are introduced, the more complicated the interplay becomes, with each feature influencing others. In a worst-case scenario, every language features interferes with every other language feature. Each feature has to work not only with each other feature, but also be compatible with the intersection of each pair of interfering features. Especially in the design of programming languages, this warrants consideration due massive growth of complexity, but may be necessary for the promise of backwards compatibility.

## "Natural" boundaries

`TODO: ensure that the description makes clear that it is often a conscious concerted effort by the maker to find a suitable boundary and to (be able to) define the conditions such that it can be based on this boundary.`

> Distinguishing characteristics inherent to a decision.

Natural boundaries, or more accurately _"naturally" occuring properties suitable as boundaries_, are a core mechanism used in this document. Its significance is in the fact that, because these boundaries already exist for a different reason, there are benefits to using _them_ instead of arbitrarily chosen boundaries, e.g. boundaries based on arbitrary values or personal "_taste_".

Distinguishing characteristics may be present either in theory (hardware, specifications) or in practice (protocols, designs and implementations, `TODO: is there more here?`). Given that these characteristics already exist for other reasons and pose a limit of some kind, they are inherently suitable as a boundary.

To use such bounds is not effortless. It often means rephrasing and/or redefining the boundary condition such that it fits with the characteristic of the chosen bound and still works to accomplish the intended goal. Additionally, not all boundaries are equally suitable. It requires insight and a certain way of analyzing to learn to quickly identify and use such boundaries.

> Analogous to nature's natural boundaries. E.g. land separated by water: where a river, sea or ocean doubles as natural border between counties, provinces, states or nations.

In case of software architecture, design and programming, natural boundaries are often derived from technological limitations, decisions at technological foundations, constants in protocol specifications, programming language's paradigms, high-level features and their constraints, and language syntax and semantics.

In case of computer science, boundaries often have a binary component to them. Let's take message delivery semantics, for example. It is complicated to ensure a message is delivered exactly once. This means, the message is guaranteed to not get lost, and the message is guaranteed to not be delivered twice to the receiver, taking into account various types of failure conditions. This is a hard problem to solve. However, carefully evaluating the problem, reveals that there are two guarantees to be satisfied: delivery guaranteed _at least once_ (`1` or more times), and delivery guaranteed _at most once_ (`1` or fewer times). These two guarantees are the actual base cases that need to be considered. The natural boundary is the magic value `1`: less than one means nothing happens, while more than one means corrupting state if each of the (duplicate) messages is processed, causing state corruption.

Interestingly, delivery semantics as described above are often represented as symmetric counter-parts. Considering the same semantics from the point-of-view of which of the communicating parties has the actionable knowledge, you would clearly position _at least once_ at the sender-side, and _at most once_ at the receiver-side. _at least once_ at the sender-side, because the sender is the first one to acquire the message so it knows whether it was acted upon or not. _at most once_ fits better at the receiver-side, as we need to know definitively that the message was fully processed thus not lost prematurely. Notice that we are evaluating the full message handling process, instead of merely whether or not "the message reaches the other side of the transport channel". There are multiple levels to consider for this problem.

`TODO: is the above paragraph too much for this topic?`

Rules and conditions that derive from "natural boundaries" have a stronger foundation than those based on arbitrary values. Furthermore, they are more compatible with designs, as they already have to comply with existing boundaries.

### Example: design for the moment

Another type of natural boundary is the decision to make the decision to design/implement only for the current problem statement. Even if some information about future requests is known, even if they are already being discussed and there is a viable path to implementing, you should not do this. The reason is that many features interfere/influence each other. It is better to have a solid implementation of exactly one feature, than to implement with mitigations merely for the preparation of the potential arrival of a feature request.

The concern is not that the request may not come. Rather, with every moment of time, plans can change, requirements can change, priorities shift. Anything not currently necessary is technical debt. Anticipating for potential future means introducing technical debt in anticipation of a possible future. You should consider what is valuable _at this moment_: do you choose a single well-implemented requested feature, or many features already partially implemented, anticipated for a future that may never come having chosen a solution direction that may no longer be present. What is real, is the maintenance burden. Real from the moment this work is merged into the code base.

Previously, I defined _simplicity_ as consisting of three properties: _reduced_, _dedicated_, _unoptimized_. Simple is the best-case code to have: it is straight-forward, to the point, therefore minimal impact on technical debt. Elaborate, generalized or optimized code are trade-offs. These influence technical debt. Excessively elaborate statements affect readability, possibly the compilers ability to optimize, etc. Generalization may require handling of more error cases. Optimizations are there for the benefit of efficiency, but at the cost of requiring more fine-grained control over the logic.

`TODO: does it make sense to say that optimization, etc. have impact on technical debt???`

```TODO
- holds both for technology, designs (min/max values), protocols (constants in spec), theories (encapsulation: privileged vs usage access), etc.
- chosen constants and other boundary values introduce "edges" that can be used as boundaries. By defining your conditions in such a way that they match/play into such characteristics, one can avoid choosing arbitrary (non-sensicle) values.
- by using the values inherent in the technology, boundaries/conditions can be appropriately adjusting/dynamic.
- natural boundaries work better than boundaries defined by arbitrary values as natural boundaries already have an existing nature to repulse, i.e. already provide friction.
- TCP 3-way handshake as example of arbitrary number:
  - ... you can basically go on infinitely, because given an infrastructure that cannot guarantee lossless transmission, a packet can always get lost. Therefore, there is no "right" number for amount of acknowledgements. Just a number "that works". The more lossy the infrastructure, the more problematic this is.
  - Discussions on whether to or how acknowledgements are still going on, e.g. in inter-planetary networking protocols. (TODO: Source?)
  - An alternative is to anticipate loss and have other interaction mechanisms to compensate for this, either a-priori or a-posteriori or make lossy work if that is an option. TODO: double-check if this example is sufficiently realistic to include: <https://tools.ietf.org/html/rfc675> <https://en.wikipedia.org/wiki/Transmission_Control_Protocol#Connection_establishment>
  - Choose _exactly one_ approach and define the corresponding guarantees.
    - counter-example: TCP congestion control, requires saturated connections to drop packets such that congestion control can use that as an indicator. In the mean time, routers started getting ridiculously large buffers to store a backlog of packets for retransmission, causing huge delays, but packets _eventually_ being delivered. The result is that the congestion control algorithm _cannot_ function correctly/efficiently. (packet drops are no longer a signal, roundtrip time varies wildly so similarly bad indicator)
- rules/boundaries based on arbitrary values are sometimes: confusion, ambiguous, prone to discussion on every new instance, requiring consensus on "how this should work" for each application, ...
- ...
```

`TODO: refer to Kardashev scale for nice example of theoretical natural boundaries (distinguishing types) (https://www.youtube.com/watch?v=rhFK5_Nx9xY)`

`TODO: is term procedural correct? (imperative may be more suitable here)`

## Architecture vs design

```TODO
Design: the decision making process based on given problem statement and other necessary requirements. The design focuses on the problem at hand, produces a solution that fits solves that problem exactly. (No anticipating potential futures.
Design looks inward towards the solution that is being constructed, makes the decisions to make that solution work/fit.

Architecture: requirements - often non-functional - that are relevant to make a solution fit in the larger context, i.e. the business, the application infrastructure, the network infrastructure, etc. These are not the requirements that solve the problem at hand, but rather that make the problem suitable to be adopted by the business/into the infrastructure.
```

## Engineering vs development

```TODO
Development: designing and programming a solution. Focuses on the solution for the one stated problem. Focus only on the _current_ problem, as opposed to past and future obstacles.

Engineering: business/architectural requirements, analysis and development of a solution that is suitable for the business problem and architectural requirements. By taking business domain and architectural requirements into consideration, you extend the view past the "designed single solution to a single problem" into the larger context. Introduces the time-dimension to be compatible with larger ecosystem, as well as specific requirements for upcoming projects.

Engineering is quite literally the next layer on top of solution design/development, orchestrating how a (given) solution fits in with the rest of the ecosystem. 
```

## Technical debt

Informally, _technical debt_ is the collective name for all the less-than-ideal aspects of the solution (implementation), often a code-base. Ideally, one would clean up any such debt, akin to fixing bugs. The presence of technical debt is a burden on maintenance, reliability and other properties.

Technical debt are things such as obsolete code and functionality, messy code that works but no-one knows exactly why, highly-optimized code that is fast but requires specialist knowledge to comprehend, bad error handling, support for irrelevant use cases, overly-complicated logic (interleaving) causing issues with comprehension, etc.

A formal definition of _technical debt_ seems to be lacking. Following, I will make an attempt, though this definition is not officially recognized, and possibly too restrictive.

Technical debt are the (negative) consequences of everything that _isn't "simple" about the solution for a given problem_. Notice that "_anything that isn't simple_" includes content with (intentional) optimizations/generalizations and content which was not reduced on purpose and/or introduced unnecessary dynamics. This makes sense, because you deliberately traded off simplicity for a niche characteristic.

In practice, technical debt causes excessive delays, complications during code changes, difficulty testing, spaghetti code, unexpected errors, and so forth, and additional mental overhead for its developers which may result in missed use cases, obsolete use cases, undocumented use cases, and difficult to comprehend (traces of) functionality.

## Verification vs validation

Verification and validation are mostly used as you would expect, and occasionally the terms are just mixed up.

_Verification_ is about looking "inward", reflecting on itself. While _validation_ is looking to an external source for information, e.g. to compare against or for checking instructions. _Verification_ is about confirming things internally correct, and _validation_ is about confirming things correct according to an external source of knowledge.

Let's use an XML-document as an example. When one _verifies_ a document, this document is being checked to see if the correct XML syntax is used. For example, to confirm that tags that are opened, are also closed. That no illegal characters are used. That special characters are escaped. And so on. Just by looking at the document itself, one can verify the document.

Now, let's look at the same XML-document and assume that it is correct in itself. There exist XML-schema documents, and our example XML-document can define which schema describes its structure. Now that we know what the document should look like, we can use the XML-schema and _validate_ the XML-document against it. In addition to the XML-document having a correct consistency, now we can also see if the right tags are present in the right order, nested correctly, containing values of the designated type, etc.

We previously discussed about natural boundaries. The natural boundary here is at the transition from looking inward to looking outward for performing checking.

## State invariants

The computer science there is the notion of "_state invariants_". The _state invariant_ expresses the rules that need to hold for an application to work correctly. Basically, if state invariants do not hold, it may have consequences such as failing logic, incorrect calculations, etc.

Defining state variants over a complete application is difficult. There would be many invariants, all covering very specific parts of the application. The inherent difficulty is in the amount of data that needs to be managed in a complete application. To mitigate this difficulty, application logic is often partitioned into many different parts. Parts can be procedures, functions, objects and/or modules. Procedures and functions typically carry only local state. Objects and modules, on the other hand, carry persistent state being reused whenever an object or modules is called.

Objects and modules are where the _state invariants_ are applicable. Each state invariant describes the rules, expectations, of one or more variables. Or, if applicable, the relationship between the values of multiple variables.

As an example, let's consider an implementation of a list data structure. The list manages the elements inserted and the current size of the list. Whenever new elements are inserted, elements are removed, elements are updated, or the list is cleared, the size needs to be updated accordingly. The state invariant would say that the _size_ needs to correspond to the _number of elements_ in the list. To acquire data, one looks up the right _element_. However, to find out whether the list is empty, one would check the _size_. Both variables need to show a consistent state of the data structure. If the right state invariants are defined, and these are upheld, then the data structure can be used without risk of corruption.

State invariants are theoretical rules. They may be realized as logic in the code, or sometimes can be declarative, as rules. Even if state invariants are not explicitly defined, sound  logic will still respect them implicitly. (Or the logic would contain errors and eventually produce bad state.)

`TODO: reread, as this is a first rough writing.`

## "Usage" vs "privileged" access

This is a prelude to the upcoming topic on encapsulation. For now we will simply consider logic that touches data in a way it can be corrupted, and logic that touches data in a way that is harmless, i.e. no risk of corruption.

Previously we discussed _state invariants_. State invariants being the rules that define when data is valid, if all state invariants are satisfied, and when some data is invalid, i.e. state is corrupt. Based on this logic we can distinguish two types of logic: logic that reaches data in a way that risks state corruption, and logic that reaches data in such a way that it cannot corrupt.

Let's say that logic that _can_ corrupt data the "_privileged_" logic, and logic that _cannot_ corrupt data the "_usage_" logic. "_Privileged_" because this logic enjoys a certain privileged position being allowed to modify data arbitrary, granting this logic a significant amount of trust. "_Usage_", similarly, because this logic is merely the typical use of state in a way that cannot make it fail. Hence _no_ special privilege is enjoyed or necessary. _Privileged_ logic, by its definition exists only because the logic cannot be implemented otherwise, i.e. as _usage_ logic. We would expect the _privileged_ logic to be rather minimal, while the _usage_ logic is everything else.

The _privileged_/_usage_ split is the minimum partitioning that allows protecting (internal) state. Various programming languages have a different number of levels, different level-names, and different effects tied to this type of access moderation. We will continue to use the terms _privileged_ and _usage_ logic to avoid biasing the terms towards certain notions of access control as defined in a particular language.

## OOP: paradigm vs structure

`TODO: where to put the explanation that you get "separation of concerns" for free when you correctly apply encapsulation/OOP-structure. I.e. only combine data that is inseparable otherwise, with minimal logic to expose it safely. Consequence is that we only capture the concerns that are inseparable anyways. ("Separation of concerns" as emerging property from the right approach to encapsulation.)`

`TODO: introduce distinction between "privileged"/"usage" access, i.e. logic that may freely access the fields possibly temporarily violating the state invariants and logic that is only granted controlled access, therefore not able to violate/corrupt the data. (private vs package/protected/public, unexported vs exported)`

Before we can get started with any of the main topics, it is critically important to clear up some common misunderstandings. The first and - by far - the most important one is the misconceptions surrounding _Object-Oriented Programming_ (a.k.a. _OOP_). This fallacy is impacting multiples areas, including education, program structure and programming rationale.

At no point, is the distinction between the kinds of OOP intended as a value judgement. Both kinds have their uses. Each kind impacts a different level of software engineering. The two variants are complete orthogonal. There is no competition, no matter of "good vs bad". There is the matter of incorrect reasoning, which may result in bad decisions on multiple levels of a program.

Also note that this section does not intend to provide full definitions of either variant of object-oriented programming. The descriptions are geared towards emphasizing the fundamental differences.

### The OOP paradigm

`TODO: Alan Kay reference`

`TODO: include term multi-tasking somewhere?`

Alan Kay is the one who originally coined the term "object-oriented programming". He explained this as a new way of programming, i.e. a new paradigm. He reasoned that logic could interact similarly to how cells interact in biology. He described the idea of how small pieces of logic, which he called 'objects', would interact with each other by sending messages back and forth, calling it "message passing".

Alan, at the time, was fully aware that for this to work there is a necessary characteristic, which I'll call 'independent execution'. There would be two objects, one sender and one receiver, which each act independently having 'message passing' as a momentary synchronization. (I will not consider asynchronous communication here, to keep the subject narrow and focused.) For those familiar with the internal workings of computer programs: each of the objects has its own stack. The key characteristic is '_concurrency_'. All '_objects_' have their own independent "_thread of execution_", aligning only momentarily at the point of communication (message passing).

So, why call this a paradigm? Notice that the word _concurrency_ was chosen deliberately. We cannot know exactly how the program will be executed on different computers. Single-tasking single-processor systems will only be able to execute one object's logic at a time. Only when execution cannot continue (dependence on another object e.g. point of communication - sending or receiving) or at predetermined points in logic for yielding control will another object get executed. Multi-tasking single-processor systems may switch objects independent of the code. Although single-processor, it creates the illusion of multi-processing. Multi-processor systems will be able to truly execute multiple objects simultaneously.

This effectively allows for the emergence of non-deterministic program execution expressed in structure. Non-deterministic - not only for different systems, but also for multiple executions on the same system. The non-determinism is emergent from the way object-oriented programming structures/describes the program. While procedural programs may contain logic instructing the use of multiple threads of execution (explicit, execution-time), object-oriented programs inherently enable multiple threads of execution by their structure (implicit, design-time).

`TODO: looping code in SCUMM game engine: (while true; animate clock tick; sleep 1 second; done)`

[Article 'The SCUMM Diary'](https://www.gamasutra.com/view/feature/196009/the_scumm_diary_stories_behind_.php "The SCUMM Diary: Stories behind one of the greatest game engines ever made (Gamasutra)") tells the story of SCUMM, an adventure game virtual machine. This game engine has been designed with concurrency as first-class citizen. The story illustrates nicely how concurrent systems thinking is different, powerful. The article could help to understand concurrency more thoroughly.

`TODO: discuss how lacking a concurrent system would require "weaving" all parts into a predetermined sequence of (inter)actions. For SCUMM this would mean everything is going to be in a single static sequence. With the elegance of the concurrent system engine that is SCUMM (actually engine is the other one ...) we introduce a lot of flexibility without any additional requirements to computing hardware.`

### OOP language characteristics

The other variant of Object-Oriented Programming is the one where three properties converge: _encapsulation_, _inheritance_ and _polymorphism_. This OOP defines certain capabilities of programming languages regarding flexibility, code re-use, and dynamism.

`TODO: mention classes/prototypes/structs/...`

_Encapsulation_ allows grouping of data and corresponding logic, protection of data from bad use and mutations, effectively creating minimature "modules" of program, now called '_objects_'. These _objects_, given that data is shielded and access can be forced through the object's containing logic (called '_methods_'), are protected from misuse. A well-written object cannot be used incorrectly and should therefore not fail, assuming a correctly functioning system.

_Inheritance_ enables certain types of code re-use through the ability to write an base-level of functionality that can be extended in various ways through multiple implementations. A different way of re-use is through composition, i.e. the use of existing objects as part of another object, however that is a different kind of re-use. Inheritance also allows defining methods to-be-implemented by derivations (_abstract methods_) and it allows for deriving classes to override or add to existing methods. Inheritance produces a hierarchy, where one class may inherit from another class. (An implementation detail of programming languages, which is sometimes exposed, is that all classes derive from at a minimum a base class called 'Object' or similar.)

`TODO: mention hierarchy as part of inheritance, to clarify the "direction" of inheritance.`

_Polymorphism_ is the ability to distinguish between actual types (implementation) and expected/required (interface). It allows distinguishing between what users of the object should see and how the object is implemented (internals). It allows setting expectations and guarantees. It enables multiple implementations for the same type, i.e. _multiple derivations_. It allows for types that are merely defined, i.e. _interfaces_. In its essence, _polymorphism_ is the ability of a class to be viewed differently, based on different requirements. Consequently, this introduces predetermined points for dynamic behavior in application structure, as multiple types may fit.

Apart from the properties described above, the interplay of each of these will result in interesting capabilities. _Polymorphism_ and _inheritance_ allow defining objects for use without an implementation existing. _Encapsulation_ and _polymorphism_ allow controlled mutation of data by pieces of logic out of your control. These three characteristics combined provide a rich and powerful language of possibilities.

Notice that - with all the possibilities - nowhere do we introduce non-determinism during program execution. This is important to realize: however complex we make a object-oriented program, we are always able to deterministically trace execution from one statement to the next. The rich possibilities for dynamic behavior provided through syntax does not make it easy, but it is certainly possible. We _can_ make the program execute non-deterministically. However, we would express this through logic statements that introduce threading, not because an "object" is inherently independently-executing. Note also that we usually talk about threading (process threads as known by the operating system, execution time) instead of the concept of independent threads of execution (concurrent behavior, design-time), meaning that we accomplish non-deterministic execution instead of non-deterministic program structure.

`TODO: reread and fine-tune the wording.`

### The OOP fallacy

`TODO: this whole section is crap, rewrite and focus/divide in clear concerns, etc.`

There are a number of characteristic mistakes being made in using OOP.

- You have to realize that there is "OOP - the paradigm" and "OOP - the syntactic capabilities". The former expects a different mind-set, the latter expects thorough understanding of available syntax, and how combining them exposes new capabilities.
- The OOP paradigm is about designing an application, structured for concurrency. It is a way of reasoning about applications as independently operating, cooperating parts that synchronize (only) at predefined synchronization points.
- OOP the syntactic capabilities is about decomposing large programs into smaller parts - avoiding complications due to downscaling, and data management.
- OOP the paradigm is non-deterministic by design, may have many different call-stacks due to independence of parts. OOP the syntax is deterministic by design, based on a single call stack by design.
- OOP the paradigm may apply OOP the syntactic capabilities in each of its individual parts.
- "method-calling" is not a different name for "messaging". Methods are called on the same stack. Messaging is a synchronization construct (though typically not a primitive) to momentarily align (synchronize) independently operating parts, i.e. multiple call-stacks.
- The mix-ups between the paradigm and the syntactic characteristics resulted in misconception on OOP not being data-oriented. The syntactic characteristics, however, are especially concerned with data management.  
- `TODO: make distinction between data-oriented as data leading object design, and data-oriented as in types organized/managed in large arrays for efficiency and fast accessibility. (The latter is an optimization that is done in case very fast access is needed. The structure of the data no longer reflects its use.) As mentioned, data organized in arrays is necessary for speed, but its need is vastly exagerated/accelerated (?) by languages that use pointers for referencing everything in different spots on the heap, versus many fields packed together in a single struct, hence a single memory area. Issue partly exists because e.g. Java extensively uses heap-allocated memory for class fields, resulting in severe penalties.`
- OOP, the programming language syntax, is actually data-oriented contrary to what many people claim. These claims are based on misconceptions on how to use OOP: encapsulation to protect data, polymorphism to support various types of implementations, inheritance to share a partial implementation.
  - ADT - as mentioned by Alan Kay - actually puts data at the center. However, forcing data in inconvenient structure due to misinformed "rules" of how classes should be defined, is the cause of bad experiences. The label given to the concept has gotten priority over the data that represents the concept. Furthermore, the "conceptually seemingly nice methods" have gotten priority over "methods necessary for use".

[1][youtube-rustconf-catherine-west],[2][email-alan-kay-oop]

### Programming languages and OOP

`TODO: make explicit that in OOP-paradigm: object is self-sufficient concurrent "living" entity, while in OOP-structure: object is instance of a class.`

In the end, the OOP paradigm is about designing _concurrent systems_. That is, one thinks about a solution as a composite of smaller, independent parts.

Whether you use OOP - the paradigm - itself, or approach it through other facilities provided by the programming language, does not matter. The way a solution is implemented is not prescribed.

It should be clear that programming languages that inherently follow the OOP paradigm are easier to use as the paradigm is prescribed in the programming language syntax. Erlang and Smalltalk are well-known examples. Go is a programming language that contains some features in support of the OOP paradigm: the `go` keyword initiates a _coroutine_, a concurrent piece of control flow that runs in a user-level thread, a _go-routine_. This primitive makes it possible to construct the individual small independent parts of a composite system as envisioned in the OOP paradigm. Many other languages, such as Java or C#, do not natively support concurrent systems. Java is only recently exploring user-level threads. `TODO: reference project 'loom'`

Many programming languages support the OOP structure, the abstract data types (ADT) Alan Kay also refers to. Java, C#, C++, etc. support the various OOP properties and consequently their powerful features. C does not inherently possess the OOP syntax, but can very much - if not completely - approach OOP's syntactic capabilities.

`TODO: OOP syntax or semantics? It seems that calling it syntax is too superficial. Actually, it's OOP semantics expressed through a dedicated syntax.`

### Design patterns

`TODO: is this better suited as a subsection to OOP or as separate section?`

[Design patterns][wikipedia-software-design-patterns] are a set of recipes for OOP-structured compositions that solve certain kinds of problems. Each of the solved problems are solved by a construct, an composition of objects (OOP-structure), introducing a level of abstraction exactly where variability is needed to solve the particular problem - and nowhere else. Design patterns exist to introduce _indirection_. That is, they exist to introduce variability at places where the constraints of the programming languages would otherwise not allow variability.

`TODO: rewrite above section to recognize that not all design patterns are OOP-structured in nature. Don't dismiss other procedural-based design patterns.`

A benefit of static programming languages is the amount of control that is available to the programmer. Programming languages expect certain parts of code to be static. Design patterns provide deliberately designed constructions to relax strictness at particular applications without having to sacrifice control in the programming language generally. The design patterns exhibit certain desirable characteristics, these being the type of relaxation that is applied. Be it to make certain logic interchangable/configurable, allow absence of logic/data, allow late decision-making on data/logic, enhanced control over data/instances/logic, ability to extend existing data/logic, etc.

Design patterns are relevant for code that is unavailable for change. This could be because code is out of your control, e.g. from a third party that cannot be modified. Or it could be code - in your own control - that already covers a single concern and should therefore not be extended.

Design patterns themselves can again be composed to solve a more elaborate problem. One can evaluate the characteristics of each design pattern and select the design patterns that compose into the desired set of characteristics.

The [Design Patterns-book][book-GoF-design-patterns] is intended as a hand-book - a reference guide - to software architecture. In the definition of 'architecture' assumed in this document, the book is an registry of _design_ constructs rather than _architecture_ constructs. These constructs are all based on the orthogonality of OOP properties _encapsulation_, _inheritance_ and _polymorphism_. The recipes demonstrate how to apply the various properties (sometimes repeatedly) to achieve a level of indirection aimed to solve that particular problem. The book classifies design patterns into three categories: _creational_, _behavioral_ and _structural_.

`TODO: is the Design Patterns-book list of patterns exhaustive? It should be possible to create an exhaustive list of design patterns, as there are only so many unique ways in which OOP-structured properties can be composed.`

`TODO: read and include if useful: https://aseure.fr/articles/2020-09-18-dont-hate-the-book-because-you-dont-use-it/`

## Processes and threads

We previously discussed how _concurrency_ is about designing systems to consist of multiple independent, interoperating parts. Concurrency does not give any guarantees on actual execution. Processes and threads are concerns at time of execution.

Both processes and threads are contructs of the operating system. Processes are the mechanism for isolating the execution of a program. A program runs in its own context. Threads are the mechanism that adds parallel execution to the process. `TODO: natural boundaries provided by the operating system.`

Processes and threads are managed by the operating system, used for scheduling execution time on the hardware. `TODO: relatively heavy due to larger context: stack, other memory for managing by OS, crossing boundary from user-space into kernel-space with corresponding context-switching overhead.` Specifically for programs designed for concurrency, there is a necessity of multiple independent threads of execution, i.e. the OOP-paradigm _objects_. However, nothing dictates that these need to be operating system threads. A light-weight mechanism, that exists only inside the application itself, is already sufficient.

`TODO: mention multi-processing and multi-threading here to distinguish between those and multi-tasking. Multi-processing and multi-threading as OS-supported parallel execution constructs.`

`TODO: ...`

### User-level threads

Similar to processes and threads, there exist the concept of user-level threads. Whereas processes and threads are operating system terms that indicate specific conditions under which a program executes, user-level threads are more of a conceptual representation. User-level are like threads, but they exist fully in user-space. That is, the user-level thread as well as anything related to the execution is managed from user-space. No context-switch to kernel-space is necessary. However, this implies that scheduling, execution and other management actions are taken in user-space as well, often from within the application itself.

The benefit of this is that user-level threads _can_ be very light-weight. Its stack is small. The thread-context only needs to contain necessary data from within the application. This allows user-level threads to very closely approach the theoretical concept of "thread of execution" in concurrent designs, where one only reasons about the various threads of control, without considering the overhead involved to make it work.

> User-level threads, a.k.a. light-weight threads, a.k.a. green threads, a.k.a. fibers, are used for concurrency in applications.

`TODO: do jobs (multi-tasking) fit in above list? Include [non-preemptive multitasking](https://en.wikipedia.org/wiki/Cooperative_multitasking) in the user-threads list?`

A few of the names for user-level threads. Different programming languages have different names, although it basically boils down to the same concept. Implementations may vary, primarily due to the difference in characteristics between programming languages.

> Coroutines a.k.a. generators (Python) a.k.a. continuations (Java, project Loom) a.k.a. objects (OOP paradigm)

As programming languages offer a variation of the user-level threads concept, they typically expose some low-level primitives for the developer to use. These primitives are what enables the developer to express the various parts of the concurrent system.

The most well-known are the Python generators. Python allows the creation of so-called "generators" which are independently executing functions that in addition allow one to return intermediate results (using `yield` keyword) and then at a chosen moment to resume execution until the next `yield` or the final `return` statement.

Consider how, given that a generator has its own stack and the ability to pause execution, one can now write infinitely executing functions. For example, write a function that returns all even numbers, running into infinity. As long as the generator is asked, a new number is generated. As long as we stop asking at some point, the generator can be infinite. There is no need for it to end, because some other piece of code determines how often it runs.

`TODO: check and execute code sample to ensure it is correct!`
```python
def generate_even_numbers():
  i = 0
  while True:
    yield i
    i += 2
```

`TODO: go into detail on cooperative tasks, infinite programs (e.g. prime-number generator using generators and yielding in python, or goroutines and channel in Go), infinite lists, pipes?`

Generators themselves do not have a separate thread-of-execution. The generator concept allows defining a separate thread, but says nothing about its execution. Generators, coroutines, continuations, objects and the likes, are how we can define parts of the concurrent system. User-level threads, green threads, fibers and the likes, are what performs the execution itself. These are two halves of the recipe for concurrent execution.

As previously mentioned, [article 'The SCUMM Diary'](https://www.gamasutra.com/view/feature/196009/the_scumm_diary_stories_behind_.php "The SCUMM Diary: Stories behind one of the greatest game engines ever made (Gamasutra)") tells the story of SCUMM game engine. The article touches on details of how concurrent systems were executed.

User-level threads are especially suitable for concurrency. Due to them existing within the application's process, many of the concerns regarding overhead that are present for OS-level threads and processes are gone:

- stacks can be small in their initial size, hence there is a small per-thread footprint,
- there is no kernel involvement and no (process) context-switching,
- coroutines as opposed to subroutines (functions, procedures, methods), i.e. these are independent threads,
- no guarantees on parallel execution, as these are managed within the application,
- (often) no control over low-level execution properties such as thread-local storage, processor caches, thread ID, etc., unless exposed by the runtime/scheduler of the application.

Conceptually, to make concurrent designs work, you should be able to freely design and use as many concurrent threads as needed. Therefore, practically, we need absolutely minimal overhead for each thread to realize concurrent designs. These are coroutines, pieces of logic executed concurrently.

An [article written by Simon Tatham - of 'putty' fame](https://www.chiark.greenend.org.uk/~sgtatham/coroutines.html "Coroutines in C") explains the complexity of designing (with) coroutines in C.

```
- "OOP paradigm"-ed programs may use OOP-structured program, i.e. orthogonal + different conceptual level.

This is not about cracking languages, but rather on the distinction in rationale that is necessary for following chapters.

Right: Erlang, (~)Go, Smalltalk (various incarnations), ...
  - Go has caveats as it still requires "expressing" individual t-o-e through 'go' keyword. Go does not guarantee multi-processing, but instead gives primitives to describe many concurrent executions and user/developer has to wait for execution to see how they interact/impact/run.

Wrong: Java, C#, C++, Python, ...

In OOP (paradigm) there is also "OOP" (structure) a concern, but at the level of individual objects.
```

```
- Java/C#/Python/C++/...: OOP-structured, i.e. key feature missing from OOP-like features: concurrency, i.e. message passing between independently executing entities.
- OOP-structured: data encapsulation with related logic "attached".
- How to use not-OOP? Procedural. Not like OOP. Reduce your expectations.
- ... (spaghetti-code?)
```

## Low-level vs high-level programming languages

Low-level is referring to how close the programming language is to the hardware it runs on. The lowest level language is writing the `1` and `0` bits directly. One needs to understand _everything_ about the hardware it runs on. The higher-level the language is, the details it abstracts away for the benefits of portability, cross-platform support, ease of use, etc. Abstracting away details usually comes at the price of either overhead or limitation.

There are all kinds of features raise the level of the programming language due to taking away a particular concern. One makes a trade-off by choosing a high-level langauge, usually at the cost of execution performance. However, not every feature comes at a cost, and not every cost has to be paid all the time. Depending on how the (language) feature is designed, one may opt-out at crucial moments.

Let's look at a number of concerns of programs, some are considered "high-level features":

- `(writable XOR executable)` memory regions: strict separation of data and logic.
- memory tagging
  - RWX
  - normal vs secure vs custom-defined memory region (access control/isolation e.g. ARM)
- stack management:
  - pre-determined, fixed stack
  - pre-determined, dynamic stack (growing, copying)
- memory management:
  - none,
  - stack-only (avoid heap allocations),
  - data ownership (moving),
  - reference counting, garbage collection.
- memory allocation:
  - no management necessary (part of language/interpreter)
  - allocate specific regions
  - pre-allocate arenas of different sizes, then allocate from arena. (`TODO: arenas same as pools?`)
  - allocator strategies depend on characteristics of memory use (everything allocated on heap e.g. java vs. most on stack)
- thread management:
  - none,
  - preemptive scheduling,
  - cooperative scheduling.
- data management
- cross-compatibility of code:
  - written for single piece of hardware,
  - single ISA,
  - independent of platform/operating system.
- (natively) executing vs. interpreted
- representation of primitive values (endianness, encoding - such as two's complement, lossy decimal numbers as IEEE floating point / bfloat16 / posit)
- availability/accessibility of heap memory
- polling/eventing/interrupting
- "absence of values" vs "values in bytes of memory"
- null pointers/references
- bits/bytes vs. type hinting vs. type-system
- alignment of fields in struct according to register sizes/word sizes
- by-value / by-reference
- copying and moving semantics (move local variables in case reuse in called method)
- memory-model
  - strictness of memory model
  - (re)ordering of memory accesses, memory barriers
  - compile-time vs execution-time reordering of memory accesses
- characteristics of memory access:
  - DMA: fast access, but raw, i.e. without protections.
  - availability of protection for raw memory accesses (e.g. IOMMU)
- ISA/Structure of the processing unit(s):
  - single large CPU/GPU vs SoC (CPU, GPU, AI, DSP, etc., such as Apple M1 - Apple Silicon)
    - determines for large part the amount of parallelization, independence of execution and expectations in instructions/data preparation.
  - properties of instructions, registers: how to prepare instructions and data, ordering, which registers to use, for which type of register-use to optimize (i.e. many general purpose registers - data placed such that instructions cannot conflict, vs: each instructions has specific places to look for data)
  - properties of instructions: arithmetic w/ or w/o overflow w/ or w/o carry-flag, i.e. multiple variations.
  - properties of instructions: scalar/vector.
- CPU performance given certain use of assembly instructions (AVX512 will slow down processor speed)
- synchronization (primitives) (mutexes, semaphores, atomic operations, locks/spinlocks, monitors/conditions, etc.) `TODO: atomic operations are synchronization primitives? probably not`
- syntax / types used to express size of memory allocation and its intended bit representation, as opposed to (valid) value representation including custom/arbitrary boundaries. (Former expresses what to expect from a memory region, but additional boundaries/rules may be present in actual type semantics. Latter actually expresses the type semantics - possible/allowed values.)

```TODO
- data management (enforcing restrictions/contraints on usage of data a la rust)
```

All of these contribute to making programming easier, assuming that the high-level features are completely acceptable as they are designed to operate, i.e. you fully accept the cost. All aspects of programming that are taken care of, are aspects that no longer take up "mindspace". It becomes easier to imagine the big picture due to there being less details to take into account along the way. Given that some aspects are handled transparently, there is less code to be concerned with. More of the behavior is implicit, i.e. not visible in the code-base itself. This makes the code-base more concise, more expressive compared to lower-level languages. Fewer lines of code express more elaborate program behavior.

`TODO: move below to high-level vs. expressivity section?`

However, using a high-level language is not the only way to achieve more expressiveness. It is important to realize that one only needs to adapt the programming language for features that cannot be implemented on top of the language. This is also a reason why the programming language _C_ is so pervasive. It is sufficiently high-level to be useful, but low-level to the point that many useful high-level features can be implemented (to reasonable extent) as a library. However, although many features are _possible_ to some extent, you still pay the low-level cost of the features that _C_ is lacking.

```TODO
Having sufficient utility functions available to tackle elaborate operations in a single function call. Only features of a certain complexity are required to be handled as part of the programming language, as they cannot be realised otherwise. For all other features, implementations in a particular language is sufficient. Therefore, having access to libraries that provide utilities for abstracting over certain concerns is an equally powerful method to making your application code more expressive.
```

### The `null` "mistake"

- pointers (naturally occurring due to memory and concept of storing address pointing to somewhere else, cannot avoid, can hide in sufficiently high-level language)
- references (need not occur due to syntax enforcing reference is always taken from existing object `TODO: correct?`)

... `TODO: not a billion-dollar mistake but instead a naturally-emerging, necessary construct to represent absence at the level where we cannot ignore/dismiss/deny the possibility`

In a later section we will discuss the uses of `null` and how to reason about `null` to use it correctly.

`TODO: null references <https://www.youtube.com/watch?v=YYkOWzrO3xg>`

`TODO: sufficiently low-level that the concept of pointers and thus addresses exist. Then, before an actual address is known, there must be some replacement value stored, i.e. the 0-value (null) Hence naturally emerging.`

### High-level languages vs expressivity

```
Stateless -> pure functions, immutable variables, ...
Side-effect constructs -> everything that cannot be stateless, such as console output, file I/O, network I/O, etc.
on same spectrum: functional --> procedural --> data-oriented (OOP)
high-level languages tackle more concerns, but to be efficient in programming, one needs to write fewer lines. This can already be accomplished with the right modules/functions.
```

### Utilities (usage logic)

Previously, we discussed the two levels of access to state: _privileged_ and _usage_. We previously mentioned that we expect the _privileged_ logic to be minimal, only that logic which cannot work without privileged access to (internal) state. Everything else is _usage_ logic.

Now there is logic that manipulates data for the specific purpose of the business application, and there is logic that is a common sequence of logic captured in a function for easy re-use. The former kind is specific to your one purpose of the one application, so it makes little sense to make it easily accessible/reusable. The latter kind, for example string manipulations such as splitting, joining, repeating, replacing, etc., can be useful in many applications and as such should be made easily accessible to many.

For this purpose, it is important to ensure two things:

- you can identify utility logic,
- you know how to determine the obvious, predictable place for this utility.

Utility logic are the sequences of logic that are application-independent, i.e. not mutating business data. And, in addition, do not require privileged access to state. Typically they are small sequences of function calls or value manipulations, that can be extracted from larger sets of business logic.

Right now there do not seem to be clear recommendations for where to find/put utilities. My personal recommendation is simple and based on the data type and the utility logic itself:

1. determine the most general interface/type to which this logic may be applied,
1. define a source file/module for this type at an obvious location, (use naming patterns as recommended by the programming language conventions)
1. define a function using a name that describes the utility function best and put the logic there.
1. utilities are typically small enough to apply to a single type, so if this is not the case for your logic, consider if the utility needs to be split up into multiple utilities for different types.

Determining the most general type ensures that the utility can be applied to as many types as possible. Determining the location based on the most general type ensures that anyone can _easily locate_ the utility. Utilities are typically small because your goal is to make the small sequences of manipulations on a particular type concise and reusable. The idea is to make business logic read like high-level code. To accomplish this is easy if a single line describes a single-purpose elaborate manipulation. Single purpose is significant as this makes the utility reusable.

```
TODO:
Possible locations:

- default method on interface
- static method on original type (possible, but cannot be extended, part of type i.s.o. collected with other utils so by definition incomplete, violates "my" rules for classes)
- utility class
- utility class in utility library

1. Utilities for single type (immediate usage patterns)
1. Utilities for interop of multiple types, conversion between types, etc. (indirect? 2nd-level? ...?)  
   Can we call these utilities? (given interop, is there a specific functional domain involved that isn't just the subject-type?)
```


#### Extending the original type

First-class support:

- extension methods (C#)
- default methods (Java)
  - methods on interfaces are by definition utilities, because there cannot be privileged access as there is no implementation. These methods can be non-static, but given only access to public API it is equivalent to static methods, except for the one detail of being able to satisfy an interface definition that requires a specific method to be present even if not strictly necessary for the implementation to function.
- Extensions (Kotlin)

Simulated:

- monkey-patched into the type (Python)

```
extension methods, method extensions, default methods, utility libraries, utility classes, etc. All mechanisms to offer utility ("usage") logic for existing data types.
ideally these methods are available on the type itself, however it is hard to make this exhaustive. Various mechanisms such as default methods, method extensions/extension methods, etc.
Various approaches to making utilities easily accessible, locateable. Mechanism for adding utilities after the fact. Others introduce utilities in separate libraries, separate arbitrarily-named packages/modules, etc.

Utility methods can test for specific derived type and call specialized utility for improved handling.
```

## Functional programming vs procedural programming

`TODO: this will need serious correctness checking. Will be working on many assumptions that are at most educated-guess/extrapolation level. ... and I'm probably insulting 90% of the functional programming crowd :-P`

`TODO: functional closures are (formally) equivalent to objects in OOP-structured  programming. (Probably because of pushing variables away from logic into parameters or once determined in (stack) memory not as parameter but merely accessible.)`

## Thorough understanding

```TODO
- Build upon (solid) foundation of knowledge.
- Always learn new things with relation to known base of knowledge:
  - ensures new information can be positioned inside your framework of knowledge ... relations/derivations/etc.
  - ...
- Evaluate (simplify, generalize) in order to produce knowledge of maximum usefulness:
  - simplify: suitable to hacking mentality-type discovery; and
  - generalize: ensure widest possible breadth of application.
  - re-evaluate known foundations and rearrange knowledge, experiment/evaluate new insights from simplifications/generalizations.
```

```TODO
if-less programming through each individual if-bodies implemented as class, injected through polymorphism. (As opposed to enum + if-"chain", but forgets about just calling different functions, which is even simpler and more straight-forward)
```



[youtube-rustconf-catherine-west]: <https://www.youtube.com/watch?v=aKLntZcp27M> "YouTube: RustConf 2018 - Closing Keynote - Using Rust For Game Development by Catherine West"
[email-alan-kay-oop]: <http://www.purl.org/stefan_ram/pub/doc_kay_oop_en> "Conversation: Dr. Alan Kay on the Meaning of “Object-Oriented Programming”"
[wikipedia-software-design-patterns]: <https://en.wikipedia.org/wiki/Software_design_pattern> "Wikipedia: Software design pattern"
[book-GoF-design-patterns]: <https://en.wikipedia.org/wiki/Software_design_pattern> "Design Patterns: Elements of Reusable Object-Oriented Software (1994)"
