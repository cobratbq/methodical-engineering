# Current state

The current state of software engineering. This is a summary of my own experiences and invariably whoever is reading this will have had different experiences. Over the whole, though, it should give an reasonable representation of software engineering.

## Problem statement

`TODO: ...`

## Requirements

```TODO
- scope
- relevant requirements (business, architecture, ...)
- trade-offs / prioritization (list => single entries)
```

## Solution design

```
- difficulty setting up design and clean structure
- decisions for requirements, trade-offs and/or boundary conditions and/or part relevant to the solution and how it will be resolved.
```

## Programming

### Utilities

```TODO
- Utilities are often extracted arbitrarily.
- Good separation of concerns is difficult, often mixed in available functions.
- Sometimes difficult to separate domain-concerns from utility logic, causing utilities to be domain-specific even if for very general types such as programming language primitives.
- Difficult to locate utilities, hence multiple utils locations exist with same utility logic.
- Existing wide-spread used general utility logic libraries are competing with each-other, have slightly different usage, ...
  - both competing incarnations,
  - as well as multiple version of same utility library/vendor: competing with different implementations of competing/alternative logic.
```
  
### Context and Dependency Injection

In programs, especially for the likes of enterprise application servers, many smaller applications are hosted on a shared platform.

These shared platforms exists to facilitate many common concerns of applications, such as networking, persistence layers, transactionality, and more. These are part of the application server which in itself is unattainable to the hosted program for purpose of enhanced security and the need for control to isolate the many programs that may access these shared resources. Instead, there is a controlled mechanism for injecting exactly the requested resources into an application at start-up, known as _context and dependency injection_ (_CDI_). The name is self-explanatory. These resources and dependencies, that are provided by the server-environment, are injected into the program for use. The exact resources that are injected are specially crafted objects that ensure proper isolation, thread-safety and restriction.

The mechanism, however, is often misused and abused for dependency injection within scope of the program, for which the _dependency injection_-pattern is sufficient. In addition, the mechanism is often used in the manner in which dependencies are injected directly into private fields. This negates the ability for constructors to enforce state invariants, as at construction time fields are not yet injected, thus the invariants cannot hold.

By negating the protection mechanisms that make encapsulation work:

1. enforce field verification,
1. enforce state invariants,
1. prevent `null` values for _required_ fields,
1. control which implementation of a type is provided (in case of polymorphism, multiple viable implementations)

Furthermore, many times an object that relies on dependency injection (frameworks) cannot be instantiated without them, due to "reflection"-magic and annotations that need to be processed through such a framework.

The way CDI can be used, is to inject dependencies _through_ the constructor, i.e. as parameters. Or, if dependencies are optional, through their respective _setter_-methods. Therefore both through constructor and setters can state invariants be enforced, and the mechanisms are equally accessible through "manual" use, i.e. without dependency injection framework. In many cases, the _dependency injection_-pattern is superior as it makes dependencies explicit in syntax, so you get support from the programming language and compiler. (Note that it may not look as pretty, as more things are made explicit that are otherwise implicit/invisible.)

The only valid reason to construct invalid objects, would be if one wants to reflect corrupted data, e.g. a database-record or encoded data, as memory in the program. However, this should not be the norm. In addition, one should consider that such cases require simple _struct_ types (C, C++, ...) or Java 15's _record_ types, which do not typically enforce state invariants and are intended merely as data carries.

### Error handling

```
- Unavoidably "ah these annoying additional error paths that we also need to handle in some way" and "why am I handling this it cannot even happen, or I don't even know what I can do to mitigate this"
- when handled wrongly, can hide errors, can even hide unsuccessful execution, in worst case causing (silent/unidentified) corruption in the process.
- the only way to _not_ handle errors, is to freeze execution at point of occurrence. Even crashing the application is a choice of handling.
```

## Documentation

```  
- often stored in arbitrary storage systems (such as enterprise CMS) away from its intended goal.
- design documentation versioned independent of corresponding program.
- documentation version/content does not match 1:1 with program version.
- documentation not dedicated to the intended audience.
- documentation created/maintained in a format that is incompatible and inconvenient to the target audience/program.
- Goals:
  - Design:
    - intended audience: technical, developers, designers, stakeholders requiring rationale on decisions.
    - textual reference/reflection of the program,
    - graphics (diagrams, charts,  etc.) to illustrate more complicated to express structures,
    - reflects the current state of the program, (past and future are irrelevant)
    - should be easy to read/reference,
    - should be easy to update,
    - should capture all information, such as decisions, rationale, requirements, scoping, concerns, context, that is relevant to the program - leads towards the program as the end result - but is not visible/extractable from the program. (Typically any information leading to the program in its current incarnation.)
  - Business/Architecture requirements:
    - intended audience: functional knowledge => business/architecture
    - independent of single application,
    - describes (relevant concerns of) the larger context,
    - requirements should be devoid of design concerns, should aim for larger non-technical goals instead,
    - ...
```

## Metrics

```
- the current state: we know we need metrics. We have a rough idea which metrics would be useful. We cannot clearly quantify and bound/limit the metrics.
- Metrics:
  - lines of code
  - lines of comments
  - complexity (cyclometic complexity)
  - test coverage
  - number of branches
  - 
```

## CDI

```TODO
- very often field injection, in some occasions setter-injection, very rarely constructor injection. However, constructor injection is the variant that makes most sense by a long shot.
- many implementations of CDI.
- many developers follow the bad examples.
- specific use case for CDI field injection.
- setter-injection allows guarding the set value, but only if value is intended to be modified (set called repeatedly).
- constructor injection allows guarding the value even if value can only be set at construction time. This is the variant that matches with programming language nature most accurately.
...
```

## Notes

- (Context) Dependency Injection: constructor > setter > field, preference determined by amount of control over state that is lost. (constructor injection respects the programming language fully)
- Outline:
  - Requirements (functional, operational, developmental; a.k.a. business and architecture)
    - careful consideration of what applies to this problem, problem statement.
  - Design
    - modeling,
    - selection of applicable requirements, reinterpretation if needed to make suitable for problem statement/solution,
    - decisions, boundary conditions, limitations to current implementation,
  - Implementation / unit testing
    - programming language,
    - IDE,
    - build tooling (interpreter/compiler/linker, static analysis, transformation, ...)
    - continuous integration (testing, analysis, quality gates, quality metrics, ...)
  - Verification / validation
  - Deployment
    - 

- Design vs. architecture:
  - How to make the distinction?
  - How to protect the area of responsibility of design against architecture?
  - How to defend and argue on the area of design against architecture?- Programming:
  - Various models to model architecture levels, etc. However, not everything is solved with such models, because the models themselves do not have strict semantics. There is arguable freedom and the same freedom is what causes confusion, friction, discussion during projects.
- Error-handling:
  - consistent error handling.
  - "lossless" error handling. `TODO: WTF is "lossless"???`
  - common understanding of error handling, ensuring no negative consequences.
  - consequences of bad error handling.
    - silencing, silent errors, unpredictable behavior, undefined behavior, bad behavior, loss of trust in application.
  - handling
    - workaround / mitigate / ...
    - abort / crash
- `null`-handling
  - Trouble handling `null` values. Issues with actual semantics, how to use, how to detect misuse, how to avoid excessive testing, what to do if encountered in inconvenient situation?
  - Bad understanding of NPE: misconception, bad preconceptions.
  - utilities that always internally also check `null`
  - `null` analysis available but not often used.
    - including non-null by default for parameters.
  - compile-time null analysis checking (ErrorProne + NullAway)
    - including non-null for everything.
  - Fear of NPE (too often encountered, too hard to debug due to delayed consequences)
- Exceptions
  - trouble getting grips with many (types of) exceptions being thrown.
  - delayed exceptions due to `null` accidentally being stored and only later found out to be `null`.
  - confusion/misconceptions between checked and unchecked exceptions.
  - mentality missing for how to behave around unchecked exceptions, crashing.
  - lost/conflicted in how to mitigate exceptions that happen under circumstances where crashing is not acceptable. Need realization that there are different, conflicting requirements at work. Realization that there is an architectural boundary and a change of exception handling strategy.
- Programming:
  - too little effort on extracting utility logic, meaning that lots of elementary-level logic is littered between business logic. Therefore, business logic is lengthy, low expressivity, tedious to read. -> cohesion/coupling issues, separation of concerns issues, ...
  - hard to keep in control over growing code-base, multiple developers.
  - hard to preserve consistent coding style.
  - (too) low bar for quality (often, not even reaching compiler recommendation)
  - `TODO: ...`
- High-level, expressive programming, utilities:
  - where to find them?
  - when to write them?
  - what are they exactly?
  - how are utilities classified? / when does logic qualify as utility?
  - write yourself of download library? Which library, as there are multiple?
  - Separation of business logic and utility logic.
  - More expressive, high-level programming through use of readily-available utilities. Requires having utilities.
- manual style formatting / style conventions per code-base

ideation - define context - gather information about the problem - define problem statement - define scope - gather requirements from business - gather requirements from architecture - select/filter requirements on applicability - make selection of requirements to fit problem statement - make selection of requirements to fit scope - decide on solution direction based on problem statement and requirements - design solution taking into account everything already present in application - make decisions on design-level issues - verify design (internally consistent) - validate design against requirements - figure out how implementation can be validated against design - implement design - verify implementation - validate implementation against design - define deployment procedure - ... - maintenance - ... - deprecation - ...

## References

- 
- ...
