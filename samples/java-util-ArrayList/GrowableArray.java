package java.util;

import jdk.internal.util.ArraysSupport;

final class GrowableArray<E> {

    /**
     * Shared empty array instance used for empty instances.
     */
    private static final Object[] EMPTY_ELEMENTDATA = {};

    /**
     * The array buffer into which the elements of the ArrayList are stored.
     * The capacity of the ArrayList is the length of this array buffer. Any
     * empty ArrayList with elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA
     * will be expanded to DEFAULT_CAPACITY when the first element is added.
     */
    private transient Object[] elementData; // non-private to simplify nested class access

    public GrowableArray(final int initialCapacity) {
        if (initialCapacity > 0) {
            this.elementData = new Object[initialCapacity];
        } else if (initialCapacity == 0) {
            this.elementData = EMPTY_ELEMENTDATA;
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
    }

    public GrowableArray(final Object[] a) {
        this.elementData = Objects.requireNonNull(a);
    }

    /**
     * Provide direct access to the backing array without allowing it to be replaced by another instance.
     *
     * @return the element data array.
     */
    public Object[] data() {
        return this.elementData;
    }

    /**
     * Trims the capacity of this {@code ArrayList} instance to be the
     * list's current size.  An application can use this operation to minimize
     * the storage of an {@code ArrayList} instance.
     */
    public void trim(final int size) {
        if (size < elementData.length) {
            elementData = (size == 0) ? EMPTY_ELEMENTDATA : Arrays.copyOf(elementData, size);
        }
    }

    /**
     * Increases the capacity to ensure that it can hold at least the
     * number of elements specified by the minimum capacity argument.
     *
     * @param minCapacity the desired minimum capacity
     * @throws OutOfMemoryError if minCapacity is less than zero
     */
    public void grow(int minCapacity) {
        if (elementData.length == 0) {
            elementData = new Object[minCapacity];
            return;
        }
        final int newCapacity = ArraysSupport.newLength(elementData.length,
                minCapacity - elementData.length, /* minimum growth */
                elementData.length >> 1           /* preferred growth */);
        elementData = Arrays.copyOf(elementData, newCapacity);
    }
}