diff --git a/src/java.base/share/classes/java/util/GrowingTable.java b/src/java.base/share/classes/java/util/GrowingTable.java
new file mode 100644
index 00000000000..94c453494b6
--- /dev/null
+++ b/src/java.base/share/classes/java/util/GrowingTable.java
@@ -0,0 +1,195 @@
+package java.util;
+
+final class GrowingTable<K, V> {
+
+    /**
+     * The default initial capacity - MUST be a power of two.
+     */
+    static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16
+
+    /**
+     * The maximum capacity, used if a higher value is implicitly specified
+     * by either of the constructors with arguments.
+     * MUST be a power of two <= 1<<30.
+     */
+    static final int MAXIMUM_CAPACITY = 1 << 30;
+
+    /**
+     * The load factor used when none specified in constructor.
+     */
+    static final float DEFAULT_LOAD_FACTOR = 0.75f;
+
+    /**
+     * The load factor for the hash table.
+     *
+     * @serial
+     */
+    private final float loadFactor;
+
+    /**
+     * The next size value at which to resize (capacity * load factor).
+     *
+     * @serial
+     */
+    // (The javadoc description is true upon serialization.
+    // Additionally, if the table array has not been allocated, this
+    // field holds the initial array capacity, or zero signifying
+    // DEFAULT_INITIAL_CAPACITY.)
+    private int threshold;
+
+    /**
+     * The table, initialized on first use, and resized as
+     * necessary. When allocated, length is always a power of two.
+     * (We also tolerate length zero in some operations to allow
+     * bootstrapping mechanics that are currently not needed.)
+     */
+    private transient Node<K,V>[] table;
+
+    public GrowingTable() {
+        this(DEFAULT_LOAD_FACTOR, DEFAULT_INITIAL_CAPACITY);
+    }
+
+    public GrowingTable(final int threshold) {
+        this(DEFAULT_LOAD_FACTOR, threshold);
+    }
+
+    public GrowingTable(final float loadFactor, int initialCapacity) {
+        if (initialCapacity < 0)
+            throw new IllegalArgumentException("Illegal initial capacity: " +
+                    initialCapacity);
+        if (initialCapacity > MAXIMUM_CAPACITY)
+            initialCapacity = MAXIMUM_CAPACITY;
+        if (loadFactor <= 0 || Float.isNaN(loadFactor))
+            throw new IllegalArgumentException("Illegal load factor: " +
+                    loadFactor);
+        this.loadFactor = loadFactor;
+        this.threshold = tableSizeFor(initialCapacity);
+    }
+
+    // These methods are also used when serializing HashSets
+    public final float loadFactor() {
+        return loadFactor;
+    }
+
+    public final int threshold() {
+        return this.threshold;
+    }
+    
+    public final int capacity() {
+        return (table != null) ? table.length
+                : (threshold > 0) ? threshold
+                : DEFAULT_INITIAL_CAPACITY;
+    }
+
+    public Node<K,V>[] data() {
+        return this.table;
+    }
+
+    public void updateThreshold(final int s) {
+        if (table != null) {
+            throw new IllegalStateException("Table already created. Arbitrary updating threshold is no longer permitted.");
+        }
+        float ft = ((float) s / loadFactor) + 1.0F;
+        int t = ((ft < (float) MAXIMUM_CAPACITY) ? (int) ft : MAXIMUM_CAPACITY);
+        if (t > threshold) {
+            threshold = tableSizeFor(t);
+        }
+    }
+
+    /**
+     * Initializes or doubles table size.  If null, allocates in
+     * accord with initial capacity target held in field threshold.
+     * Otherwise, because we are using power-of-two expansion, the
+     * elements from each bin must either stay at same index, or move
+     * with a power of two offset in the new table.
+     *
+     * @return the table
+     */
+    final Node<K,V>[] resize() {
+        Node<K,V>[] oldTab = table;
+        int oldCap = (oldTab == null) ? 0 : oldTab.length;
+        int oldThr = threshold;
+        int newCap, newThr = 0;
+        if (oldCap > 0) {
+            if (oldCap >= MAXIMUM_CAPACITY) {
+                threshold = Integer.MAX_VALUE;
+                return oldTab;
+            }
+            else if ((newCap = oldCap << 1) < MAXIMUM_CAPACITY &&
+                    oldCap >= DEFAULT_INITIAL_CAPACITY)
+                newThr = oldThr << 1; // double threshold
+        }
+        else if (oldThr > 0) // initial capacity was placed in threshold
+            newCap = oldThr;
+        else {               // zero initial threshold signifies using defaults
+            newCap = DEFAULT_INITIAL_CAPACITY;
+            newThr = (int)(DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
+        }
+        if (newThr == 0) {
+            float ft = (float)newCap * loadFactor;
+            newThr = (newCap < MAXIMUM_CAPACITY && ft < (float)MAXIMUM_CAPACITY ?
+                    (int)ft : Integer.MAX_VALUE);
+        }
+        threshold = newThr;
+        @SuppressWarnings({"rawtypes","unchecked"})
+        Node<K,V>[] newTab = (Node<K,V>[])new Node[newCap];
+        return table = newTab;
+    }
+
+    /**
+     * Returns a power of two size for the given target capacity.
+     */
+    private static int tableSizeFor(int cap) {
+        int n = -1 >>> Integer.numberOfLeadingZeros(cap - 1);
+        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
+    }
+
+    public void reinitialize() {
+        this.table = null;
+        this.threshold = 0;
+    }
+
+    /**
+     * Basic hash bin node, used for most entries.  (See below for
+     * TreeNode subclass, and in LinkedHashMap for its Entry subclass.)
+     */
+    static class Node<K,V> implements Map.Entry<K,V> {
+        final int hash;
+        final K key;
+        V value;
+        Node<K,V> next;
+
+        Node(int hash, K key, V value, Node<K,V> next) {
+            this.hash = hash;
+            this.key = key;
+            this.value = value;
+            this.next = next;
+        }
+
+        public final K getKey()        { return key; }
+        public final V getValue()      { return value; }
+        public final String toString() { return key + "=" + value; }
+
+        public final int hashCode() {
+            return Objects.hashCode(key) ^ Objects.hashCode(value);
+        }
+
+        public final V setValue(V newValue) {
+            V oldValue = value;
+            value = newValue;
+            return oldValue;
+        }
+
+        public final boolean equals(Object o) {
+            if (o == this)
+                return true;
+            if (o instanceof Map.Entry) {
+                Map.Entry<?,?> e = (Map.Entry<?,?>)o;
+                if (Objects.equals(key, e.getKey()) &&
+                        Objects.equals(value, e.getValue()))
+                    return true;
+            }
+            return false;
+        }
+    }
+}
diff --git a/src/java.base/share/classes/java/util/HashMap.java b/src/java.base/share/classes/java/util/HashMap.java
index 77743d680cc..fecdd36fdcb 100644
--- a/src/java.base/share/classes/java/util/HashMap.java
+++ b/src/java.base/share/classes/java/util/HashMap.java
@@ -30,6 +30,7 @@ import java.io.InvalidObjectException;
 import java.io.Serializable;
 import java.lang.reflect.ParameterizedType;
 import java.lang.reflect.Type;
+import java.util.GrowingTable.Node;
 import java.util.function.BiConsumer;
 import java.util.function.BiFunction;
 import java.util.function.Consumer;
@@ -231,23 +232,6 @@ public class HashMap<K,V> extends AbstractMap<K,V>
      * avoid aliasing errors amid all of the twisty pointer operations.
      */
 
-    /**
-     * The default initial capacity - MUST be a power of two.
-     */
-    static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16
-
-    /**
-     * The maximum capacity, used if a higher value is implicitly specified
-     * by either of the constructors with arguments.
-     * MUST be a power of two <= 1<<30.
-     */
-    static final int MAXIMUM_CAPACITY = 1 << 30;
-
-    /**
-     * The load factor used when none specified in constructor.
-     */
-    static final float DEFAULT_LOAD_FACTOR = 0.75f;
-
     /**
      * The bin count threshold for using a tree rather than list for a
      * bin.  Bins are converted to trees when adding an element to a
@@ -273,50 +257,6 @@ public class HashMap<K,V> extends AbstractMap<K,V>
      */
     static final int MIN_TREEIFY_CAPACITY = 64;
 
-    /**
-     * Basic hash bin node, used for most entries.  (See below for
-     * TreeNode subclass, and in LinkedHashMap for its Entry subclass.)
-     */
-    static class Node<K,V> implements Map.Entry<K,V> {
-        final int hash;
-        final K key;
-        V value;
-        Node<K,V> next;
-
-        Node(int hash, K key, V value, Node<K,V> next) {
-            this.hash = hash;
-            this.key = key;
-            this.value = value;
-            this.next = next;
-        }
-
-        public final K getKey()        { return key; }
-        public final V getValue()      { return value; }
-        public final String toString() { return key + "=" + value; }
-
-        public final int hashCode() {
-            return Objects.hashCode(key) ^ Objects.hashCode(value);
-        }
-
-        public final V setValue(V newValue) {
-            V oldValue = value;
-            value = newValue;
-            return oldValue;
-        }
-
-        public final boolean equals(Object o) {
-            if (o == this)
-                return true;
-            if (o instanceof Map.Entry) {
-                Map.Entry<?,?> e = (Map.Entry<?,?>)o;
-                if (Objects.equals(key, e.getKey()) &&
-                    Objects.equals(value, e.getValue()))
-                    return true;
-            }
-            return false;
-        }
-    }
-
     /* ---------------- Static utilities -------------- */
 
     /**
@@ -373,23 +313,9 @@ public class HashMap<K,V> extends AbstractMap<K,V>
                 ((Comparable)k).compareTo(x));
     }
 
-    /**
-     * Returns a power of two size for the given target capacity.
-     */
-    static final int tableSizeFor(int cap) {
-        int n = -1 >>> Integer.numberOfLeadingZeros(cap - 1);
-        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
-    }
-
     /* ---------------- Fields -------------- */
 
-    /**
-     * The table, initialized on first use, and resized as
-     * necessary. When allocated, length is always a power of two.
-     * (We also tolerate length zero in some operations to allow
-     * bootstrapping mechanics that are currently not needed.)
-     */
-    transient Node<K,V>[] table;
+    private final GrowingTable<K,V> table;
 
     /**
      * Holds cached entrySet(). Note that AbstractMap fields are used
@@ -411,24 +337,6 @@ public class HashMap<K,V> extends AbstractMap<K,V>
      */
     transient int modCount;
 
-    /**
-     * The next size value at which to resize (capacity * load factor).
-     *
-     * @serial
-     */
-    // (The javadoc description is true upon serialization.
-    // Additionally, if the table array has not been allocated, this
-    // field holds the initial array capacity, or zero signifying
-    // DEFAULT_INITIAL_CAPACITY.)
-    int threshold;
-
-    /**
-     * The load factor for the hash table.
-     *
-     * @serial
-     */
-    final float loadFactor;
-
     /* ---------------- Public operations -------------- */
 
     /**
@@ -441,16 +349,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
      *         or the load factor is nonpositive
      */
     public HashMap(int initialCapacity, float loadFactor) {
-        if (initialCapacity < 0)
-            throw new IllegalArgumentException("Illegal initial capacity: " +
-                                               initialCapacity);
-        if (initialCapacity > MAXIMUM_CAPACITY)
-            initialCapacity = MAXIMUM_CAPACITY;
-        if (loadFactor <= 0 || Float.isNaN(loadFactor))
-            throw new IllegalArgumentException("Illegal load factor: " +
-                                               loadFactor);
-        this.loadFactor = loadFactor;
-        this.threshold = tableSizeFor(initialCapacity);
+        this.table = new GrowingTable<>(loadFactor, initialCapacity);
     }
 
     /**
@@ -461,7 +360,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
      * @throws IllegalArgumentException if the initial capacity is negative.
      */
     public HashMap(int initialCapacity) {
-        this(initialCapacity, DEFAULT_LOAD_FACTOR);
+        this.table = new GrowingTable<>(initialCapacity);
     }
 
     /**
@@ -469,7 +368,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
      * (16) and the default load factor (0.75).
      */
     public HashMap() {
-        this.loadFactor = DEFAULT_LOAD_FACTOR; // all other fields defaulted
+        this.table = new GrowingTable<>();
     }
 
     /**
@@ -482,7 +381,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
      * @throws  NullPointerException if the specified map is null
      */
     public HashMap(Map<? extends K, ? extends V> m) {
-        this.loadFactor = DEFAULT_LOAD_FACTOR;
+        this.table = new GrowingTable<>();
         putMapEntries(m, false);
     }
 
@@ -496,25 +395,22 @@ public class HashMap<K,V> extends AbstractMap<K,V>
     final void putMapEntries(Map<? extends K, ? extends V> m, boolean evict) {
         int s = m.size();
         if (s > 0) {
-            if (table == null) { // pre-size
-                float ft = ((float)s / loadFactor) + 1.0F;
-                int t = ((ft < (float)MAXIMUM_CAPACITY) ?
-                         (int)ft : MAXIMUM_CAPACITY);
-                if (t > threshold)
-                    threshold = tableSizeFor(t);
+            if (table.data() == null) { // pre-size
+                table.updateThreshold(s);
             } else {
                 // Because of linked-list bucket constraints, we cannot
                 // expand all at once, but can reduce total resize
                 // effort by repeated doubling now vs later
-                while (s > threshold && table.length < MAXIMUM_CAPACITY)
+                while (s > table.threshold()
+                        && table.data().length < GrowingTable.MAXIMUM_CAPACITY)
                     resize();
             }
+        }
 
-            for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
-                K key = e.getKey();
-                V value = e.getValue();
-                putVal(hash(key), key, value, false, evict);
-            }
+        for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
+            K key = e.getKey();
+            V value = e.getValue();
+            putVal(hash(key), key, value, false, evict);
         }
     }
 
@@ -566,21 +462,23 @@ public class HashMap<K,V> extends AbstractMap<K,V>
      */
     final Node<K,V> getNode(Object key) {
         Node<K,V>[] tab; Node<K,V> first, e; int n, hash; K k;
-        if ((tab = table) != null && (n = tab.length) > 0 &&
-            (first = tab[(n - 1) & (hash = hash(key))]) != null) {
-            if (first.hash == hash && // always check first node
-                ((k = first.key) == key || (key != null && key.equals(k))))
-                return first;
-            if ((e = first.next) != null) {
-                if (first instanceof TreeNode)
-                    return ((TreeNode<K,V>)first).getTreeNode(hash, key);
-                do {
-                    if (e.hash == hash &&
-                        ((k = e.key) == key || (key != null && key.equals(k))))
-                        return e;
-                } while ((e = e.next) != null);
-            }
+        if ((tab = table.data()) == null || (n = tab.length) <= 0 ||
+                (first = tab[(n - 1) & (hash = hash(key))]) == null) {
+            return null;
         }
+        if (first.hash == hash && // always check first node
+            ((k = first.key) == key || (key != null && key.equals(k))))
+            return first;
+        if ((e = first.next) == null) {
+            return null;
+        }
+        if (first instanceof TreeNode)
+            return ((TreeNode<K,V>)first).getTreeNode(hash, key);
+        do {
+            if (e.hash == hash &&
+                ((k = e.key) == key || (key != null && key.equals(k))))
+                return e;
+        } while ((e = e.next) != null);
         return null;
     }
 
@@ -625,7 +523,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
     final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
                    boolean evict) {
         Node<K,V>[] tab; Node<K,V> p; int n, i;
-        if ((tab = table) == null || (n = tab.length) == 0)
+        if ((tab = table.data()) == null || (n = tab.length) == 0)
             n = (tab = resize()).length;
         if ((p = tab[i = (n - 1) & hash]) == null)
             tab[i] = newNode(hash, key, value, null);
@@ -659,93 +557,64 @@ public class HashMap<K,V> extends AbstractMap<K,V>
             }
         }
         ++modCount;
-        if (++size > threshold)
+        if (++size > table.threshold())
             resize();
         afterNodeInsertion(evict);
         return null;
     }
 
-    /**
-     * Initializes or doubles table size.  If null, allocates in
-     * accord with initial capacity target held in field threshold.
-     * Otherwise, because we are using power-of-two expansion, the
-     * elements from each bin must either stay at same index, or move
-     * with a power of two offset in the new table.
-     *
-     * @return the table
-     */
     final Node<K,V>[] resize() {
-        Node<K,V>[] oldTab = table;
-        int oldCap = (oldTab == null) ? 0 : oldTab.length;
-        int oldThr = threshold;
-        int newCap, newThr = 0;
-        if (oldCap > 0) {
-            if (oldCap >= MAXIMUM_CAPACITY) {
-                threshold = Integer.MAX_VALUE;
-                return oldTab;
+        final Node<K, V>[] oldTab = table.data();
+        final Node<K, V>[] newTab = table.resize();
+        if (oldTab == null || oldTab == newTab) {
+            return newTab;
+        }
+        transfer(oldTab, newTab);
+        return newTab;
+    }
+    
+    private void transfer(final Node<K,V>[] oldTab, final Node<K,V>[] newTab) {
+        Node<K,V> e;
+        for (int j = 0; j < oldTab.length; ++j) {
+            if ((e = oldTab[j]) == null) {
+                continue;
             }
-            else if ((newCap = oldCap << 1) < MAXIMUM_CAPACITY &&
-                     oldCap >= DEFAULT_INITIAL_CAPACITY)
-                newThr = oldThr << 1; // double threshold
-        }
-        else if (oldThr > 0) // initial capacity was placed in threshold
-            newCap = oldThr;
-        else {               // zero initial threshold signifies using defaults
-            newCap = DEFAULT_INITIAL_CAPACITY;
-            newThr = (int)(DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
-        }
-        if (newThr == 0) {
-            float ft = (float)newCap * loadFactor;
-            newThr = (newCap < MAXIMUM_CAPACITY && ft < (float)MAXIMUM_CAPACITY ?
-                      (int)ft : Integer.MAX_VALUE);
-        }
-        threshold = newThr;
-        @SuppressWarnings({"rawtypes","unchecked"})
-        Node<K,V>[] newTab = (Node<K,V>[])new Node[newCap];
-        table = newTab;
-        if (oldTab != null) {
-            for (int j = 0; j < oldCap; ++j) {
-                Node<K,V> e;
-                if ((e = oldTab[j]) != null) {
-                    oldTab[j] = null;
-                    if (e.next == null)
-                        newTab[e.hash & (newCap - 1)] = e;
-                    else if (e instanceof TreeNode)
-                        ((TreeNode<K,V>)e).split(this, newTab, j, oldCap);
-                    else { // preserve order
-                        Node<K,V> loHead = null, loTail = null;
-                        Node<K,V> hiHead = null, hiTail = null;
-                        Node<K,V> next;
-                        do {
-                            next = e.next;
-                            if ((e.hash & oldCap) == 0) {
-                                if (loTail == null)
-                                    loHead = e;
-                                else
-                                    loTail.next = e;
-                                loTail = e;
-                            }
-                            else {
-                                if (hiTail == null)
-                                    hiHead = e;
-                                else
-                                    hiTail.next = e;
-                                hiTail = e;
-                            }
-                        } while ((e = next) != null);
-                        if (loTail != null) {
-                            loTail.next = null;
-                            newTab[j] = loHead;
-                        }
-                        if (hiTail != null) {
-                            hiTail.next = null;
-                            newTab[j + oldCap] = hiHead;
-                        }
+            oldTab[j] = null;
+            if (e.next == null) {
+                newTab[e.hash & (newTab.length - 1)] = e;
+            } else if (e instanceof HashMap.TreeNode) {
+                ((HashMap.TreeNode<K,V>)e).split(this, newTab, j, oldTab.length);
+            } else { // preserve order
+                Node<K,V> loHead = null, loTail = null;
+                Node<K,V> hiHead = null, hiTail = null;
+                Node<K,V> next;
+                do {
+                    next = e.next;
+                    if ((e.hash & oldTab.length) == 0) {
+                        if (loTail == null)
+                            loHead = e;
+                        else
+                            loTail.next = e;
+                        loTail = e;
                     }
+                    else {
+                        if (hiTail == null)
+                            hiHead = e;
+                        else
+                            hiTail.next = e;
+                        hiTail = e;
+                    }
+                } while ((e = next) != null);
+                if (loTail != null) {
+                    loTail.next = null;
+                    newTab[j] = loHead;
+                }
+                if (hiTail != null) {
+                    hiTail.next = null;
+                    newTab[j + oldTab.length] = hiHead;
                 }
             }
         }
-        return newTab;
     }
 
     /**
@@ -813,7 +682,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
     final Node<K,V> removeNode(int hash, Object key, Object value,
                                boolean matchValue, boolean movable) {
         Node<K,V>[] tab; Node<K,V> p; int n, index;
-        if ((tab = table) != null && (n = tab.length) > 0 &&
+        if ((tab = table.data()) != null && (n = tab.length) > 0 &&
             (p = tab[index = (n - 1) & hash]) != null) {
             Node<K,V> node = null, e; K k; V v;
             if (p.hash == hash &&
@@ -858,7 +727,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
     public void clear() {
         Node<K,V>[] tab;
         modCount++;
-        if ((tab = table) != null && size > 0) {
+        if ((tab = table.data()) != null && size > 0) {
             size = 0;
             for (int i = 0; i < tab.length; ++i)
                 tab[i] = null;
@@ -875,7 +744,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
      */
     public boolean containsValue(Object value) {
         Node<K,V>[] tab; V v;
-        if ((tab = table) != null && size > 0) {
+        if ((tab = table.data()) != null && size > 0) {
             for (Node<K,V> e : tab) {
                 for (; e != null; e = e.next) {
                     if ((v = e.value) == value ||
@@ -946,7 +815,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
         Object[] r = a;
         Node<K,V>[] tab;
         int idx = 0;
-        if (size > 0 && (tab = table) != null) {
+        if (size > 0 && (tab = table.data()) != null) {
             for (Node<K,V> e : tab) {
                 for (; e != null; e = e.next) {
                     r[idx++] = e.key;
@@ -969,7 +838,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
         Object[] r = a;
         Node<K,V>[] tab;
         int idx = 0;
-        if (size > 0 && (tab = table) != null) {
+        if (size > 0 && (tab = table.data()) != null) {
             for (Node<K,V> e : tab) {
                 for (; e != null; e = e.next) {
                     r[idx++] = e.value;
@@ -1003,7 +872,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
             Node<K,V>[] tab;
             if (action == null)
                 throw new NullPointerException();
-            if (size > 0 && (tab = table) != null) {
+            if (size > 0 && (tab = table.data()) != null) {
                 int mc = modCount;
                 for (Node<K,V> e : tab) {
                     for (; e != null; e = e.next)
@@ -1060,7 +929,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
             Node<K,V>[] tab;
             if (action == null)
                 throw new NullPointerException();
-            if (size > 0 && (tab = table) != null) {
+            if (size > 0 && (tab = table.data()) != null) {
                 int mc = modCount;
                 for (Node<K,V> e : tab) {
                     for (; e != null; e = e.next)
@@ -1123,7 +992,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
             Node<K,V>[] tab;
             if (action == null)
                 throw new NullPointerException();
-            if (size > 0 && (tab = table) != null) {
+            if (size > 0 && (tab = table.data()) != null) {
                 int mc = modCount;
                 for (Node<K,V> e : tab) {
                     for (; e != null; e = e.next)
@@ -1197,7 +1066,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
         int binCount = 0;
         TreeNode<K,V> t = null;
         Node<K,V> old = null;
-        if (size > threshold || (tab = table) == null ||
+        if (size > table.threshold() || (tab = table.data()) == null ||
             (n = tab.length) == 0)
             n = (tab = resize()).length;
         if ((first = tab[i = (n - 1) & hash]) != null) {
@@ -1297,7 +1166,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
         int binCount = 0;
         TreeNode<K,V> t = null;
         Node<K,V> old = null;
-        if (size > threshold || (tab = table) == null ||
+        if (size > table.threshold() || (tab = table.data()) == null ||
             (n = tab.length) == 0)
             n = (tab = resize()).length;
         if ((first = tab[i = (n - 1) & hash]) != null) {
@@ -1362,7 +1231,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
         int binCount = 0;
         TreeNode<K,V> t = null;
         Node<K,V> old = null;
-        if (size > threshold || (tab = table) == null ||
+        if (size > table.threshold() || (tab = table.data()) == null ||
             (n = tab.length) == 0)
             n = (tab = resize()).length;
         if ((first = tab[i = (n - 1) & hash]) != null) {
@@ -1418,7 +1287,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
         Node<K,V>[] tab;
         if (action == null)
             throw new NullPointerException();
-        if (size > 0 && (tab = table) != null) {
+        if (size > 0 && (tab = table.data()) != null) {
             int mc = modCount;
             for (Node<K,V> e : tab) {
                 for (; e != null; e = e.next)
@@ -1434,7 +1303,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
         Node<K,V>[] tab;
         if (function == null)
             throw new NullPointerException();
-        if (size > 0 && (tab = table) != null) {
+        if (size > 0 && (tab = table.data()) != null) {
             int mc = modCount;
             for (Node<K,V> e : tab) {
                 for (; e != null; e = e.next) {
@@ -1469,15 +1338,6 @@ public class HashMap<K,V> extends AbstractMap<K,V>
         result.putMapEntries(this, false);
         return result;
     }
-
-    // These methods are also used when serializing HashSets
-    final float loadFactor() { return loadFactor; }
-    final int capacity() {
-        return (table != null) ? table.length :
-            (threshold > 0) ? threshold :
-            DEFAULT_INITIAL_CAPACITY;
-    }
-
     /**
      * Saves this map to a stream (that is, serializes it).
      *
@@ -1493,7 +1353,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
     @java.io.Serial
     private void writeObject(java.io.ObjectOutputStream s)
         throws IOException {
-        int buckets = capacity();
+        int buckets = table.capacity();
         // Write out the threshold, loadfactor, and any hidden stuff
         s.defaultWriteObject();
         s.writeInt(buckets);
@@ -1565,7 +1425,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
 
         HashIterator() {
             expectedModCount = modCount;
-            Node<K,V>[] t = table;
+            Node<K,V>[] t = table.data();
             current = next = null;
             index = 0;
             if (t != null && size > 0) { // advance to first entry
@@ -1584,7 +1444,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
                 throw new ConcurrentModificationException();
             if (e == null)
                 throw new NoSuchElementException();
-            if ((next = (current = e).next) == null && (t = table) != null) {
+            if ((next = (current = e).next) == null && (t = table.data()) != null) {
                 do {} while (index < t.length && (next = t[index++]) == null);
             }
             return e;
@@ -1644,7 +1504,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
                 HashMap<K,V> m = map;
                 est = m.size;
                 expectedModCount = m.modCount;
-                Node<K,V>[] tab = m.table;
+                Node<K,V>[] tab = m.table.data();
                 hi = fence = (tab == null) ? 0 : tab.length;
             }
             return hi;
@@ -1676,7 +1536,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
             if (action == null)
                 throw new NullPointerException();
             HashMap<K,V> m = map;
-            Node<K,V>[] tab = m.table;
+            Node<K,V>[] tab = m.table.data();
             if ((hi = fence) < 0) {
                 mc = expectedModCount = m.modCount;
                 hi = fence = (tab == null) ? 0 : tab.length;
@@ -1704,7 +1564,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
             int hi;
             if (action == null)
                 throw new NullPointerException();
-            Node<K,V>[] tab = map.table;
+            Node<K,V>[] tab = map.table.data();
             if (tab != null && tab.length >= (hi = getFence()) && index >= 0) {
                 while (current != null || index < hi) {
                     if (current == null)
@@ -1748,7 +1608,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
             if (action == null)
                 throw new NullPointerException();
             HashMap<K,V> m = map;
-            Node<K,V>[] tab = m.table;
+            Node<K,V>[] tab = m.table.data();
             if ((hi = fence) < 0) {
                 mc = expectedModCount = m.modCount;
                 hi = fence = (tab == null) ? 0 : tab.length;
@@ -1776,7 +1636,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
             int hi;
             if (action == null)
                 throw new NullPointerException();
-            Node<K,V>[] tab = map.table;
+            Node<K,V>[] tab = map.table.data();
             if (tab != null && tab.length >= (hi = getFence()) && index >= 0) {
                 while (current != null || index < hi) {
                     if (current == null)
@@ -1819,7 +1679,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
             if (action == null)
                 throw new NullPointerException();
             HashMap<K,V> m = map;
-            Node<K,V>[] tab = m.table;
+            Node<K,V>[] tab = m.table.data();
             if ((hi = fence) < 0) {
                 mc = expectedModCount = m.modCount;
                 hi = fence = (tab == null) ? 0 : tab.length;
@@ -1847,7 +1707,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
             int hi;
             if (action == null)
                 throw new NullPointerException();
-            Node<K,V>[] tab = map.table;
+            Node<K,V>[] tab = map.table.data();
             if (tab != null && tab.length >= (hi = getFence()) && index >= 0) {
                 while (current != null || index < hi) {
                     if (current == null)
@@ -1907,12 +1767,11 @@ public class HashMap<K,V> extends AbstractMap<K,V>
      * Reset to initial default state.  Called by clone and readObject.
      */
     void reinitialize() {
-        table = null;
+        table.reinitialize();
         entrySet = null;
         keySet = null;
         values = null;
         modCount = 0;
-        threshold = 0;
         size = 0;
     }
 
@@ -1924,7 +1783,7 @@ public class HashMap<K,V> extends AbstractMap<K,V>
     // Called only from writeObject, to ensure compatible ordering.
     void internalWriteEntries(java.io.ObjectOutputStream s) throws IOException {
         Node<K,V>[] tab;
-        if (size > 0 && (tab = table) != null) {
+        if (size > 0 && (tab = table.data()) != null) {
             for (Node<K,V> e : tab) {
                 for (; e != null; e = e.next) {
                     s.writeObject(e.key);
diff --git a/src/java.base/share/classes/java/util/LinkedHashMap.java b/src/java.base/share/classes/java/util/LinkedHashMap.java
index 9c40ea8e05c..966fdc98359 100644
--- a/src/java.base/share/classes/java/util/LinkedHashMap.java
+++ b/src/java.base/share/classes/java/util/LinkedHashMap.java
@@ -29,6 +29,7 @@ import java.util.function.Consumer;
 import java.util.function.BiConsumer;
 import java.util.function.BiFunction;
 import java.io.IOException;
+import java.util.GrowingTable.Node;
 
 /**
  * <p>Hash table and linked list implementation of the {@code Map} interface,
@@ -187,9 +188,9 @@ public class LinkedHashMap<K,V>
      */
 
     /**
-     * HashMap.Node subclass for normal LinkedHashMap entries.
+     * GrowingTable.Node subclass for normal LinkedHashMap entries.
      */
-    static class Entry<K,V> extends HashMap.Node<K,V> {
+    static class Entry<K,V> extends GrowingTable.Node<K,V> {
         Entry<K,V> before, after;
         Entry(int hash, K key, V value, Node<K,V> next) {
             super(hash, key, value, next);
