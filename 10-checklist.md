# Checklist

A collection of questions regarding software engineering that allow you to validate a project against a plethora of considerations.

## Organization

- [ ] The boundary between design-level decisions and architectural/business requirements is respected, such that engineers and business/architects respectively each have their own domain of influence.
- [ ] All work items can be represented as a single list of items, with order defining priority. (If not, force definite priority as this is necessary in case of conflict / decision making. Send back to business owners/architects to start discussions/negotiations until a single list is formed. This is a business/architecture problem, not a development/design problem.)

- Teams:
  - [ ] Boundaries for team responsibility is clearly defined.
  - [ ] Given clear boundaries, distinction between _design_ and _architecture_ is known. (Determines team's own responsibility vs. external responsibility.)

## Requirements

The concept of engineering covers both the solution designed for the current problem, as well as the larger context of business requirements, architectural requirements, and to some extent time.

- [ ] Business provides requirements necessary to solve the business problem, including translating the functional domain into appropriate, suitable technical form.
- [ ] Architecture provides requirements necessary to make the solution fit in the larger context of the business, infrastructure, data handling, security, support, legal, etc.
- [ ] All functional/operational/developmental requirements can be represented in a single list. If not, start discussing which requirements take priority.

## Development

The concept of design and implementation of the current problem statement. The solution focuses on now, instead of potential futures and larger contexts.

- [ ] The design is based on _current_ requirements only.
- [ ] A design should respect all requirements that are set. That is, assuming that the requirements respect the boundary between requirements and design.
- [ ] The design describes exactly one solution.
- [ ] What is the obvious location to look for utility logic?

### Encapsulation

- [ ] Classes have minimal set of fields. (According to theory of regions.)
- [ ] Classes have minimal (most restricted) logic necessary to make class useful. (Anything more should be included in utility logic.)
- [ ] All logic of a class accesses privileged logic and/or fields.
- [ ] Private logic is used for reuse of common logic.
- [ ] Concerns needed to enforce proper use, such as state invariants, synchronization, ..., are enforced in public logic.
- [ ] After construction and after each method call by usage logic, state invariants are again respected. (Logic only accessible by other privileged logic need to enforce these state invariants, but may do so.)
- [ ] Usage logic can never corrupt a class instance.

### Context and Dependency Injection

- [ ] Classes that employ CDI are also constructable/usable without CDI framework present.
- [ ] Classes use primarily constructor injection (for required fields) or setter injection (for optional fields).
- [ ] Classes that employ field injection (for `privileged` fields) do not have state invariants.

## Documentation

- [ ] Design documentation is located close to the solution it designs for.
- [ ] Design documentation is aimed primarily at the designers, developers and other stakeholders of the _solution_.
- [ ] The documentation describes the current considerations of the solution, as opposed to the outdated/obsolete past facts or (one of many) potential future predictions.
- [ ] Documentation is in sync with the solution.
- [ ] Documentation documents design decisions (choices, trade-offs) to design-level issues and functional/operational/developmental requirements.

## User-friendliness

`TODO: rewrite and maybe move stuff to appendix.`

- [ ] Fail gracefully, but fail nonetheless:
  - [ ] Establish the failing is a good, ofteh the best, solution to an unfixable/unexpected/unsupported problem.
  - [ ] Will the application crash upon encountering an illegal state?  
    _The alternatives are worse, such as: quietly continuing with bad data, failing at a later point in time with false information and bad pointers towards seemingly correct but actually false causes. Giving false hope to users._
  - [ ] Does the application fail in a way that explains to the user and leaves information for the user to act upon?  
    _Do not fail quietly by simply disappearing. This only gives frustration. Instead, give information of what happened: the crash. Give information about the crash, ideally information that is safe to become public in case of bug reporting. Give information on where to file the bug report. Ideally, just dropping this information in a bug report should help towards fixing, such that users that do not return or do not have the knowledge to actively participate in fixing or providing information, will still have helped simply by reporting._
  - [ ] Make clear whether this is a bug for a feature that is supposed to work, or whether it is a new use case that is actually not supported or not guaranteed to work. This gives an indication for what the user should expect in terms of resolution and timescale.
  - [ ] If there is a lot of set-up effort for the user, support them in recovering. If there is a significant computation time, give them a way to restore an in-progress computation to a particular restore-point. (Make clear where the restore content is, don't make it look like uncontrollable magic. Lets them make a copy in case they want to further experiment with a problematic case.)
- [ ] Warn users in case of actions that have non-obvious/non-straight-forward (side-)effects:
  - risk of making private data public (accidental disclosure). (E.g. stack traces, memory dumps, log files, etc.)
  - risk of deleting progress, such as loss of work taking significant amounts of time. (E.g. unrecoverable action, uncertain destructive action, actions that are risky due to resource constraints, etc.)
