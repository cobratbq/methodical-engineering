# Appendix

## A-1

This is a summary of interesting quotes from the _The SCUMM Diary_ article in Gamasutra. See _Appendix A-2_ for the article itself.

There are interesting little nuggets, sometimes a single sentence, that gives some insight into software development then. But more than that, some of these statements still hold true today. I will provide quotes with some commentary. Sometimes the comments will be very concise, merely to highlight a particular case.

> Since we didn't have very good editors for writing code on the PC I would use the editors on the Sun and transfer the files over. When there were errors on the PC, I would recompile and debug the code on the Sun workstation so this forced me to write cleaner code that would run on two vastly different computers from day one.

Code is cleaner, because it is developed for multiple platforms, even multiple processor architectures, from the start. Good error messages - or in general good tools - are _critical_ to the software development process. If the tools are lacking, time-costly mitigations are often the next-best thing.

> For controls, mice weren't always standard equipment, so controls were designed to work for mice, joysticks or keyboard. This made moving the game engine to other platforms easier since the controls were modularized. [..] For sounds, we supported [..]. Graphics were also modularized since we were supporting the Black & White Hercules display, 4-color CGA graphics, 16-color EGA, VGA in a 16 color mode, and Tandy Graphics in yet another video mode.

A clear example of the abstraction of devices. The abstract concept of a "controller", which can be a _keyboard_, _mouse_ or a _joystick_. Each of the supported controllers, even multiple devices of a type, are supported in separate modules. The appropriate modules can be loaded and used. Same for sound and graphics devices.

> To make this possible, all of our graphics were rendered into a buffer in memory and then very specialized routines would then copy the buffer out to the video card. We used what is called a “dirty rectangle” system that kept track of what areas had been updated, so we only copied the portions of the screen that were necessary.

An description of the chosen boundary for the video abstraction. Graphics are first rendered into a buffer. This is part of the general behavior of the game engine. Then, the way in which the buffer was read and fed into the video card, was specific to the video card itself. One could drop all color information, or strip it down to fewer colors, or keep all colors, depending on the target device. The common interface is the _graphics buffer_, it contains data in a format that each of the graphics modules knows what do to with. Each graphics module knows how to specialize for its own (and _only_ its own) intended target device.

> SCUMM was a language that was compiled into a tokenized form. For example, the command 'walk dr-fred to laboratory-door' would convert the Walk command into a single byte command. The next byte was which actor or character the command was for, and then the object 'laboratory-door' was converted into a 2-byte number so the entire command was reduced into 4 bytes of data.

So, a script was defined to write the adventure game story and interaction, anything that was part of the game. The program executable is the game engine, not the game itself. This allows work on the game engine and work on the game to be completely independent of each other. Only when the interface - the scripting language - changes, will work on the game engine affect the game itself.

> As you can see, the tokenized language was very efficient and the interpreter never had any understanding that one actor or another was “dr-fred” he was simply an actor-number, so we always tried to avoid hard-coding any specific information about a game directly into the interpreter.

So the game runs in an interpreter, but the design is such that the minimal amount of data needs to be processed. The engine works with generic concepts such as an 'actor' and can therefore be completely oblivious to the type of game or its backstory.

Notice the comment "_... we tried to avoid hard-coding any specific information about the game directly into the interpreter._". There are bounds to the capabilities of the engine. Sometimes you need to make a decision of where to draw the boundary. For example, previous quote said "_... would convert the Walk command into a single byte command._", so there is a hard limit of 256 distinct commands. The game you intend to make may push you to a certain limit, dictating such decisions along the way. The art is in recognizing whether or not such limits make sense for the game engine, or are merely part of the game.

There is also a trade-off to make. The game engine does not need to be infinitely dynamic/adapting. You would want the engine to be as flexible as needed for the game. Anything less interferes with development of your game. Anything more is unused potential. But this unused potential _does_ factor into the design and implementation complexity. So, another reason some aspect of the game is hard-coded in the engine, is a limitation of the engine. For example, maybe an engine supports only 1 character that is controlled by the player. That need not apply to all games, but may be hard-coded into the game engine regardless. This only becomes relevant once a game idea is born that requires controlling multiple characters.

> There was one exception to that rule during Maniac and that was that the color used by an actor to display text... [..] The original code had those values built into the interpreter, and when Maniac was upgraded to the PC and before Zak McKracken, two new commands were added so those values could be controlled by the scripts instead. “set-actor-talk-color” and “set-actor-color” moved the last game-specific commands out of the interpreter, so now the scripts controlled everything.

... or this example. Notice how they _add two new commands_. The game engine is evolving in a backwards-compatible way. Older game scripts will continue to run as they simply do not use the new commands. Newer games (game scripts) run only on the newer version of the game engine.

As an example, let's say they modify an existing "_talk_" command to accept 2 new parameters: foreground and background color. That would work, but it would have destroyed backwards compatibility.

> Since I am talking about the interpreter, let me be specific about what it is. The interpreter was the program that the end-user ran that would initialize the graphics and sounds, read the files from the disk, and interpret the scripts and data in those files to run the game.

It was mentioned before, but now going into more detail: the game engine reads and interprets the game script. The game is itself is not a native executable, rather the game engine is. The game engine ensures that all hardware is appropriately discovered and available. The game script can assume that when it's running, all necessary components are provided by the game engine or if unavailable then appropriately mitigated. The game script exists in its own virtual world inside the game engine. Its influence is limited to this virtual environment, but at the same time this makes it possible for the scripting language to be small and dedicated to story telling.

> When we would ship a game we would rename the interpreter to “Monkey.exe” or “Dig.exe”, but during development this tool was called SPUTM, which stood for “SCUMM Presentation Utility (tm)”.

And if the script requires an interpreter, the interpreter needs to be shipped as part of the game for the game to run.

> SCUMM, or Script Creation Utility for Maniac Mansion was the tool that tokenized the scripts and also merged all of the game assets together into the files that we shipped on the disk.

The SCUMM tool converts a human-readable game script into bytes that can trivially be read by the interpreter. This both simplifies and speeds up the interpretation process. The script is no longer readable for humans, but much simpler to read for the interpreter.

It is far easier for the interpreter to know to read a single byte followed by however many bytes are expected by that particular command, than to need to read an arbitrary number of bytes up to a certain one of multiple command termination characters but not line termination characters, followed by command parameters of similar arbitrary-length texts.

> The version of SCUMM that was used for Maniac probably shared 80 percent or more of the commands used in later games such as Full Throttle. Once the language was developed, most of the key commands did not require modifications. “walk bernard to clock” and “walk ben to motorcycle” were essentially unchanged.


```
Describe the SCUMM engine capabilities regarding concurrency / concurrent design in a single processor, single thread world.

- objects are individual "living" parts (people, clocks, swinging chandeliers, etc.)
- animations, sounds, scripts, etc.
- scripts, script logic possibly infinite.
- scheduling characteristics for engine
- ...

(also in sources.md) www.gamasutra.com/view/feature/196009/the_scumm_diary_stories_behind_.php?page=1
```

## A-2: [Gamasutra: "The SCUMM Diary: Stories behind one of the greatest game engines ever made"](https://www.gamasutra.com/view/feature/196009/the_scumm_diary_stories_behind_.php)

`TODO: need to check with Gamasutra for permission to include this content of the article itself as an appendix/source.`

[Part 1](https://www.gamasutra.com/view/feature/196009/the_scumm_diary_stories_behind_.php?page=1) - [Part 2](https://www.gamasutra.com/view/feature/196009/the_scumm_diary_stories_behind_.php?page=2) - [Part 3](https://www.gamasutra.com/view/feature/196009/the_scumm_diary_stories_behind_.php?page=3) - [Part 4](https://www.gamasutra.com/view/feature/196009/the_scumm_diary_stories_behind_.php?page=4) - [Part 5](https://www.gamasutra.com/view/feature/196009/the_scumm_diary_stories_behind_.php?page=5) - [Part 6](https://www.gamasutra.com/view/feature/196009/the_scumm_diary_stories_behind_.php?page=6)

### The SCUMM Diary: Stories behind one of the greatest game engines ever made

SCUMM might "just" be a video game engine -- but it's a video game engine that can elicit emotions nearly as strong as the games that it powers. 

When you talk about of the heyday of LucasArts adventure games, you have to talk about SCUMM, the "Script Creation Utility for Maniac Mansion" that powers some of the most memorable games ever made, such as Full Throttle, Day of the Tentacle and Sam & Max Hit the Road, and, of course, Maniac Mansion.

Aric Wilmunder, along with famed game designer Ron Gilbert, built SCUMM, in effect providing a way for games like these to exist. Wilmunder and journalist Mike Bevan recently got together over email, discussing SCUMM and the stories around it. Here are choice pieces of that conversation, all in the words of Wilmunder.

We thought it was important to put Wilmunder's words on Gamasutra, where they can "live," because SCUMM is not just an engine or a piece of tech. For many developers, it was how they expressed and shared their artistic vision with so many people, during one of the most memorable periods in video game history.

### On the evolution of the engine

One of the capabilities that distinguished LucasArts from most other developers was that many of us came from a mainframe background and so we developed tools and compilers on Sun workstations and then created hardware that allowed us to download the code and data to our target machines. This started with the Atari 800 and when Ron Gilbert was hired to do C64 work a similar system was developed for that platform as well.

The key benefit was that we could develop tools in C, or in the case of SCUMM using YACC, to process and then tokenize the code. Most developers were using the same machine to write and run their code, so their tools could only be as powerful as their target machine. After the first few years of PC product development, the PC itself became as capable as some of our earlier workstations so one by one we began to migrate our tools until we effectively retired the Sun workstations altogether.

The evolution of features started very early. I was the first internal developer to work on PCs and at the time the existing tools and compilers were very crude. For example, the C compilers that we had been using would give very detailed messages if an error was encountered. On the PC you would get a message like “File: walk.c Line:409 Error: 4004“ and your options at this point were to look at the line of code and try to figure out the problem or get out the manual to convert the error code into something meaningful. Often the error descriptions were pretty obscure as well. Since we didn't have very good editors for writing code on the PC I would use the editors on the Sun and transfer the files over. When there were errors on the PC, I would recompile and debug the code on the Sun workstation so this forced me to write cleaner code that would run on two vastly different computers from day one.

The PC presented a number of additional challenges since controls, sounds, and graphics were vastly different. For controls, mice weren't always standard equipment, so controls were designed to work for mice, joysticks or keyboard. This made moving the game engine to other platforms easier since the controls were modularized. For sounds, we supported the internal speaker; I think it was a sound card called CMS that was a precursor to the Adlib, as well as the sound system for the Tandy computers. Graphics were also modularized since we were supporting the Black & White Hercules display, 4-color CGA graphics, 16-color EGA, VGA in a 16 color mode, and Tandy Graphics in yet another video mode. To make this possible, all of our graphics were rendered into a buffer in memory and then very specialized routines would then copy the buffer out to the video card. We used what is called a “dirty rectangle” system that kept track of what areas had been updated, so we only copied the portions of the screen that were necessary.

![Aric Wilmunder](resources/Gamasutra-SCUMM-page1-image1.png "Aric Wilmunder")  
__Aric Wilmunder__

### Speaking in SCUMM

SCUMM was a language that was compiled into a tokenized form. For example, the command 'walk dr-fred to laboratory-door' would convert the Walk command into a single byte command. The next byte was which actor or character the command was for, and then the object 'laboratory-door' was converted into a 2-byte number so the entire command was reduced into 4 bytes of data. For those unfamiliar, a Byte is a location in the computer's memory that can hold a numeric value from 0-255. As you can see, the tokenized language was very efficient and the interpreter never had any understanding that one actor or another was “dr-fred” he was simply an actor-number, so we always tried to avoid hard-coding any specific information about a game directly into the interpreter.

There was one exception to that rule during Maniac and that was that the color used by an actor to display text...

When Maniac was being developed there were a couple of exceptions to the rule that the interpreter should not have any game values encoded and that had to do with the colors used for the actors and the color of the text displayed when they talked. The original code had those values built into the interpreter, and when Maniac was upgraded to the PC and before Zak McKracken, two new commands were added so those values could be controlled by the scripts instead. “set-actor-talk-color” and “set-actor-color” moved the last game-specific commands out of the interpreter, so now the scripts controlled everything.

Since I am talking about the interpreter, let me be specific about what it is. The interpreter was the program that the end-user ran that would initialize the graphics and sounds, read the files from the disk, and interpret the scripts and data in those files to run the game. When we would ship a game we would rename the interpreter to “Monkey.exe” or “Dig.exe”, but during development this tool was called SPUTM, which stood for “SCUMM Presentation Utility (tm)”. The name wasn't really trademarked, but we wanted to name it after another bodily fluid.

SCUMM, or Script Creation Utility for Maniac Mansion was the tool that tokenized the scripts and also merged all of the game assets together into the files that we shipped on the disk. The version of SCUMM that was used for Maniac probably shared 80 percent or more of the commands used in later games such as Full Throttle. Once the language was developed, most of the key commands did not require modifications. “walk bernard to clock” and “walk ben to motorcycle” were essentially unchanged.

### SCUMM’s multi-tasking

Probably the most distinctive part of SCUMM was that it was multi-tasking. This meant that multiple scripts could effectively run simultaneously. You might create a clock on the wall in Zak McKracken’s office and have it animate. There would be a single, very simple script just for the clock that would tell the animation engine to change from one image of the clock to the next, tell the sound engine to play the “tick” sound, and then tell the script to “sleep-for 1 second” and then repeat. The Sleep commands were deceptively simple commands that told the system that the script was complete, and to come back some period of time later and keep running the script from where you left off. “Sleep” would wait for a specified period of time.

There was also the ability to “Wait”. This would often be used when you told an actor to walk to an object or turn and face a direction. The script would simply command the actor to walk, and then issue a “wait-for actor” command which put the script to sleep until the actor had arrived or was facing the right direction. This allowed you to write your scripts in a very linear fashion reflecting the series of steps that you wanted the actor to follow.

### All kinds of useful tools with gross names

Along with SCUMM and SPUTM, there were a variety of other tools we developed for creating the games. SPIT was a simple font editor so we could design different text formats for different parts of the interface. Dialog at the top might be in one font, but another would be used for the save game screen, and another on the bottom of the screen when we used a verb-based (Walk-To, Pick-Up, Look At, etc.) interface. FLEM was a graphical tool used for managing the rooms. You could mark the objects in the room, their different states (Doors open or closed), and also build the walk-boxes that controlled where actors could travel. FLEM also let you preview the clipping planes, which were layers used to create the appearance of the actor disappearing behind objects or terrain. In Maniac there was only one plane, but in later games there might be as many as three planes that the actor might clip behind.

![Maniac Mansion](resources/Gamasutra-SCUMM-page2-image1.png "Maniac Mansion")  
__Maniac Mansion__

MMUCUS was the partner of FLEM. MMUCUS took the room image and the object data, the clipping planes and the walk-box data and compressed the data into a 'room' file containing all of that information. This was important since having the bulk of the data compressed allowed the scripters to be able to make quick script changes and only need to compile the script before giving the change a try. The room file would remain unchanged unless objects were added or walk-boxes were changed.

BYLE was our original animation tool used to draw and animate actors. BYLE had a simple animation engine that could be programmed to cycle animations from one frame to the next and it also had a notion of 'direction' so you could change the actor's facing. You could compress actors using a variety of methods and this allowed very simple and fast animations like the beginning of Day of the Tentacle to take little space, while multi-colored characters like Ben Throttle would use another compression method that was necessary when 16 or 32 colors were used. Over time we saw the need for even more complex animation types so a new tool, CYST was developed that still used the same costume data format but it was much easier to manipulate larger images.

For a short time we had a tool called SMEGMA. One of the programmers had a child and told us that when babies are born, their first bowel movements consist of this. Well, he was mistaken and it turns out that substance is called Meconium. We hadn't bothered to look up Smegma, we just liked the sound of it. Once we did, the name changed a few days later. As you can probably imagine, standing near us in the lunch line was probably a bit unsettling as we talked about SCUMM, BYLE, MMUCUS, FLEM, et al.

### Updating SCUMM

One of the criticisms of SCUMM was that it didn't go through revolutionary changes and instead improved incrementally over time. A clear example of this would be graphics. We started with 16-color graphics support in a 320 by 200 resolution. As the installed base of graphics cards improved, we moved to 256 colors and 640x480 screen resolution. Acoustically, we started with the internal IBM speaker and moved to multiple channels of streaming stereo audio. Characters that were just a few dozen pixels tall could now zoom to nearly full screen height. The interfaces evolved as well from the original verb interface to more sophisticated icon-based UIs, not unlike today's Mac desktop.

One of the key steps was the integration of the INSANE engine, the video system developed for Rebel Assault. I had originally hired Vince Lee to work on the Amiga version of one of the SCUMM games and he quickly showed his programming skills across a wide variety of platforms. When working on SCUMM he saw how I had worked to isolate the primary machine-specific parts of the system making portability to other computers fairly straightforward. During the development of INSANE he took that effort to a new level and added the ability to stream and branch video and that was quickly a high priority to incorporate into SCUMM. Full Throttle was the first step in that process and with the rock-and-roll soundtracks by the Gone Jackals, when someone started the game in the next room, you quickly took notice. The Throttle engine was not quite a match made in heaven, with the SCUMM system shutting down when INSANE started to run. So between Throttle and Monkey 3, I made the commitment to fully integrate the two systems, and tossed out the original video and cursor system and even the font system so that SPUTM was really running on top of INSANE. This put more eggs into the same basket, but the advantages were that, say if I added Japanese or Korean support, now both the Rebel Assault products and the SCUMM products could now both support Japanese and Korean.

### Impressively multiplatform

Because of the ease of portability, SCUMM games ran on over a dozen systems and in as many as a dozen languages. We started on Commodore 64, then IBM PC, Atari ST, Amiga, 8-Bit Nintendo, Fujitsu Towns, (Fujitsu) FM Marty, Sega CD, CDTV, Mac, and most recently iPhone and iPad. Not bad for a system that was first developed 25 years ago. 

With projects such as ScummVM, a fan-written SCUMM interpreter, additional target machines are now possible. That Monkey Island was selected as one of five games to be running at an exhibit at the Smithsonian Museum of American Art shows how good storytelling is often more important than flash-in-the-pan games based only on technology. 

What isn't always known is that SCUMM was also the basis of many top rated educational games such as the Putt-Putt, Freddi Fish and Spy Fox games and the Backyard Baseball/Football/Soccer products developed by Humongous Entertainment. If you peel back the covers, you would find the same commands and much of the same code as their LucasArts brethren.
Learning SCUMM

At the time, all of the designers were also programmers and SCUMM, while unique in many aspects, was also pretty easy to learn and code. There was no manual for Maniac or Zak but before Monkey a group of 6-8 new scripters were hired and a manual was created and a one-week training class (“Scumm University”) was organized. For the training, Ron would take the most recent game and simply delete all but one room and put objects in that room that represented a range of capabilities. 

New scripters, or “Scummlets,” would start in that room and learn the fundamentals and within a few days they were taught how to add more rooms, create walk boxes, some had artistic talent and would create their own animations, others would focus on writing dialog. Usually by the end of the week we had a pretty good sense of the skills that each of the Scummlets had and then the different project leaders would haggle to decide which ones would work on their projects.

I think that the first “Scumm University” or “Scumm U” started with the standard verb based UI. One of the early projects was always determining how the UI was going to work. So typically one or two scripters would get started on getting that up and running. Regarding Scummlet training, I think that one time it was at the Ranch (George Lucas’ ranch) and everyone was up on the third floor of the main house. George's offices were on the second floor so they had to be well-behaved.

![The Secret of Monkey Island](resources/Gamasutra-SCUMM-page3-image1.jpg "The Secret of Monkey Island")  
__The Secret of Monkey Island__

### The SCUMM advantage

One of the great benefits of SCUMM was how quickly a game could be prototyped. The designer would have ideas for rooms and locations and the lead background artist could start doing sketches. When enough of the sketches were done, they would get scanned in and you could very quickly add and connect them up using SCUMM. Usually within just a few weeks of the start of the design process, there would be many dozens of rooms, often drawn as simple pencil sketches, and we would usually take the actors from another game and start wiring them up. You might find that a room needed to be flipped, or redrawn since they didn't connect very well, but you could rapidly prototype a huge portion of the game.

The scripters could now create preliminary walk boxes so the actor could walk around, the background artists could start converting the sketches to final artwork, and the animators could now begin working on the character animations. Since final characters were still under development, during this development stage you might be walking around in a penciled room from Full Throttle but your main character might be Guybrush from the Monkey games. Having the placeholders allowed the designers to experiment and make changes and improvements at very little development expense.

### SCUMM’s longevity

I don't think that any of us thought that SCUMM games would be around this long. I worked on the system for about 12 years and I tried really hard to 'future proof' my code by testing across as many computers as I could. When developing under Windows, I would test it under Windows NT, even if that wasn't one of the target machines, but NT required stricter coding standards. So if it ran under NT, chances were improved that it would run under other future Windows operating systems.

I also tried to avoid too many coding tricks that might fail with future computers. For example, you can write self-modifying code, but depending on the size and type of processor cache, this could fail. So instead I would write five or six slightly different variations of the same routine, each specifically optimized for a certain situation. For example, there were many versions of the actor drawing code. If the actor was full screen and didn't clip behind other objects, I had one version of the code. If the actor was off the left or right edge of the screen I might have another. If the actor scaled I would have another. I think that there were eight variations on how an actor might appear and I had eight versions of the code each optimized to do the task as quickly as possible. I would also write the code initially in “C” to get it working and to optimize it. I would then rewrite the code in assembly language to get every bit of speed possible. Having the "C" code was handy when we developed SCUMM for other computers, since those developers could turn that code on and get everything working, and then optimize their code as necessary.

Scumm 2.0 was what we called the system for Zak. Maniac was 1.0, Zak 2.0, Monkey was 3.0 and Indiana Jones and the Last Crusade may have been on 3.0 or 3.1. I would have to do some digging to get the actual version numbers. There were also a number of special releases. The first Indy 256 color version was a promotion with a video card manufacturer. I had gotten the code running, but there weren't enough 256 color cards in the market yet, so we made a deal and shipped it along with the card.

### Advantages of interpreted languages

Ron had worked with interpreters for some time. He developed a series of new commands that could be added to Commodore Basic so he had reverse engineered that system. When developing Maniac, he thought that this was the best approach, and Chip Morningstar had a background in language compilers so the language grew mostly out of their collaboration. What is ironic is that Chip's project at the time was Habitat, which instead of using interpreted code was written to support small chunks of 6502 Assembly code that could be swapped in and out and moved in memory. Any time one of these chunks crashed, it would take out the whole system. SCUMM on the other hand only used about 80 commands, and the commands were packed into bytes with values from 0-255. If you ever got a command value > 80, the system would flag an error, so SCUMM was very quick to let you know if you were running bad commands, while Habitat would just crash, and it was very hard to trace bugs. I think that in retrospect Chip would have decided to use an interpreted language.

![game screenshot](resources/Gamasutra-SCUMM-page4-image1.gif "Game screenshot")

Another advantage of interpreted languages is that they migrate from machine to machine very easily. The interpreter was written in very portable C code and would simply parse through the series of command tokens. One huge benefit was that since the entire game was data, when developing for new machines, you knew that your data was correct, so you could eliminate that entire portion when looking for bugs and simply focus on the small amount of code needed to get a particular machine working.

### Working with the inimitable Ron Gilbert

Working with Ron was tremendous fun. Here are a few tales...

Much of our early artwork was done using traditional art techniques. Pencils, pens, paper, and occasionally errors were corrected by using an Exacto knife to make a repair. One day Ron snagged one of these blades and spent the next few days playing with it at his desk. He took to holding the knife between his teeth with the blade out while he would write code. One day I heard a loud gasp and turned to see blood running down Ron's arm and onto the floor. After helping to bandage the wounded hand, the conversation turned to tetanus and so I called my wife at home to see if Ron should get a shot. What had happened was that Ron's hair had gotten in his face. He had forgotten that he had the blade between his teeth and he reached up to brush the hair away and stuck the blade right into his palm. After this experience, the knives stayed in the art department.

Another time, Ron and I were working one evening at our building on Kerner Blvd. near ILM and we had some major disagreement about how to implement a bunch of code. Ron and I would often work until 10 or 11 at night, so we got to know the cleaning crews pretty well. One of them even went on to become a movie editor. Anyway, this evening I was sure that my solution to a problem was right, Ron was sure that his approach was better, and for whatever reason this escalated back and forth and turned into a huge shouting match. Ron finally stood up and I think cussed me out and walked over to the door and slammed it as hard as he could as he left. I was probably thinking good riddance and went back to coding.about five minutes later Ron came back in and said quite calmly, "Ok, we got that over with. Now let's figure out how we're going to solve this problem." Within minutes we were back at the whiteboards working on solutions, listening more carefully to what the other was saying, and ultimately we developed a hybrid solution that took the best of each of our ideas and was better than either of us had originally envisioned. Ron was a master collaborator.

Again, back in the early days, one of our co-workers was scheduled to give a presentation at a computer club in Berkeley. We didn't have any looming deadlines, so we jumped in the car to head across the Richmond-San Rafael bridge and watch. About half-way across the bridge, Ron pointed to a car in the break-down lane, so I pulled over and there was our co-worker with a flat tire. He didn't have a spare and my tire wouldn't fit, but he wouldn't leave the car, so he handed us two boxes of floppy disks and asked us to give the presentation in his place.

Well, we had no notes for the presentation, just disks, so when we were introduced, I just started to pull out disks and loaded whatever I could find on their computer while Ron gave a completely impromptu presentation.  Ron had worked in radio back in college and was on the “morning team" so he was very much in his comfort zone. I never liked public speaking but I could give great software demos. The presentation was really well received and we went home that night feeling pretty good about pulling this off. We were still feeling pretty good in the morning until about 10 a.m. when our co-worker showed up for work and he was pissed. We couldn't figure out why. We had covered his arse and the audience had a great time. Well, it turns out that our co-worker had a friend in the audience. When the friend was asked if we had demoed one particular piece, it turns out that we hadn't. We didn't know what we were given, and we must have skipped it. Instead of being thankful, our co-worker thought we had intentionally sabotaged his demo.

![image](resources/Gamasutra-SCUMM-page5-image1.jpg "Ron Gilbert, Aric Wilmunder, Noah Falstein circa 1985 (via Mobygames)")  
__Ron Gilbert, Aric Wilmunder, Noah Falstein circa 1985 (via Mobygames)__

There was the night of the Loma Prieta earthquake when Ron and I were going to drive into San Francisco to see if we could find his girlfriend. Bridges were down, we could see that fires had consumed over a city block in the marina district, and we were going to try to find someone. My wife was very upset about this but I wasn't going to let Ron try this alone. We were just about to head out the door when the phone rang and she had caught a bus to San Rafael and needed to be picked up.

At one point, Ron left LucasArts because his girlfriend went on a teaching job to China. This was in the middle of my work on Maniac Mansion for the PC, so when I would encounter parts of the code that Ron wrote that I didn't understand, I would literally convert the 6502 Assembly code straight to C so I could get the system to work even if I didn't understand what it was doing. This code was re-written as soon as Ron returned, but even years later there were rumors of 6502 code lurking inside the system.

While Ron was away, he lent his new RX7 to another co-worker. Unfortunately one night while driving home, a deer ran across the road and the car spun off the road and went backwards through a barb-wired fence putting scratches in the paint and ripping off the moon-roof. The co-worker had all of the body work done before Ron got back and the car was repainted. Ron didn't know about the accident at first and was confused because he was sure his moon-roof had a different look to it. Eventually the co-worker explained the whole story and Ron was relieved that he wasn't going crazy.

### My friend, SCUMM

Still, 25 years is a long time. I don't think that there is a single list of all of the games that used SCUMM, but between LucasArts and Humongous it must be between 30 and 40 different games. I know at times, some designers felt that SCUMM was confining. One described using SCUMM as being like trying to push an elephant with a pencil eraser. 

Still, I think that that designer's best-received game was written in SCUMM, so there may be times that working within limits forces a greater degree of focus. SCUMM was designed to do one thing, and as long as what you wanted to do was build great adventure games, SCUMM was certainly the best option around.

I played all of the SCUMM games start to finish many times, though on The Dig I think I skipped around and never played it from the beginning. One of the disappointments being on the inside was that I saw solutions to many of the best puzzles ahead of time, either during design or while the artists or scripters were working on them. Some puzzles I missed entirely since I knew the solution ahead of time so I didn't need to look around for the clues. I also played the games in many different languages and on different machines since I was responsible both for the international versions as well as cross-platform development.

My answer about my favorite SCUMM game has stayed the same for many years. I was always most excited and most proud of the most recent game we were working on. With each new game, we tried to push the boundaries and do things that had never been done before. I always thought that if you weren't moving the art or the technology forward, why bother? There were some favorite moments, like the opening animation in Day of the Tentacle, or the humor in Sam and Max, the music in Full Throttle, or being able to walk all three kids simultaneously in Maniac. Each accomplishment was very different, but like your children, they each hold a special place in your heart.

## B: Stack allocation for the JVM (Microsoft)

People at Microsoft were working on an implementation of stack allocation for the JVM. On current JVMs new objects are constructed with heap memory. The only "thing" on the stack is the pointer to the heap memory where the object is stored.

The idea for improvement is to figure out whether or not data is used after the method has completed. If the data would disappear, it means that we can store it on the stack and have it disappear automatically as part of the stack machine, therefore no memory needs to be allocated on the heap. From this idea [a prototype was born](https://ftp.fau.de/fosdem/2020/H.1302/reducing_gc_times.webm "FOSDEM 2020 recording on stack-allocated objects").

I want to highlight a very small detail here: `Math.max` is being replaced by an `if`-statement. Why is this? Escape analysis, the process of finding out whether the data is passed on to somewhere else, did not figure out that `Math.max` does not store the state anywhere. So, erring on the side of caution, escape analysis fails to identify the data as local-only.

The point I make here, is that the simpler expression - a simple `if`-statement is what makes escape analysis possible in the prototype. Of course, the prototype can be improved, but there are limits. The simpler the expression, the easier it is to analyze. A trivial change such as this was necessary to demonstrate how significant the improvements _can be_ with escape analysis performed and stack allocation applied.

References:

- [FOSDEM 2020 recording on stack-allocated objects](https://ftp.fau.de/fosdem/2020/H.1302/reducing_gc_times.webm)
- [Charlie Gracie: Stack allocation prototype for C2](https://mail.openjdk.java.net/pipermail/hotspot-compiler-dev/2020-June/038779.html)
- [Code Review for Initial stack allocation prototype](https://cr.openjdk.java.net/~adityam/charlie/stack_alloc/)
- [if-condition i.s.o. `Math.max` in ArrayList](https://cr.openjdk.java.net/~adityam/charlie/stack_alloc/src/java.base/share/classes/java/util/ArrayList.java.udiff.html):  
  ```diff
    @@ -233,12 +233,15 @@
                  int newCapacity = ArraysSupport.newLength(oldCapacity,
                  minCapacity - oldCapacity, /* minimum growth */
                  oldCapacity >> 1           /* preferred growth */);
                  return elementData = Arrays.copyOf(elementData, newCapacity);
              } else {
    -             return elementData = new Object[Math.max(DEFAULT_CAPACITY, minCapacity)];
    -         }
    +             if (DEFAULT_CAPACITY > minCapacity) {
    +                 return elementData = new Object[DEFAULT_CAPACITY];
    +             }
    +             return elementData = new Object[minCapacity];
    +         }
          }

          private Object[] grow() {
              return grow(size + 1);
  ```
- [if-condition i.s.o. `Math.max` in regex Matcher](https://cr.openjdk.java.net/~adityam/charlie/stack_alloc/src/java.base/share/classes/java/util/regex/Matcher.java.udiff.html):  
    ```diff
    @@ -242,12 +242,15 @@
          Matcher(Pattern parent, CharSequence text) {
              this.parentPattern = parent;
              this.text = text;
                
              // Allocate state storage
    -         int parentGroupCount = Math.max(parent.capturingGroupCount, 10);
    -         groups = new int[parentGroupCount * 2];
    +         if (parent.capturingGroupCount > 10) {
    +             groups = new int[parent.capturingGroupCount * 2];
    +         } else {
    +             groups = new int[20];
    +         }
              locals = new int[parent.localCount];
              localsPos = new IntHashSet[parent.localTCNCount];

              // Put fields into initial states
              reset();
  ```

## C: Recommended utility names

Recommended names for utilities. This list cannot be followed to the letter due to naming conventions at the level of the programming language. This listing is based on Java conventions primarily.

Naming/terms:

- `value`: a plain, arbitrary value
- `key`: a value that is used for look-ups, e.g. in dictionary/map or collection.
- `result`: either a value or a absence of value (e.g. `null`, `Optional.absent`, _checked exception_)
- `abort-on-failure`: throw _unchecked exception_ or `panic` or whatever one does to abort execution due to bug in the code.

Use cases:

- Private field `x`:
  - Field is immutable: `x()`  
    _The field is immutable, so the retrieved value cannot ever change. Used for example in retrieving singleton-instances._
  - Field is mutable:
    - Retrieve constant-time: `getX()`  
      _Constant-time (strongly preferred) retrieval of current value of field._
    - Retrieve variable-time: `TODO: ???`
    - Update constant-time: `setX(value)`  
      _Constant-time (strongly preferred) updating of current value of field._
    - Update variable-time: `updateX(value)`
- Type-specific operations:
  - Any many-/repeated-value types (strings, bytes, arrays, collections):
	  - Value is present: `lookupX(key) value`, abort-on-failure.
  	- Value may be absent: `findX(key) result`.
    - `concatenate(s1, s2, ...) value`: Connecting two strings back-to-front - joining without an intermediary value. Return produced value.
    - `join(glue, s1, s2, ...) value`: Connecting two or more strings with `glue`-value in-between. Return produced value.
	- ...
