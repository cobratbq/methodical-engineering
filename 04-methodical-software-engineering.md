# Software design

## Goals

1. Maximizing creative expression, by making all the trivial, boring, deterministic parts clear and methodical. Creativity can be found in:
	1. defining problem statement, identifying problem characteristics, 
	1. defining solution, make decisions regarding requirements,
	1. identifying deviations from simplicity-by-default, based on needs and trade-offs
	1. 


1. Software engineering should be about defining the right solution for a problem. (As opposed to nitpicking which characters are put in a file and where the spaces are in the content.)
1. Make a significant part of the software engineering practice deterministic through prescribed methods.

```
- encapsulation: fields in class
- encapsulation: logic bound to class (privileged), and push out unnecessary logic (usage)
- encapsulation: high cohesion
- utilities: usage logic (+ the extent of what is put in a utility - i.e. match single type)
- utilities: prescribed locations where to find utilities (no guessing)
- business logic: what logic is part of business logic, purge lines that should move to utility
- business logic: push business data up the call stack. (separate business logic and data)
- separate functional domains (i.e. business domain of the application from the functional domains of list/array/... implementations)
- minimize amount of logic and data that is part of business application, maximize reuse inside utilities
- ...
```

```
1. correctly functioning application / signaling any kind of error / reliable, trustworthy application / ... -> crash on any kind of "bad" situation (i.e. error that cannot be mitigated, discovery of corrupt data (in memory/persistent), missing hardware, etc.)
   an application can only be trustworthy if it visibly fails, because there cannot be doubt about the result of a "successful" execution.
   an application can be useful if it executes successfully for some case. Because it fails visibly on other paths, at least a user can distinguish between successful and unsuccessful runs.

1. Minimize surface-area of application logic, maximizing functional domain logic. (upside-down pyramid, tip at bottom is programming language + standard library, next functional domain and corresponding utilities, then (preferably very thin) top layer is front-end application)
1. Large functional domain ensures reusability across applications that operate on the same platform. Also valuable across architectures. (From monoliths to micro-services, rump-kernels, etc.)
1. ...
```

`TODO: where to put 'naming' things?`

## Heuristics

- any rules/guidelines for "good design/code" cannot be based on arbitrary numbers. Look for strict, natural boundaries. This prevents senseless discussion and arguing over "where to draw the boundary" and subsequently disappointments and frustration if you happen to violate the rule by the minimal amount.
- any rules/guidelines: if the right boundary cannot be found, continuing searching: larger scope, reinterpret the situation, reinterpret the goal/intention, etc.

```
if requirements are contradictory/mutually exclusive, you hit an abstraction barrier. Consider whether or not one requirement is actually not a design requirement, but instead an architectural requirement. These requirements need to be handled at a different layer.
```

```
1. determine data that is (minimally) needed to express the information that needs to be available.
1. Apply "theory of regions" (TODO: IS THIS CORRECT?) to determine nesting of classes.
   - ensuring that business logic is separated from utility logic.
     - properly separate utility logic into common library.
   - ensuring that data is properly encapsulated.
   - minimizes maintenance burden due to lack of structure (spaghetti-code)
   - 
1. 
```

## Key insights

I will start with the key insights. Next chapters will go into these in more detail.
  
- 'Simple' code as the base-level. Deviate with good reason, i.e. indicated/evidenced by profiling.
- Push context-dependent data (functional domain) up the call hierarchy.
- Seemingly contradictory requirements (dichotomy?) may be indicative of a conflict between design requirements and architecture requirements.  
  The most well-known is how it is beneficial for an application to crash on any kind of (unhandled) error, but often there are architectural requirements to "not crash".
- Design for exactly 1 solution, for one current problem.
- Determine the desired level of maturity, distribute your effort accordingly. `TODO: reference/include maturity levels as previously defined, with proof-of-concept = only-happy-flow (Works On My Machine), maturity has increasing handling of error cases.`
- Respect errors: errors are cases to be handled.
  - There is no such thing as "not handling" an error case. The only way to "not handle" an occurring error is to freeze the application. Anything else is an explicitly or implicitly chosen mitigation path. You can only choose to take control of the case or to leave it to chance/(non-)determinism/luck.
  - How many error cases or how much of the full spectrum of errors to handle, depends on the intended maturity of the application, and the breadth of the spectrum of use cases to be handled.
- ...

`TODO: refactoring is less dangerous than it seems: identifying and constructing objects is a matter of understanding the state invariants and recognizing whether mutations endanger the invariants (i.e. requires privileged access) vs. innocent operations (i.e. requiring merely usage access). After this is known, state and logic can be moved out to a different class. Class logic can be immensly complex and understanding is not necessary ... or can be acquired gradually along the way of refactoring.`

```
implementation detail (logic/code structure) < ... < design choice < ... < requirement (functional/architectural) < ...

---

"Ask why 5 times ...", or 6, or 7, or 8, or ... times. The idea being that you increase in levels of abstraction until arriving at the appropriate level to find the origin of the (faulty) behavior.

The bug/behavior itself is level 0: the bug is the very instance of (bad) behavior that is investigated. With each asking of "Why?", you attempt to backtrack to the origin of the current behavior: the behavior becomes the piece of logic, becomes the design choice, becomes another design choice or program structure subdivision, eventually becomes an functional or architectural requirement. Somewhere along those lines, expectations aren't met, resulting in (bad) behavior.

- As you trace back, you will find either code/structure, or design choice, or functional/architectural requirement where reality conflicts with expectations.
- It is not guaranteed that fixing the first encountered level fixes the behavior. If a bad requirement is at the root of a bad design choice is the cause of bad code structure, then fixing the bad code structure is not sufficient. The fixed code structure should also still meet all corresponding design choices, and those should meet with requirements.
```

## Level of maturity

`TODO: explain the trust-based definition, i.e. PoC can only be trusted to handle predefined dataset gracefully (i.e. not necessarily correct/fully), subset of features necessary for typical dataset containing only supported data characteristics reliably handled/processed. --- look up more in (unpublished?) blog post.`

- Base-case: proof-of-concept, only the happy path is covered and cared for. Anything else is unsupported, i.e. will crash at best. Application is only guaranteed to work with carefully selected requirements for data files, as the whole application is built up for that specific use case.
- Incremental steps of improvement: better support for additional cases as requested, e.g. supporting more data, offering more configuration options, better support for handling error cases e.g. mitigating/resolving instead of abandoning, etc.

## Design and architecture

- Design the solution for a _single_ problem. There can be multiple concerns. If so, make sure that everything is composed in a single, consistent problem description and requirements.
- Design for requirements that are relevant _now_. No anticipation for possible futures, no preservation of past work. Past is only relevant if current requirements specify taking into account certain circumstances from the past.

`TODO: programming in its own universe, foundations need to be built: domain types. Up-side down pyramid: foundations allow for broader more extensive building upon these foundations. Allowing further development faster than using only base types.`

## Programming

### Identifying classes / encapsulation requirements

1. Apply region theory (`TODO: look up and read up on exact details on this ... too many assumptions right now ...`) to identify which fields require same level of access simultaneously.
	- `TODO: should be processable as pairs of fields, due to associative, commutative, transitive.`
	- `TODO: this analysis should require understanding of the full logic, merely the range of values for fields such that it can be determined whether or not privileged access is necessary.`
	- two concerns:
		- `TODO: restrictions on field values exist => privileged access distinction needed => indicates need for (nested) encapsulation`
		- `TODO: access is required simultaneously => the logic cannot work unless access to both fields is available => indicates strong dependency`
	- privileged access required to multiple fields, simultaneously.
	- privileged access required to multiple fields, but not simultaneously.
	- privileged access required for one field but not the other, simultaneously.
	- privileged access required for one field but not the other, not simultaneously.
1. For each identified region:
	- what does the region protect against, i.e. what state invariant is protected? (For example: theoretically, the answer is always: either 'writing' or 'writing + reading' a field or more subtle fine-grained control. However, the effective restriction depends on the type, e.g. arrays can be modified if the "field containing the array (reference)" is merely read. Writing is intended for setting a new array in its place.
	- for newly identified regions: nested region is identified based on chosen level of protection/encapsulation. It's the distinguisher between the "privileged" and "usage" accesses. This difference is what the boundary represents for regions.
- Every class either `abstract` (or `interface`) if intermediate in hierarchy, or `final` if concrete class. If any intermediate is non-abstract whenever one changes it for immediate (own) benefit accidentally influences any derivative class.
  - Additionally, one should carefully consider if it makes sense to have an intermediate class that is immediately useful as concrete class. If the intermediate class is a complete implementation in itself, shouldn't it be used through composition (and can therefore be `final`)?
  - A non-`abstract` and non-`final` class has double function: basis for other implementations, actual implementation available for use.
  - state invariant => encapsulation (NOTE: state invariant is missing e.g. when using primitive type where any value is allowed, i.e. full range of 32-bit integer or full array of bytes, therefore state invariant non-existant, therefore no encapsulation needed.)
  - public methods protect state invariants
  - private methods provide common logic used in multiple public methods
  - all public methods part of interface for common use ("usage" access)

### Taking control of error handling

A common understanding of error handling is necessary. The nature of errors, kinds of errors, how to handle errors appropriately, primary and secondary ways of handling. Failing to thoroughly understand error handling results in errors not being handled, errors being silently forgotten resulting in corrupted state or even incorrect storage or communication, propagating errors to other executions and systems. Misunderstanding is prevalent, often fueled by unanswered questions on how to handle certain error cases.

First, it is important to realize that _you cannot __not__ handle errors_. The only way to do that is to freeze application execution entirely. And even if you do, you might cause other errors as a consequence. Aborting, throwing/not catching an exception, ignoring and silently continuing. All of these are ways of handling an error. The key insight is that you _must_ make a decision, so the question becomes: What is the most appropriate way to resolve the error case?

Also realize that aborting program execution is by far the most graceful thing you can do if something is wrong with the system. Let the user know. A program needs to be trustworthy. If errors are identified early and communicated clearly, you create opportunities to improve. Stability, reliability will come if you have sufficient information. Conversely, if execution continues silently and state corruption occurs and propagates, you will need to start a full investigation into not only what goes wrong - multiple things at this point - but also into the root cause.

NOTE: if the desire is for the application not to fail, this is an architectural/business requirement that contradicts fast-failure. This seems like an impossible request as you cannot both fail and not-fail at the same time. You can, however, handle errors differently on either side of an architectural abstraction boundary. Identify such boundary and take appropriate actions on both sides of the boundary.

The article [Joe Duffy: The Error Model](http://joeduffyblog.com/2016/02/07/the-error-model/ "Joe Duffy - The Error Model") gives a lot of insight into the many flavors of error handling that exist. He discusses pros and cons. In the end, there is no ideal solution, only a careful selection of trade-offs. He does note that there is value in making it explicit in code that logic can fail (in some undefined way). The error models that do not reflect in syntax at all, lack the control that is required for expecting viable error cases.

`TODO: consider example of framework that protects itself against plug-in crashes. For example, consider a web browser. It has many subsystems, one would be the add-ons. For badly written or in-progress add-ons, it is not uncommon to have bugs. It is imperative that the add-on crashes such that the bug can be discovered. It is equally important for the web browser to _not_ crash. So, you take actions appropriately, which are different in the space where the add-on lives than in the space where the web browser lives.`

Some things to consider:

1. _Can the error actually occur?_  
   If not, fail hard. If the program changes sufficiently that this starts occurring, you will know that error resolution at this site needs thorough investigation.
1. _Is it in the power of the application to handle this error?_  
    If the operating system is missing crucial libraries, or the JVM does not have support for the requirement cryptographic algorithms, there is nothing the application can do, but fail. The best solution is to abort program execution with a clear error of what is wrong.
1. _Can the error be resolved at this point?_  
   If so, take appropriate action. One can often handle the error.
   - If there are alternative actions to try.
   - If one can return an error response as opposed to the error itself.
   - If one can wrap the error such that the right type of error is propagated across an abstraction boundary, e.g. to avoid exposing implementation details to the caller.
1. _Can the error be handled further up the call stack?_  
   If the error cannot be handled on the spot, it is generally the case that the method call itself can no longer be executed successfully. The error that started this, may not even be useful anymore. The error needs to propagate up the callstack to let the caller know that execution failed. The caller can decide to mitigate or itself fail.

- dichotomy in design vs architecture: must fail vs. must not fail
- there is no such thing as "not handling" (that would be freezing execution on the spot)
- there is a primary way of handling, and side-activities to give more insight into program execution.
- benefits of handling vs propagating
- propagating across architectural boundaries (in what form?)
- ...



```
- You cannot _not_ handle errors. The only way to _not_ handle errors is to freeze in place, simulating a situation where the application executes indefinitely and the error never occurs. However, we also know that it will never finish as the error is there.
- Accept that there are many paths that you may not want to handle. (crashing is good, it is a clear signal) (Realize that crashing is a design decision. Crashing not being desirable is a requirement. These are in opposition, therefore there is a problem to solve.)
- Use implementation types if possible without violating abstraction boundaries.
  - implementation types may have fewer error cases to handle. (Interfaces have their specific purpose. Not every case needs an interface.)
- Realize that there are multiple levels of abstraction. Not all errors can be handled at the same level. Requirements also contribute. Consider if you really solve anything by handling the error or that there is a different problem to be solved than the apparent one that is the error.
- Error analogues:
  - checked exceptions = Result<_,Error> = error return value = ...
  - unchecked exceptions = panic!() = panic() = abort = C++ exceptions = ...
```

### Operating in a larger context

```TODO
- Dependency Injection & CDI
- ...
```

## Qualification

```
- Define strictly qualifiyable/quantifyable metrics.
  - Cohesion: (completion percentage?)
    - all fields in class member of same region (region theory)
    - logic reduced to necessary minimum to expose/manipulate state to guarantee state invariance (Question: what logic do I need to write to allow the object to be used without being able to corrupt the state if the user does not know what he is doing?)
    - logic strictly limited to logic that requires privileged access (Ask: Can I write this logic as a utility and still make it work? If the only way to realize the intended goal is with privileged access, then it is acceptable.)
  - Coupling: (?????????? does a coupling metric make sense or is it emerging?)
    - consider if coupling is a indirect metric that is necessitated by the implementation. It may very well be that there is no sense in guarding this as it reflects the consequences of failing the "coupling" metric.
    cohesion (for classes) is guaranteed through the minimalism resulting from the stated rules.
    cohesion (for utilities) is ???????
    coupling for utilities: ensure utility logic focuses on a single data type. Define functions such that they can be nested or chained.
  - Lines of Code,
    - in business application: smaller is better, meaning that more of the trivial logic is called through utilities.
    - in utility logic: ...
  - 
```

## Notes

```TODO
- Character of business logic as:
  - calling logic with intermediate results (as local variables) present only to mitigate errors, or to call on next logic for different kind of utility.
  - ideally, every statement either references privileged data or calculates result.
  - sequences of statements (all on same type of data?) all for purpose of preparing data is likely extractable as utility.


- add definition of 'simple' somehwere. Note that deviating from simple means choosing to either:
  - optimize (typically reason of performance),
  - undo reduction (intermediate values are required?)
  - prefer generalized solution over dedicated (code re-use, compatibility with other code/programs, ...)
- Design exactly 1 solution for one current problem.
  - 1 solution: keep your goal in mind and aim for it.
  - 1 problem: dissect and clarify the problem you are solving, solve only that problem.
    - there can only be one problem being solved. If multiple goals are in mind, the full interaction should be evaluated and a solution should be designed for the complete composite, hence again a single problem. Any shortcuts here effectively forces your hand at an incomplete solution hence leads to problems, hidden errors, unanticipated errors, unanticipated results, etc.
  - current problem: strip away obsolete past requirements, strip away possible future requirements. The requirements should reflect exactly and only the needs for the problem currently at hand.
    - past: technical debt. No longer useful, but semantically present so will interfere with operation.
    - future: technical debt. Not yet useful, as it is designed for a possible future, there are many alternative interpretations that may slightly change requirements still. Therefore any extrapolation of possible requirements is likely to lead to technical debt instead of benefit.
- Design, and only design, is concerned with the specifics to provide a consistent, verified solution validated to solve the problem.
  - design is not concerned with how well it fits in the larger context. Interest is limited to the necessities, that is the requirements for interoperability when needed.
  - architecture is concerned with the larger whole, producing additional requirements the problem to be solved, ensuring acceptance of the solution by the environment. Architecture is not concerned with the specifics of a designed solution.
  - Interaction between architecture and design is in the form of requirements. Design is responsible for a suitable solution to a problem. Architecture is responsible for providing additional requirements to the problem for a solution to fit in the larger context. Relationship of architecture to design is 1:n. A single architecture influences many individual solutions.
- Recognize that contradictory requirements and (gradual) mutation of requirements are signs of natural (abstraction) boundaries.
- Working with nulls (parameters, returns, etc.)
- Enums represent a fixed number of states/values, typically as defined in a specification. A more advanced form would be "sealed classes" (in newest Java version), which provides a fixed number of implementations based on given interface. In both cases, you should be able to find a one-to-one mapping to some spec. The knowledge of a fixed number can be used to ensure that you have covered all your code paths and error paths, and to enjoy syntax-based error reporting in case you have not.
  - important to realize that having polymorphism and calling every individual property `updateUser` does not make for good structure or descriptive code/naming. If the number of cases is fixed and e.g. UI is static, let each UI element call on a different method. This is straight-forward, obvious and perfectly fine. Once your UI is dynamically built up from files/networking/etc. and every value is mappable to a particular implementation or part of that implementation, then it makes sense to go into the direction of polymorphism. If this isn't the case, then you can still go this way but you will introduce a redundant level of abstraction.  

Type-structure:
- Object-oriented: array of composite type (multiple fields)  
  Primary benefit: related value-types grouped as fields in same struct.
- Data-oriented: arrays for each field-type (relatedless not expressed, known only by logic following implicit convention)  
  Primary benefit: contiguous arrays provide data-locality for each of the fields. (Optimization trading off access speed improvement for obscurity.)

Data types:
- CASE: single value(-type): field/variable of type
  - Benefit: trivial case, most straight-forward and obvious.
  - VARIANT: value-type is stack-allocated.
  - VARIANT: value-type is heap-allocated: dereferencing will take time. For optimal performance, store value in a stack-allocated type (strictly an optimization).
- CASE: repeated, single value(-type): array of specific type
  - VARIANT: value-type is stack-allocated.
  - VARIANT: value-type is heap-allocated: dereferencing will take additional time. For optimal performance, store value in a stack-allocated type (strictly an optimization).
  - Benefit: typically supported out-of-the-box, compiler/interpreter will (probably) allocate contiguous memory for array.
- CASE: multiple value(-types), fixed predetermined keys: struct/class.
  - CASE: keys have specific value(-types):
	- VARIANT: struct/class itself and/or field value-types allocated on stack.
	- VARIANT: struct/class itself and/or field value-types allocated on heap, require dereferencing, takes time. Consider splitting up into stack-allocated types. (Strictly an optimization.)
  - CASE: keys are typeless/generic/dynamic typed (generic type/Object/interface{}):
  - CASE: a key can contain a variety of value(-types) - may be abused for different purposes. (bad sign)
  - Benefit: keys do not take up memory, due to them being part of the type.
  - Benefit: predetermined types means it is clear at compile-time which possible keys and values can/will be stored. (Although not yet which will be utilized at same time. For simplicity, least-surprise use all fields of the type, or maybe split up into different use cases and handle independently.)
- multiple values, arbitrary keys (arbitrary value types): dictionary/map of generic type/Object/interface{}
  - Downside: keys take up memory (also explicitly stored)
  - Downside: keys may not be present, need to be checked at run-time.
- repeated, multiple value(-types), fixed predetermined keys, fixed corresponding value-types: array of struct/class.
  - In case fields are references to heap-memory, in data-oriented fashion one may want to split the struct/class into several arrays, one for each of the keys, for cache locality benefits.
```

- Key insights:
  - Note: you can also push data down the hierarchy, but what happens then is that data becomes mixed in with other data that has a more specific purpose, resulting in data that is (correctly) managed by the various pieces of logic but only because the logic itself contains an implicit notion of how the various pieces should be treated. OOP - encapsulation in particular - is there to make the separation of concerns explicit. Therefore, push up all data not of concerns for a particular class.
  - Note: spaghetti-code is a consequence of concerns not separated, i.e. unrelated data mixed together.
    - Probably, the identifying characteristic/consequence of spaghetti-code is that the relationship between various (often unrelated) data is in semantics only, i.e. the syntax does not clearly show which pieces of data are related and which are unrelated. This happens most often with logic for mutating various pieces of data is intertwined, but mixed-up logic is only a problem because the developer is supposed to read the (semantics of the) data relationship from the logic. The code is unreadable because of the large amount of possible unknowns, if reading local pieces of code.
  - gradual improvement is feasible, once it is possible to recognize the right direction for improvement.
  - functional -> procedural -> OOP = increasingly separated of concerns. ==> readability, structure, etc. For execution, this separation of concern is (no longer) necessary, if the semantics are preserved.
  - recognize different requirements in strategies/requirements as natural boundaries, i.e. architectural/design boundaries, and treat them as such. (There exist no code-bases where errors need to be silenced. There only exist code-bases where errors need to be thrown, and some code-bases have another requirement that the application cannot fail and therefore errors need to be handled appropriately. The part of the code that throws the error is still valid. Identify the right level of abstraction where errors need to be mitigated and implement that.)
  - if you identify contradictory requirements, consider if there should be an architectural/design boundary in between. (E.g. one side of boundary: errors are thrown, other side of boundary: errors are handled appropriately and mitigating actions are taking place. There is no such thing as "silencing errors because they are irrelevant".)
  - Cases are handled: either by supporting them, or by not supporting them. Silently ignoring will only cause problems. [Errors are cases too](https://blog.golang.org/errors-are-values "Errors are Values by Rob Pike")
    - applications throwing errors are not incomplete/inconsistent/low quality. They're applications with a clear boundary for supported use cases. Silently continuing with unpredictable/undeterministic/undocumented behavior as a consequence is a sign of low quality/incompetence, because the application starts exhibiting bad behavior. Fatal errors, on the other hand, clearly expresses an unsupported use case and demonstrates to the user that you cannot continue. The message the user receives may be unexpected and may cause frustration, but not erroring out may lead into bad results. Applications that may or may not produce bad results become unpredictable, untrustworthy and are not even useful for supported use cases, as you cannot tell when you can and when you cannot trust the results.
    - `TODO: you cannot _not_ handle errors. Instead, you can only choose the path you take, and not making this choice explicit means that you take a default path. In case of exception handling, it's often unwinding the stack then crashing the application. In case of catching-but-not-handling, it is continuing as if nothing happened. (The only way to _not_ handle errors, would be to freeze execution of the program.)`
  - Leveraging many eyes to improve software, even if not looking at code:
    - __logging__ allows knowledgeable (developers, power users) to recognize things that seem out-of-the-ordinary, potentially leading to a (subtle) bug.
    - __crashes__ show users that the program avoided doing things the wrong way. Based on use cases (supported or unsupported, with/without corresponding stack traces, additionally gathered information) allow normal users to identify things are not right. Encountering/identifying: unsupported use cases, unexpected exceptions to supported use cases, errors/bugs.
    - __Crash reports/stack dumps/log files__ give unknowledgable users a way to provide detailed information about their problem for troubleshooting. (Need to consider private data in data dumps.)
- "Simplicity" can be a good thing. Simple code is easier to be optimized automatically by compilers/JIT/JVM/interpreters.
- A common way to acquire constants defined in specifications. A way to update these constants. Convert them immediately to usable program code. Ensures no mistakes are made in copying them. Ensures that updates are incorporated in implementations. Shared use of multiple implementations. Automatically checking? (considering not using references, but instead having validation rules.)

software-application-evolution: pyramid. Establish fundament of basic functional domain, i.e. basic types + utilities. Establishing the (functional) domain such that functionality can be built up quickly using types that are aware of the functional domain. The actual (business) application can be very thin, while the functional domain is extensive. The domain can be shared among applications. The domain can be evolved (improved, extended, even drifting towards different goals over time).
  
"Engineering stages" (not very scientific nature, just some notes ...):

- define problem statement (mostly functional, business stakeholders) & architectural constraints (mostly non-functional, technical stakeholders)
- define solution parameters (derived from functional, operational, developmental requirements)
- design solution (design rules based on requirements + decisions made on level of design/implementation)
- implement solution (based on design)

Documentation:

- keep documentation close to where it is applicable: design documentation (diagrams, decisions, applicable requirements, descriptions) and implementation notes/decisions, etc. close to the implementation, because that's what it applies to and what it's intended audience is interested in. Design documentation is only relevant to the (current) implementation anyways.  
  This documentation evolves with the implementation and is only relevant for as far as it corresponds to the implementation. Make sure it is accessible to those who have an interest in it. And don't hide it far away in document management systems where it can be "easily" accessed by all who don't care.
  - design documents should match the current state of the implementation, therefore makes sense (easily locatable/accessible, saves maintenance efforts) if stored with the implementation. As opposed to business and architecture requirements, which hold for a period of time - they apply to the environment/context - and should be picked up by designs as applicable. Requirements are _independent_ of a particular design/implementation.
  - storing in a technical location is fine (developers need to access it anyways, so it is convenient for them)
  - using technical tools in the documentation process is fine (technical people can work technical tools)
  - possible set up a mechanism to produce a non-technical document for those parts of the implementation documentation that are relevant to non-technical people, e.g. the subset of functional and non-functional requirements that are satisfied in this version of the implementation, and possibly which requirements are skipped/postponed/in use, and their decision rationale.
- keep high-level documentation such as future requirements, architectural designs (that only partially apply), etc. in stores apart from the single implementation version.
- concrete classes (representing `struct` or java's `record`) is likely part of the API/interface, i.e. domain types. Do not define interfaces for such types. These are the interface and may be exposed as part of the API.

Dependency Injection and Context Dependency Injection:
- ...

```TODO
- Prioritization:
  - single list with all entries,
  - order determines priority.
  - if one cannot decide on priority between two items, either further divide (split up a single item into multiple) or force-choose a more important item (e.g. based on other criteria, circumstances, dice).
```

`TODO add notes about everything you observe using something like commented 'XXX' or 'FIXME' markers. Smallest things matter, like checking for 'null' or 0. (e.g. https://neilmadden.blog/2022/04/19/psychic-signatures-in-java/>)`

- logical fallacies
- property-pairs
- trilemmas
- ...
