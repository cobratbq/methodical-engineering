# Notes

## Structure

```
- introduce simplicity,
- connect simplicity and problems to engineering,
- elaborate simplicity in problems,
- explain ways in which deviating from simplicity has consequences: premature abstraction, premature optimization, technical debt.
- design patterns are simple (???), (little maintance if applied only when/where needed)
```

1. Chapter on introduction, motivation, concerns, ...
   - guidelines towards general program structure, with rationale for when to deviate. Creativity is in what data to store, what logic to program. (The actual design, understanding the functional domain, etc.) Methodical is in how data is stored. (How to reason about the code structure, when to deviate from standard structure, what the standard structure is, how different circumstances result in different structure, etc.)
   - OOP the "paradigm enigma": explain how OOP in Java/C#/etc. is missing the independent, concurrent nature of objects, hence is not an OOP language, hence "message passing" does not exist (no method calling is not it - concurrency is missing), hence it is not a different paradigm than procedural.
   - motivation:
     - too many decisions based on arbitrary numbers, ego, circumstantial support, language-specific quirks/"conventions", resulting in decisions on anything but "common logic based directly on programming theory itself".
     - design and architecture are confusing (in context of software engineering).
       - design shouldn't care about the future at all. Concerned with the actual solution itself. That is, how to make the implementation fit the envisioned solution. Concerned with local issues.
       - architecture should define minimal (additional) constraints to uphold for suitability of solution in the larger context. Concerned with solution fitting in the environment.
   - abstract:
     - "methodical" for the prescribed guided way of working that guides towards base-line simple code, which functions well as a starting point for further improvement. Consider thinking about it as first "taking control of the problem and solution", then one can deliberately decide to improve specific regions that are of special concern.
     - main concern, guidelines in the article are good, but rationale behind it is insufficiently explained/elaborated, therefore it becomes a practice of "blindly following". Anything in the article should be obvious, i.e. rationale based on programming language theory and design theory, trivial derivations that anyone should be able to do. The guidelines are actually more direct than many of the high-standing "programming principles" such as SOLID and GRASP.
     - goals:
       - quick reference for solution under circumstances,
       - raise the base-level for informed, thought-out, rationalized programming/quality-assessment,
       - rationale from programming theory instead of "ideal solutions for particular programming language",
       - provide mapping towards other design principles when applicable (a.o. SOLID, GRASP),
       - this is not about best practices or opinions on "better way of working". This is about identifying and eliminating arbitrary rules, and excessive and unnecessary areas of discussion. Let's make programming - typing - more boring and let's make designing more intersting. Let's focus on areas that require creativity and silence boring, repeat, senseless discussions. This may even make room for automating or scripting the boring parts without sacrificing expressiveness or restricting/inhibiting developer creativity.
       - allow developers to focus on the interesting part of their work: architecting/designing a good solution for a problem,
       - `TODO: ...`
     - aim for elementary knowledge on programming that has been muddied over under the confusion, distration, paradigm-competitions, ego-competitions, immaturity of the profession. The excitement over OOP as a paradigm made people forget that basic premise of encapsulation and that the concept of "message passing" is actually missing. Furthermore, the competition with functional programming, made them forget that `TODO: continue here, but how?`
   - concerns:
     - these are language-agnostic guidelines/rules, based on theory and in principle paradigm-agnostic. There is a separate specialization necessary for programming languages that have "best practices" and framework support for specific use cases. This is actually a big risk, as these supports need not be sane.
   - topics:
     - linear scale of functional-procedural-OOP. Position on the scale determined by the use of local state, encapsulation.
     - utility logic (context-independent) vs business logic (context-dependent).
   - needs to define and explain some concepts such that these can later be used in explaining the rules/guidelines/boundaries.

1. Chapter on current state of art, problems, grey areas/vagueness, ...
   - arbitrary numbers
   - egos arguing, instead of factual arguing
   - rules constantly tweaked to circumstances
   - design patterns but (probably) underused/undervalued.
   - utilities, but no habit of consistent use, or awareness of the vast extent of utilities, or consensus on where to put/find utilities - without awareness of the utility no benefit.
   - lack of (reliable) metrics to indicate quality / maturity of the code-base.
   - too many (endless) discussions on design choices that aren't design concerns: design for future, design for extensibility, etc. Arguments go into "how far to design ahead?", "how flexible to be?", "what is part of the design decision and what isn't?"
   - lack of hard boundaries to avoid endlessly discussion subjective values. Need to identify and use hard, strict boundaries.
   - a scenario with prescribed 10 different values being uniquely handled. Write a switch-statement/-expression and include every case explicitly. Now, such a piece of code expresses exactly what is necessary, but may still be punished because "the function is too long/has too many lines". These kinds of rules are nonsensicle, should not exist as they unnecessarily cause confusion/discussion/conflict.

1. Chapter on methodical design (programming) matching the required/requested implementation as exactly as possible.
   - Key insights:
     - 'simple' is the base level.  
       Deviating is obviously allowed, but should be informed and specific areas, i.e. on case-by-case basis.
     - push context-dependent data (business domain) up in the call hierarchy. This basically means push everything up unless data does no longer serve a purpose at that level. For example, data to maintain an array list is only useful inside the array list. Anything else, i.e. content of the array list, is pushed upward and only inserted upon use (run-time).
       - Note: you can also push data down the hierarchy, but what happens then is that data becomes mixed in with other data that has a more specific purpose, resulting in data that is (correctly) managed by the various pieces of logic but only because the logic itself contains an implicit notion of how the various pieces should be treated. OOP - encapsulation in particular - is there to make the separation of concerns explicit. Therefore, push up all data not of concerns for a particular class.
       - Note: spaghetti-code is a consequence of concerns not separated, i.e. unrelated data mixed together.
     - design aims to implement exactly 1 solution.
     - design aims to implement current requirements, i.e. no anticipation for the future.
     - design aims to eradicate obsolete past. (Only care for logic that is relevant in operation.)
     - design is only concerned with the one solution for the problem, not the larger context. (It may receive additional requirements from architecture specifying the necessity to operate in the larger context. These requirements should be specified above the level of design, such that the appropriate freedom of interpretation is available for designing.)
     - any rules/guidelines for "good design"/"good code" cannot be based on arbitrary numbers. Ideally, look for strict, natural boundaries, thus preventing senseless discussion and arguing over "where to draw the boundary".
     - any rules/guidelines: if the right boundary cannot be found, continuing searching.
     - gradual improvement is feasible, once it is possible to recognize the right direction for improvement.
     - functional -> procedural -> OOP = increasingly separated of concerns. ==> readability, structure, etc. For execution, this separation of concern is (no longer) necessary, if the semantics are preserved.
     - respect errors: errors should always be thrown and handled appropriately.
     - recognize different requirements in strategies/requirements as natural boundaries, i.e. architectural/design boundaries, and treat them as such. (There exist no code-bases where errors need to be silenced. There only exist code-bases where errors need to be thrown, and some code-bases have another requirement that the application cannot fail and therefore errors need to be handled appropriately. The part of the code that throws the error is still valid. Identify the right level of abstraction where errors need to be mitigated and implement that.)
     - if you identify contradictory requirements, consider if there should be an architectural/design boundary in between. (E.g. one side of boundary: errors are thrown, other side of boundary: errors are handled appropriately and mitigating actions are taking place. There is no such thing as "silencing errors because they are irrelevant".)
     - Cases are handled: either by supporting them, or by not supporting them. Silently ignoring will only cause problems. [Errors are cases too](https://blog.golang.org/errors-are-values "Errors are Values by Rob Pike").
   - Design (and therefore programming) is aiming for the implementation of exactly 1 solution. Must be strict, maximally restricted, simple. No sense in introducing or preparing for "possible future features", extra room for dynamics, excessively generic code, etc. All the extra freedom that is introduced and not used is making things unnecessary complicated.
     - Exactly 1 solution for 1 problem.
     - Respects the architectural constraints/restrictions that are present as part of the requirements.
     - No need to "design for the future" in general. (Specifics may be part of requirements, but should be a specific function.)
     - Design to be maximally restricted: this helps to identify where support is missing because of errors, instead of silently succeeding with bad (or at least unspecified) results.
     - Design to be maximally restricted to help take control of the project. (time, resources, functional ambiguity, etc.)
   - Aiming for "simple" (as defined below): aiming not because "it is the right thing to do", but because the properties make for a as-trivial-as-possible implementation. Subsequently one can choose to optimize, introduce more generic solutions, etc. as required. However, aiming for simplicity first, produces quality properties such as readability. One can at a later point in time decide to sacrifice simplicity for performance (optimization), genericity (`TODO: not sure what for ...`), or more excessive notation (e.g. in preparation of partial or full replacement with something else, such as an alternative solution).
   - Essentially, OOP as provided in Java/Kotlin/C#/Python/... are about data. 'Data' is the core concept: 'data' is what makes the code domain-specific.
   - Starting assumptions for design:
     - simple has priority,
     - design/implemented for current requirements only (i.e. no anticipation of possible future),
     - no alternative implementations, excessive genericity. (unless explicitly required)
     - adhere to standard rules to ensure low coupling + high cohesion, basically follow best practices for good design.
     - Given a composition of fields and field semantics that additionally satisfies with region theory, cohesion is optimal. (However, one could argue for a different composition of fields or field semantics and achieve better cohesion.(`TODO: ?what does this even mean?`) `TODO: should we drop "cohesion" definition for too vague and use region theory satisfaction?`
     - abolish arbitrary/random numbers that "guide" development and infuse conflicts, arguments and arbitrary failures for quality gates.
     - not written for single programming language, but instead for whole class of procedural languages as long. Programming language specifics may provide better argument to rationale in the text, but rationale used in the text is suitable to all languages.
     - `TODO: ...`

1. Chapter on examples of methodical design through Java:
   - short description of Java programming language characteristics for people who are not familiar with Java itself, so that they can still "think with" the examples in this chapter.
   - build/compilation:
	 - failing on compiler warnings
	 - annotation processing
	 - static analysis
	 - etc.
   - programming/language rationale/patterns/tricks:
	 - exception handling
     - use implementation types internally in class, expose restricted types/interfaces
     - ...

1. Chapter on methodical architecture (functional/operational/developmental requirements).
   - functional, operational, developmental (a.k.a. functional + non-functional)
     - _developmental_ requirements are tricky, because requirements like "_must be implemented in Java_" enforce unnecessarily strict requirements.
   - requirements must surpass the concerns of a single problem: identify requirements for a class of problems. (Requirements for a single problem are a design issue, because the choices can be geared towards the single, exact solution.)

1. Chapter on per-programming-language-specializations?
   - "magic" dependency injection vs. constructor injection pattern: field injection, setter injection, constructor injection, annotations.
   - `TODO: ...`

1. ...

## Checklist

- Investigate Theory of Regions, double-check that I am actually following the theory regarding the discovery of (nested) regions. Check whether the derivation of access level and the such match with (i.e. don't violate) the rules defined in the Theory of Regions.
- Check that references to SCUMM correctly state that the (original) game engine is called 'SPUTM' and not 'SCUMM'.
- Ensure that goals discussed in the introduction are met:
  - Discuss relevant parts of computer science and technology concept, but also give an impression of the breadth of the topic itself, to avoid making everything seem small and directed towards the goal of the document.
  - ...
- ...

## Unprocessed notes

- metrics:
  - use ratio: `lines-in-utilities / lines-in-business-logic` (no result for 0 lines of business logic) for relative expressivity metric. `value < 1` indicates elementary lines-of-code in business logic, little to no use of utilities, therefore little possibility for reuse, low expressivity. `value > 1`, the higher the value the more expressive the business logic is, so reads closer to functional domain requirements. Lots of possibility for reuse because utilities cover many lines-of-code.  
  Hard to make this an absolute metric because it depends on the nature of the functional domain how large the utilities can become. The trivial utility examples such as manipulation of collections are quite universal, but as soon as domain logic is concerned, some domains are more math-heavy or something and therefore may contain many more lines-of-code.  
  Use ratio to identify areas that may have room for improvement. Use ratio to compare to other projects to gauge maturity. (Could a geometric mean be applicable as a generic ratio for a project based on geometric mean - or maybe just (weighted) average - of all available functions?)
  - what about `lines-calling-utilities / lines-of-business-logic`? 100% indicates that each line uses some utility, therefore very little elementary code. Seems like it gives a false impression as 100% does not mean top-efficiency.
  - how many lines of business logic require privileged access vs how many lines of business logic exist?  
    The rationale here is that expressive, high-density logic should access business-concerned data almost continuously. Otherwise, you would be doing "intermediate stuff" in preparation of the business data modification.  
    The "intermediate stuff" is often one or more utilities. It is important to note that (business-)domain specific constants are considered part of the business logic, therefore, preparations and manipulations that use constants (e.g. from specification) are also business logic. This is the case because a context-independent utility would not be able to predict/prescribe which value to use. (E.g. HTTP status codes for responses, or the Generator in specific cryptographic elliptic curves.)
  - `TODO: how to judge error handling for such logic?` (almost by definition does not require privilege as it is a local problem being handled in the moment)

- interface as indicator for implemented convention: may not contain data, but just grouped logic.

- OOP:
  - encapsulation rules for data and logic
  - push data and logic up the hierarchy:
    - data in lower levels are data that are encapsulated and used to operate a single class.
    - data in higher levels are data that represent the business function itself.
    - pushing state of application further up the hierarchy may free up more logic for use as utility logic, hence enhance expressivity. (function programming-esque approach)
    - this makes sense and works _because_ almost all logic needs merely usage-access. Privileged access is not necessary.
  - extract non-data-dependent logic into utilities
  - Functional programming ------- procedural programming ------- OOP
    - expresses a gradual path from one extreme to the other. This is not a static, predetermined choice, but instead a linear scale where your code is present somewhere along this scale. Which differs even for individual functions.
    - functional: data lives at top of hierarchy in call stack, completely separated/isolated from the logic. No use for methods/classes as the struct is just another function parameter provided from the caller hierarchy. Data is basically immutable, because there is a new spot in memory for every function call.
    - procedural: data lives intertwined with logic lines. (mixed local data and business data) May have structs and functions working on these structs.
    - OOP: data and corresponding logic lives in modules, allowing logic and data to be encapsulated as appropriate. Data intended for business application is pushed up the hierarchy. Has classes/structs and logic tied to these types.
    - gradual path from higher-order functions (function as parameter) on functional-side to interface-definitions in OOP (minimal = interface with 1 defined method).
    - `TODO: can we set a default level for state management`: base is stateless (functional), or base is procedural (local variables, no objects)?
    - OOP is (mostly) syntactic sugar. Functional programming is separating state from logic. Therefore procedural programming is the "most natural" variant? `TODO: ...`
  - Goal of encapsulation:
    - isolate fields having state invariants.
    - provide controlled exposure of field data. (expose as much for _usage_ as possible without )
    - provide logic for mutating the fields (respecting state invariants)
    - minimize logic requiring internal knowledge of field restrictions. (logic requiring privileged access)
    - 

- Modular programming: on the mistaken analog of legos
  - block function = look pretty (colors)
  - block interface (consuming, providing) = size of x notches (single type, variable size)
  - note: plenty of ways to wrongly stack legos for horrible results. (Your child may not care, so lego has low adoption bar regardless)
  - note: needs a lot of insight into/visualizing of the result.
    - little syntax (enforced): lego interface doesn't say no if you make a mess out of your color scheme, build your house at an angle, build your plane upside-down, etc.
    - mostly semantics (runtime-enforced, i.e. by the consuming human): do the colors fit together, does the construction make any sense, is it the right way up, etc.
  - [LEGO blocks and organ transplants](https://www.johndcook.com/blog/2011/02/03/lego-blocks-and-organ-transplants)
  - [DZone: It's just like putting LEGO bricks together .. or not?](https://dzone.com/articles/lego-bricks)
  - [Can Software Be Like Building Lego?](http://www.secretgeek.net/lego_software)
  - > I think the lack of reusability comes in object oriented languages, not in functional languages. Because the problem with object oriented languages is they’ve got all this implicit environment that they carry around with them. You wanted a banana but what you got was a gorilla holding the banana and the entire jungle -- Joe Armstrong, [Coders at work](http://codersatwork.com/)
    - minimal interface, implementation types inside, encapsulation, high cohesion, ...

- Properties/quality metrics:
  - Bad metrics:
    - raw numbers for lines of code in modules/classes/functions/files.  
      Mitigated in these proposals by using a ratio where anything over 1 is positive, and expectations can best be aligned among projects within a single organization such that you can compare numbers in a (relatively similar) domain.
    - arbitrary values as boundaries for maximum lines of code in function, class, file, module.
    - `TODO: ...`
  - Proper location of functions. (utilities are independent, business logic is dependent)
    - How to identify "utilities" as defined in this text? (everything static? How about collections?)
    - Business logic (is the context):
      - domain implicit by function of the program, determined by the other logic, data manipulated to benefit the domain
      - write then change (When the times change, logic changes. Semantics represents rules that are currently relevant.)
      - Needs to be tested thoroughly on every change.
      - Existing unit tests evaluate/prove operation of all logic after recent change of some logic. New unit tests prove added functionality inherent in new logic.
      - Use your utility logic with equal confidence for correctness as you would the programming language built-ins.
      - Extensive use of utility logic for high-level programming (few lines for much expressivity), readable.
    - Utility logic (is independent of the context):
      - domain determined by the input, the utility accepts input and performs function but has no internal domain knowledge, just type knowledge
      - write then abandon (Don't use if not applicable, don't delete because it might be used in other code-bases. Semantics never change for utility.)
      - Each utility needs to be tested thoroughly once, used many times.
      - Unit tests evaluate/make explicit the (boundary) conditions of operation for logic.
      - Provide logic that can be used without hesitation: clear, singular, reliable, straightforward, predictable, thorough, ...
      - Needs clear location so that utilities can be easily found when developer is in need for them, easily/universally accessible.
        - primary location (with ownership): next to the type, e.g. `java.util.Collections` utilities.
        - secondary location (no ownership): separate module in same package/path as type, in predictable/obvious module.
        - tertiary location: `TODO: ...`
    - metrics:
      - LoC in utilities vs. LoC in business logic, higher ratio indicates more reusable logic + smaller code-base for business application,
        - `TODO: can we define a ratio-boundary to indicate "logic too low-level" for business logic?`
      - less duplication inter-/intra-application
      - lines changed vs lines added: good for utilities vs good for business logic, respectively.
  - "High-level programming constructs" (extract "embedded" low-level logic into utility)
    - extracting logic into utility
    - defining "variable logic" into function parameter, creating higher-level function and reusable logic in separate function.
    - make use of interfaces, generics to make logic type-independent when possible.
    - sliding scale for function-to-OOP from perspective of interfaces: higher-order functions to multi-function interface definitions.
    - metrics:
      - lines of code compared to "high-level-ness" (expressivity of logic, e.g. single line represents 10 lines, so more expressive than as every line was an elementary statement.)
      - "high-level-ness": every line of code uses some private data, or a derivative of private data. (If not, there are intermediate lines of utility logic.)
  - Encapsulation of data + logic (push any other data up the hierarchy)
    - public methods must access private resources (transitively)
    - any private field must be modified together with another private field, such that there is a reason for the two private fields to be present in the same class.
    - metrics:
      - methods must access something private to be eligible as private method.
  - High cohesion, low coupling
    - High cohesion: data encapsulated with dedicated logic + dedicated (implementation) types
    - Low coupling: minimal (type) requirements on interface
    - metrics:
      - theory of regions should put all fields of a type in same region (`TODO: needs verification`)
        - region bounds indicator: when a changed, always b changed. While, when b changed, never a changed. (I.e. `a` never changed without `b`, `b` is changed without `a`) Furthermore, check if changes to `b` when `a` are restricted to some degree: difference between mutation through public interface (`b` when `a`) and privileged mutation (`b` when not `a`).
      - number of interfaces used in internal logic should move to 0.
      - smallest types (often interfaces) applicable should be used in interface.
      - minimal applicable interface for constructor-injected dependencies.
  - [SOLID principles](https://en.wikipedia.org/wiki/SOLID):
    - "Single responsibility principle" => theory of regions should put all private fields in same region. `TODO: verify if this is too strict or just right`
    - "Open-closed principle" => add-only for utilities, change (or add with eventual delete) for business logic.
    - "Liskov substitution principle" => minimal interfaces/types for interface of class/function. (minimizes coupling)
    - "Interface segregation principle" => single responsibility, minimal interface.
    - "Dependency inversion principle" => constructor injection for dependencies, minimal interface.
  - [GRASP principles](<https://en.wikipedia.org/wiki/GRASP_(object-oriented_design)>):
    - "Information expert" => encapsulation, single responsibility, ...
    - "Creator" => `TODO: ...`
    - "Controller" => `TODO: ...`
    - "Indirection" => minimal interface, constructor injection, ...
    - "Low coupling" => minimal interface, encapsulation, single responsibility
    - "High cohesion" => implementation using implementation types, encapsulation, single responsibility
    - "Polymorphism" => minimal interface, dependency, constructor injection
    - "Protected variations" => encapsulation
    - "Pure fabrication" => `TODO: ...`
  - Others:
    - "Don't repeat yourself", i.e. utilities are context-independent logic thus can be reused in other contexts/business-goals, within a project and over multiple projects.
    - Composition over Inheritance
    - Program to an interface, not to an implementation
    - No use of arbitrary numbers of maximum length of line, number of lines of code in function, lines of code in class, etc. => arbitrary numbers are meaningless and interfere with productivity, enforce random separation/splitting.
  - Utilities/private functions/etc. cannot absolve you from using `null` correctly, i.e. leaving out when not applicable and using when applicable and necessary.
    - In addition: having `null` "_automatically handled_" by all utilities is bad practice, because `null` has different meanings for different logic.
    - metrics:
      - Use of `null` for types like arrays, lists, sets, maps, strings. (any container-type, any non-singular type)  
        There is almost never a reason to use null for container types, as the empty container is almost always sufficient. `null` only useful to express absence of optional instance, not for empty/missing content.
  - Aim for minimal interface, i.e. interface with least number of methods possible.
    - sorting-interface using `compare`-method (Java) or `Less(x,y)`-function (Go).
    - 1-method interface functionally equivalent (`TODO: ?`) with function-parameter (higher-order functions, functional programming).

- Problems with control/structure of large programs:
  - repeating/duplicated common sequences of logic (availability/accessibility/location of utility logic)
  - data maintained at wrong level in the hierarchy, mixing mutually-independent data

- Definition of terms (terminology):
  - context-dependent/-independent: something is context-dependent if it relies on values and/or operations that are specific to the functional domain. Consider that the HTTP status codes are specific to HTTP, instead of generic to any networking application. One cannot make any such logic a generic networking utility, but parts may be extractable as a HTTP-specific utility. A variable that holds the request body is specific for the handling server software.
  - business logic: sequence of logic for a specific application, hence not reusable. `TODO: also manipulating business data?` `TODO: consider defining with state present, i.e. state gets accessed/mutated.`
  - utility (synonymous with) context-free/-independent logic, i.e. not business logic, i.e. not dependent on a specific application.
  - dependency: instance with logic that is needed but not part of the single responsibility, therefore injected instead of internal.
  - simple: reduced + deoptimized + specific  
    `TODO: should 'specific' be replaced with 'dedicated' to better cover the intentionded property? Dedicated as in, the logic is specific for the intended purpose, instead of an overgeneralized all-in-one solution to also happens to apply here.`
  - complex vs complicated: complex is inherent, complicated is made.
  - `TODO: continue here ...`
  - if-less programming, using multiple classes and polymorphism for conditional decision making. (https://levelup.gitconnected.com/if-else-is-a-poor-mans-polymorphism-ab0b333b7265?gi=d7c21566ac1a)

- Decision tree to support choosing appropriate design pattern:
  - Goal: instantiate object with all-valid values for complicated requirements:
    - constructor
      - Provide initial values at construction time such that even at construction time all memory is in valid state.
      - unless: complicated combination and/or order-dependence of constraints/restrictions/values for multiple fields: builder pattern
    - builder pattern
      - Provide builder object which can be called multiple times to provide relevant initial values for object. When `build()` is called, the set of values must be valid under all constraints/restrictions, then constructor is called (privately) with validated set of values.

- Misc
  - handling/mitigating problematic situations through protocols/semantics:
    - TCP congestion control undermined (large delay are no longer an indication, packet drops are no longer an indication) through excessive buffer sizes in routers (BufferBloat)
    - arbitrary number '3' in TCP 3-way handshake, as the '3' does not represent any kind of natural boundary. Instead, it's a reasonable amount that generally works. However, consequences of not being on a natural boundary: too much overhead for lossless infrastructure, too many problems on (very) lossy infrastructure. The handshake works, but it's an arbitrary number chosen to "balance things out" under reasonable circumstances. (`TODO: double-check if this example is good? Should we use 4-way handshake for closing TCP, better example?` <https://en.wikipedia.org/wiki/Transmission_Control_Protocol#Connection_establishment> <https://tools.ietf.org/html/rfc675> [The Two Generals-problem](<https://finematics.com/two-generals-problem/>))
      - TCP Fast Open allows it to be 2 messages. It does not fix the underlying acknowledgement problem.
      - Multiple confirmations is a necessity due to how the protocol is designed. Multiple (partial) open and closed states. Design dictates technical requirements.
    - Linux kernel file system management code vs. file systems: file systems required to fail immediately, prepare a-priori for possible gracefully exiting failure situation. Meanwhile, file system management code does not fully respect own specification, resulting in failure code paths in file system implementations that are presumably hardly ever tested/touched. Consequence: cannot fix bad behavior of file system management, because it would risk touching untested (failure) code paths of existing file systems that are now considered very reliable/trustworthy. In addition, due to noble but foolhardy (`TODO: spelling?`) attempts to mitigate block device issues for the file system, file system implementation cannot reliably determine to what extent data is written successfully. Even more ambiguity and uncertainty is introduced that is already naturally present in hardware failure cases. ([Re: PATCH iomap: Handle I/O errors gracefully in page_mkwrite](https://lkml.kernel.org/lkml/20200605003159.GX2040@dread.disaster.area/))
  - Spaghetti-code = relationship between data fields only expressed through semantics; due to mixed-up logic mutating various pieces of data interchangibly, relationship/isolation of data is unclear - can be guessed at best. Hence dependency on the logic: code that is difficult to read, must be read to understand the semantics. Hence spaghetti-code. However, the mixing-up itself isn't the problem in itself.
  - prevent adding utilities to the standard library because you'll always lag behind? (`TODO: does this make sense?`) There are always more elaborate utilities to write, while there are only so many concerns to restricting the internal data fields (privileged logic). There are varying degrees of generality in the construction of these utilities.
  - what is part of functional domain: (ask yourself what is in a specification document, such as an RFC)
    - constants prescribed by domain specification, (implicit and explicit)
      - values as explicit literal values used in logic
      - values implicitly expressed as logic, e.g. `a * a` is a squaring and the fact that you should square may be part of the specification, based on case distinctions where different logic is invoked.
    - "recipes" of types and sequence of operations to perform to achieve the desired result,  
      specific types and order of data mutation (but not necessarily the logic to mutate in itself, that can be a utility). Or maybe better said: the end result of a series of data mutations, of which arbitrary subsets of lines of code can be extracted as a utility as long as it by itself is context-independent in nature, i.e. the logic makes sense if the type that it acts on by itself is sufficient to make it make sense. (No reliance on domain knowledge or directed by domain requirements/conditions/restrictions.)
    - local variables holding (intermediate) values for a functional calculation, (may also be part of a utility, it depends on whether or not it is part of the logic sequence that is context-(in)dependent)
    - `TODO: what other types are part of the functional domain (context-dependent)?`
    - Definition: "relative/local functional domain" for sake of qualification: functional domain for different classes (pref. different packages). An `ArrayList` is its own little functional domain for purpose of storing elements in list structure.
      - Based on _relative functional domain_, utility logic is anything without privileged access - "usage patterns". Business logic is anything that requires state maintenance: (instances) with state, hence privileged access.
      - Utility logic can be identified as any logic provided through static methods (requiring no privileged access). (assumption: no static state management)
  - `TODO: continue here ...`
  - How do functional programmers view constants/literals? Same way as procedural programmers? Do they reason about them differently, or treat them differently? What's the mentality for hardcoding vs. providing as parameters?
  - How to define/identify the boundaries of a design: boundary at cross-over point where a requirement is changing/contradicted?
  - Design vs. architecture:
	- Design looks inward: requirements of the solution to make the implementation work for the problem.
	- Architecture is everything outside of the solution, i.e. what are the requirements for my solution to make it fit in the surrounding landscape/environment?
	  - weak req: must be this programming language.
	  - strong req: must interface using this format/semantics such that it fits with the rest of the servers in the landscape.
	  - in class: instances used internally (only) can be stored by their implementation type.
	    - NOTE: There is no reason for hiding the implementation type as it is created internally too.
	    - NOTE: there is a reason to not-hide the type: if you use this type internally, you need to know al its idiosyncracies in order to use it efficiently. If you hide the implementation type, you lose access to implementation-specific methods, thus some of its benefits. (Some benefits are in the implementation therefore automatically available.)
	    - if exposed to the user - assuming this is possible and does not cause problems due to mutability - then expose (possibly) through (more restricted) interface type. You do this to guard the guarantees of your API. You can change your implementation type freely as long as it satisfies the return type (interface).
	    - if injected from outside (constructor, setter), store by the type minimally required to make the implementation work: statement is clear, this is what the type needs, so this is what we're asking for.
- Framework vs. library
- Maximum density of business logic (statements)?
  - single statement per line (as opposed to multiple nested statements on same line),
  - each line uses privilege (otherwise line could be part of utility)
  - `TODO: how to decide minimal nr of lines in use? Further minimalization at cost of readability or excess call depth?`
- Design and implementation of applications contains bottom-up components: even if you design top-down to force yourself to think from the functional perspective, you are still required to define the implementation details in a bottom-up approach.
  - select the (minimal) amount of data necessary to maintain information on a certain concept; put it in an object; give the object a name that illustrates what the combined data represents.)
  - E.g.: filter that drops characters if blacklisted in the filter. Class may have single field containing array of illegal characters. A zero-length array implicitly means that the filter is disabled.
    - An attempt to define an implementation top-down would have you reason about:
      - enable/disable filtering
      - list containing blacklisted chars
      Ask yourself: "What data do we need to store to actually discover enabled/disabled and which chars are blacklisted?" Find out that a enabled/disabled boolean is already redundant. Note that TDD tries to do this through rules intended for blind obedience instead of by explaining why this works.
- Composition over inheritance, i.e. use inheritance sparingly, mainly interfaces.
- Every class is either `abstract` or `final`. Do not allow intermediate parents to also be directly usable, as this creates complications when changes are made.
- building software "is like extending up-side down triangle" (need a better word for that ...). That is, the platform and language features form the origin point and small surface: it's your basis that any applications build on. You'll have to extend the basis as part of the programming work, i.e. establish a "domain" of composite types and restrictive primitive types that represent particular concepts exactly. Then, on top of that you build the business application. The business application can be small while still being full-featured, if the domain and common/shared logic is extensive.
- preconditions/postconditions/invariants
- Identifying contradictory requirements: a good design almost always includes requirement to "panic"/escape/abandon execution once an unhandleable error occurs. However, there are reasons why you may not want the program to crash. These are contradictory, and these are contradictory because of some very specific circumstances:
  - the crashing requirement is design-based - good logic needs to be predictable and reliable, hence aborting upon encountering/running into a uncorrectable problematic situation.
  - the recoverying (note this is not about not crashing, instead it's about recovering after the faulty situation occurs) is architecture-based - although from the logic's perspective we're doing a good thing, it's the context in which the logic operates that does not want the logic to abort. Hence a architectural requirement to be crash-resistant / mitigate crashes. Such requirements are typically facilitated through a framework that has its own requirements, i.e. hosted logic that is faulty, must not crash the framework itself. (Further requirements may include restarting/logging/etc.)
  - Utilities: problem with managing dependencies in utility libraries: some programming languages will always include the full library and all its dependencies. A single utility library as dependency, while very powerful and convenient, would automatically include all available dependencies, even if they are not necessary. Ideally, one would want to include the dependencies only if those particular utilities are actually used. Some programming languages, like Go, will - at compile-time - select the code that is in use and drop anything that isn't statically connected. A "monolithic" utility library cannot work efficiently otherwise. The only solution to this is for every dependency to have a corresponding utility module.
    - one library for _standard library_
    - one library for each dependency (aim to minimize number of dependencies required for use of the utility module)
- (Context) Dependency Injection:
  - `constructor > method > field injection`: prefer constructor over method over field injection from the perspective of control. With field injection, one could end up with objects that have inconsistent state, i.e. state that violates invariants, resulting in possible corrupt state/execution, not necessarily automatically corrected or correctable.
- Java: distinction between package-private (default) access for package-local sources, and `protected` access for derived classes. (Issue: `protected` access is also accessible to package-local sources. This may not be intended as such. Use `@ForOverride` annotation of Google ErrorProne package for guarding control.)
- Restricted as in: the highest hierarchical position in which something can be implemented: only go down to language syntax-level, if cannot be implemented as library. ==> easier to maintain, easier to adopt, preserves minimalism in language syntax (reduced complexity). (There can be benefits in adopting feature in language syntax, e.g. more elegant expressions, more readable, better support for managing feature - e.g. memory management.)
- Messages vs events:
  - Messages are constructed for a specific purpose, sent to a specific queue so message reaches the intended system.
  - Events are emitted to reflect a certain change in situation of a system. Any interested party can subscribe to act upon any events that are deemed relevant.
  - Messages are about dictating the next step, while events are emitted to announce recent state changes.
- Any sensible distinction between levels of architecture depend on (clear) identification of strict boundaries between levels of abstraction / separation of responsibilities.
- `TODO: does this even make sense. Is there any semantic difference between responsibilities and concerns??? I think this one is bullshit :-P` Separation of responsibility is about abstractions, while separation of concerns is about high cohesion.
- Heart-beat messages keep connections alive. Sending many heart-beat messages is only a matter of data use, not bandwidth, as heart-beat messages are only sent at times where traffic is too low, hence low bandwidth.
- `solution = design + implementation`?

Taking control of quality:

- programming language: syntax vs. semantics, null-handling, static analysis + annotations, ...
- program structure: separation of concerns, design patterns, coupling/cohesion, respect natural boundaries for design/architecture changes, ...
- programming mentality: respect errors, handle errors instead of ignoring, 
- run-time analysis: profiling, benchmarking, tracing, ...
- Erlang fundamentally understood handling errors: you cannot produce crashes and recover from crash on the same level of abstraction. The tooling that produced the crash did so due to unanticipated circumstances. Erlang understood that the overlying supervisors are crucial for managing programs that run into issues that cannot be handled immediately.
- Squashed merges: (commit history)
  - get rid of work-in-progress development history (trial-n-error)
  - don't force developers to write "valuable"/serious commit messages during trial-n-error, in unsure times regarding the actual final implementation.
  - give final chance of reflection writing the definitive commit message.
  - as implementation has actually crystallized, definitive commit message contains clear, certain information.
  - "production commit history" contains fewer commits.
  - cherry-picking becomes easier due to all necessary code changes centralized in single commit instead of combined in merge-commit but actually many commits.
  - merge-squash gives developers a final chance to look at the definitive code changes:
    - chance to identify independent changes and extract those.
    - chance to reflect on changes made to distill changes that made it to the final solution.
    - chance to reflect on changes and evaluate whether or not everything was tackled, fine-tuning changes.
    - less difficulty in merging compared to rebasing due to only applying a single batch of changes.
    - transparently handles intermediates merge-commits from mainline/"master" due to these changes being mixed transparently.
    - there is no point to keeping dev history such that you introduce a feature in "smaller commits" as these commits contain incomplete/non-functional work. You don't want to split up by time, but instead by features. You have more benefits by performing an squash-merge and selectively picking changes from there to commit independently.
- people are more confident contributing to the linux kernel than to projects in high-level languages:
  - procedural code is easier to read, i.e. less syntax thus fewer unique code structures to understand.
  - less syntax, easier to teach yourself a suitable, readable, maintainable code-structure.
  - no OOP - no OOP syntax - no confusion and endless discussion on how to structure the code.
  - this is contrary to expectations because: more low-level, closer to hardware makes coding more difficult - more sensitive to timing issues and more elaborate memory management.

"Theory of regions" `TODO: am I using this theory correctly?`:

- Most likely, _theory of regions_ applies to state invariants.  
  However, not often are state invariants defined, so next best thing is observing simultaneous (cooperative/correlated) privileged mutations.
- presence in region is determined by dependency on the field.
- presence in inner region if outer region only needs "usage" access, i.e. no "privileged" access, to data.
- different regions: `a` is always mutated with `b`, but `b` is mutated without `a`.
- different regions: `b` is only read while mutating `a`, while `a` is mutated independently.
- different regions: `b` is not correlated with any other field, but has restricted set of valid values.
- different regions: field is conditionally addressed.
- a single field may be modeled in its own region, however due to lack of restricted access no "wrapping class" is needed. For example, integers where all values are considered valid, need not be protected, i.e. no need for access privilege.
- cohesion is maximal if all fields are in the same region, degrades if fields from different regions combined.
  - degrades because nested region indicates possible need for composition. Due to dependencies of various strengths. Evidence of multiple concerns are mixed up.
- coupling is minimal if public API of an object uses most-general possible types.
  - `TODO: is public API any concern of coupling? It seems more related to internal implementation of a class.`
  - inversely proportional to cohesion? Further improve coupling by using utilities.
  - can we conclude that coupling is derived property, merely consequence of other bad coding practices?
  - note that coupling is determined by the separation of concerns. Coupling is minimal if implementation cohesion is high (maximal), i.e. least necessary logic to make implementation work.
  - is coupling even a very useful metric? It seems like it is a composite. Cohesion is very specific to decent state management.
- need a very strict definition for the joint use of multiple fields in same logic (function/method). The problem is that many functions (in spaghetti code) do multiple things, i.e. _resize container if too small_ + _add element_. The resizing code and related fields are executed strictly earlier than the element adding code. The element code itself subsequently only checks a field value (usage privilege only). There should be a definition possible that define how the two field accesses relate in equal or unequal level, i.e. in same region or referring to nested region.
  - consider defining regions as all fields that require _privileged access_ in the same "chain of logic". (Need to define "chain of logic", but basically any sequence of statements that together complete a single operation. Ideally the body of a function, but in case of spaghetti-code a function may do/manage more than a single thing.)
- By reasoning this way, `java.util.ArrayList` can be divided into a managed growing array (empty, capacity, resizing, access without direct array-handle), and managed interaction with list (size, modcount, insertion/removal, appropriately shifting remaining elements). (`TODO: see diff file for demonstration of separating ArrayList. Possibly we can also reuse the "Growable Array" logic for Vector.`)
- By same reasoning, we seem to be able to split up `java.util.HashMap`. Of course, this does not imply that the current implementation isn't good enough. The current implementation is optimized, increasing compactness for code duplication.
- For refactoring:
  - Extract and use utility functions.  
    Density indicated by minimizing lines not using privilege.
  - Use region theory to identify nested encapsulations:
    - look at all fields, understand how each field depends on the others. (i.e. state invariants)
    - which fields require privileged access simultaneously? (e.g. fields for data-array and size)
      - subsequent pieces of logic do not count, as they could be separated trivially.
      - requires both to need privileged access. Usage-access for one, indicates that they may very well be different regions.
  - you are not required to understand all the logic of a class. Merely the access patterns for various fields. Use of terms 'privileged' and 'usage' intentional, as we cannot know in advanced what is considered "harmful" access requiring restriction.
- Rationale for distribution of data (fields) over classes:
  - distinguish between _privileged_ and _usage_ access, based on whether or not "internal knowledge" is required to maintain consistency (valid state). _Usage_-access is unchecked restricted access that is acceptable due to that type of access cannot lead to corrupt state. Therefore, relies on encapsulation (and state invariants) to do their work. Therefore requires mere _use_ of the object, as opposed to _privileged_ access to internal data where one has to be careful with mutations.
- strict encapsulation according to regions + requirement of privileged access for logic => class contains minimal amount of fields with minimal (i.e. strictly necessary) amount of logic. Ensures small classes and manageble code.
- RAII, Dependency Injection (through constructor injection)
- Does it make sense to have inheritance without an abstract method (or constructor)?
  - injecting values: at instance-construction time.
  - usage as part of other feature: composition.
  - incomplete implementation, abstract method as place-holder: abstract class with abstract method.
- The essense of annotations for use in static analysis: bridging the separation between the implementation of a method and its use. (the method signature, including annotations is common information available in both cases, bridges the gap.)

Refactoring, steps in order:

1. Analyze and maximize coupling: use region theory to determine class members, refactor class.
	1. Ensure correct selection of fields. (encapsulation, coupling, state invariants)
	1. Ensure minimal logic to expose data without sacrificing consistency. (minimizing logic, simplicity, maximizing use cases)
1. Extract utility logic or replace logic with existing utilities. (minimizing logic, utilities, reusability)
1. Write utilities to complement the minimal logic of the class with common usage patterns for this class. (reusability, minimizing business logic)

<http://codinghelmet.com/articles/what-makes-functional-and-object-oriented-programming-equal>
<http://users.ece.utexas.edu/~adnan/pike.html>
<http://wiki.c2.com/?ClosuresAndObjectsAreEquivalent>
<https://www.nasa.gov/content/dtn>
<https://buttondown.email/hillelwayne/archive/whats-the-deal-with-message-passing-anyway/>
<http://literatejava.com/exceptions/checked-exceptions-javas-biggest-mistake/>

---

OOP-structure --> encapsulation --> access boundary --> logic bound to class to enforce/distinguish access control level --> conditional access to privileged members (fields) --> logic bound to class uses privileged access --> protection of state invariants must be ensured within that logic --> fields in class always consistent with state invariants. (`TODO: probably better to reverse the logic and reason from preserving state invariants on internal fields hence encapsulation required.`)
  - any call to a public method must ensure that after method logic finishes, state invariants are again preserved, therefore fields in consistent state. (purpose is to guard state when object used)
  - any call to private method should not need to be concerned with this, as it is already called by public method having this as a requirement. (purpose is to offer shared logic accessible from many public methods)
    - private methods can only be called by logic that has privilege, i.e. other private/protected/public methods. So responsibility of preserving state invariants is already delegated(`TODO: <-- not the word I was looking for`)
    
OOP-structure --> inheritance --> partial implementation --> logic referencing abstract method --> incomplete logic with carefully designed blank to be filled in --> common incomplete logic that guides/nudges towards particular implementation direction (NOTE: important difference from interfaces: latter do not nudge implementation into a particular direction)

OOP-structure --> polymorphism --> multiple implementations with uniform interface --> impartial of implementation --> each implementation total control over solution direction --> interface defines how to _use_ the implementation, but says nothing about how the implementation works in the background.
  
OOP-paradigm --> concurrency --> independent threads of control --> independent pieces of logic running concurrently (coroutines) --> thinking of application as system of cooperating pieces of logic --> running independently or cooperatively --> at least one live control flow at time --> multi-tasking (multi-threading/-processing if execution is concerned) --> structure of program (different from traditional interleaved programs by being non-deterministic, i.e. more than single inter-leaving of control flows, non-deterministic for different CPU configurations).

technical debt = any deviations from the 

<https://twitter.com/unclebobmartin/status/1306581616983183361>


- Building from the foundation up:
  - define structs/classes to group related data:
    - only add another field if the data cannot be derived in any other way.  
      There is no need to "add another field for readability", the knowledge of how (private) fields works, should remain part of the class' internal knowledge. Any complexity or "knowledge on interpretation" should stay within own class. If such an interpretation needs to be exposed, define a method for it.
    - logic for interpreting fields must be part of the same class logic _only_.
  - define methods for logic that cannot be written without access to internal logic/fields.

TO READ: https://www.cs.cmu.edu/~aldrich/papers/objects-essay.pdf

Include notes/remarks about "logical fallacies". These are the types of things that are good to be aware of.



> “Perfection is achieved, not when there is nothing more to add, but when there is nothing left to take away.” --  _Antoine de Saint-Exupéry, Airman's Odyssey_
- `simplicity = perfection`
  - THIS CANNOT BE THE CASE, BECAUSE YOU CAN HAVE PERFECTION WITHOUT FULLY APPLYING OPTIMIZATION,
    - e.g. because you care for simplicity/straight-forwardness/readability/etc. more.
    - "_taking away as much as possible_" seems to be common understanding. So, "reduced + specialized"?
    - "_completeness_" as in, does everything you need, is assumed by question/input.
    - "_perfection_" given starting point "simplicity, and move away into each of the dimensions to achieve the desired improvement. Perfection is very often not adding more clutter and more options, so "reduced" and "specialized" properties are typically preserved. Optimization is often desired for benefits of squeezing out most speed. Sometimes having to option of automatic optimization, however "lousy" in some cases, is the nicer option. You have all the benefits op simplicity, with possible free improvements given increasing capabilities of computer optimizations.
  - (by definition) simplest = minimal in all three dimensions, i.e. nothing to take away in any of the three dimensions of complexity.
  - (assumption) definition of simplicity, i.e. the three dimensions, cover all aspects. (completeness?)
  - (assumption) definition of simplicity, i.e. the three dimensions, are non-overlapping. (soundness?)
  - (???) "... nothing left __to take away__." would, by implication, be in the domain of simplicity.
  - (???) "... __nothing left__ to take away." would, by definition, correspond with _simplest_.
  - _perfect_ sometimes defined in terms of lack of flaws. Can we say that flaws correspond to complexity? Is a bit of a stretch, maybe I'm forcing things, but at the same time there is a ridiculous amount of analogy between the two concepts.
  - (???) tricker to reason about "perfection" because scope is typically larger. Not many discussion on when a single class or function is "perfect". There is little point to it, even though it is certainly possible. There may be "perfection" in a fully tuned, optimized cryptographic implementation.
- `perfection = reduced + specialized + (range from unoptimized to optimized)`?


optimizations:
- low-level implementation that surpasses the simple version through additional control of singular hardware platform.
- memory layout: more control over memory efficiency: placement, access, (cache lines?)
- cache control: data structures/batches of data aligned such that allows for keeping in/maximum use of caches/caching. (E.g. perfect control for efficient layout, accessibility)
- NUMA-aware: be aware of alignment of processors, bandwidth of memory channels, access structures of memory hardware, for most efficient routes to accessing memory for each respective CPU.
- GPU loading memory efficiency, selective appropriate use of GPUs.
- etc.

---

Perfection requires a target: fastest, allocationless, no side-channels, simplest, for x architectures, suitable for automatic (compiler/vm) optimization, suitable for automatic garbage collection, no side-effects, pure implementation, functional style, limited to certain maximum size data types due to architectures, compatibility, ease of use, accessibility, etc.

Given goals for this particular instance of "perfection":
- starting point: simplest (simplicity), then determine steps to take towards perfection.  
  _Starting from simplest should give the clearest paths for improvement, not clouded by unnecessary distractions._
- Taking steps towards the goal, because the intended goal is prioritized over simplicity: step by step until goal reached.

Perfection:
- nothing to take away: reduction/specialization, no excessive features/unnecessary parts
- nothing to add: completeness?, requirements present (and specific)
- exact condition??? (necessary and sufficient conditions)
