# Simplicification

## Conditions

- Using this mechanism, you cannot follow the established way-of-working where you write _getters_ and _setters_ for everything. This is way-of-working is not compatible with what is to be accomplished here.
- `get` and `set` methods follow convention: `O(1)` operation, merely read/write field, possibly with conditional.
  - easy to analyze automatically

## Rules

Reduction rules that should automatically result in "Minimal-OO" compliant design/implementation through iterative rewrite actions.

- Apply region theory to identify various regions and distribute class structure accordingly.
  _A class consists of fields representing data in a single region. Separate regions implies separate classes, either nested or independent._
- If field has unconditional _setter_ and _getter_ => make field `public`.
- If field is `final` and has unconditional _getter_ (and no _setter_) => make field `public final`.
- If class has single field and field is `public` => use field (type) directly. (I.e. remove class)
  - `TODO: !!! does this conflict with type aliases, i.e. class for single type (primitive or otherwise) to ensure that values of different units do not get mixed up.`

## Region theory / Theory of regions

__Goals__:

1. Identify inseparable subset of fields  
   _Each identified region should act as boundary for separability, because if region theory cannot draw boundary there is a bi-directional relationship._  
   `TODO: this needs to be thoroughly verified. Is huge assumption.`
    - intention is to identify bi-directional relationships such that no one single field can be separated from the rest without risking the chance of data inconsistency, meaning _invariants_ are violated.
    - encapsulation
    - KISS
    - SRP
    - simplicity (reduced, specialized, but not 'unoptimized' as optimized field selections would also have bi-directional dependencies)
    - minimal-OO
    - high cohesion
1. Identify separable parts and generate (nested) classes with fields for each identified region.
1. 

- `if class has multiple independent fields, split into separate classes.`
- `determine relationship based on aggregate of interactions between two fields in all class logic, i.e. all methods of class.`

## Considerations

- automated tooling relies on quality of current code for analysis. If there are many methods that do too many things, it might be impossible to reliably analyze bi-directional dependencies. I.e. there will be false positives due to the amount of stuff that happens in methods and the amount of methods that undeservingly reside in the particular class.
- the prevailing logic is that logic within a  class' method must need "privileged access" to justify being there. Long sequences of logic without using privileged access can be identified as being suitable for extraction as utilities.
- it is hard to determine -- for a sequence of logic -- how many utilities this represents, hence extraction is a manual process.
- do we need a threshold/heuristic to determine how much non-privileged activity we must allow to be reasonable?
