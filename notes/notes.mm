<map version="freeplane 1.8.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Methodical engineering" FOLDED="false" ID="ID_1907545210" CREATED="1638720556624" MODIFIED="1638721485912" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_icon_for_attributes="true" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="Ubuntu" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" COLOR="#990000">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" COLOR="#ff0000">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="13" RULE="ON_BRANCH_CREATION"/>
<node TEXT="utility-logic" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="right" ID="ID_1215037967" CREATED="1638665735295" MODIFIED="1638723010469">
<edge COLOR="#0000ff"/>
<node TEXT="sequences of &quot;usage logic&quot;" ID="ID_947626929" CREATED="1640133748802" MODIFIED="1640133785866">
<node TEXT="ease of use" ID="ID_472472423" CREATED="1640133786449" MODIFIED="1640133788067"/>
<node TEXT="ease of access" ID="ID_1113318305" CREATED="1640133788313" MODIFIED="1640133790333"/>
<node TEXT="minimal constraints" ID="ID_533876959" CREATED="1640133838345" MODIFIED="1640133843660"/>
<node TEXT="minimal/no context" ID="ID_1997060794" CREATED="1642449098940" MODIFIED="1642449105358"/>
<node TEXT="domain-less" ID="ID_1380542024" CREATED="1642449105684" MODIFIED="1642449124242"/>
<node TEXT="deduplication" ID="ID_438319732" CREATED="1640133790721" MODIFIED="1640133796294"/>
<node TEXT="working on existing concern (type/class)" ID="ID_1606311962" CREATED="1640133866417" MODIFIED="1640133884865"/>
</node>
<node TEXT="Any type can be the subject for utilities, given that utilities are the collection of &quot;usage logic&quot; of that type." LOCALIZED_STYLE_REF="styles.important" ID="ID_559503032" CREATED="1639093052156" MODIFIED="1639093088371"/>
<node TEXT="Figure out distinguishable concerns for optimal, straight-forward composability of utilities" LOCALIZED_STYLE_REF="styles.important" ID="ID_1326707080" CREATED="1642450511844" MODIFIED="1642450555034"/>
<node TEXT="pure utility functions" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1916994690" CREATED="1638665741934" MODIFIED="1642450495975">
<node TEXT="properties" LOCALIZED_STYLE_REF="styles.topic" ID="ID_728316866" CREATED="1638666124423" MODIFIED="1638722434456">
<node TEXT="for abstract and concrete types" LOCALIZED_STYLE_REF="default" ID="ID_1730676157" CREATED="1640131436357" MODIFIED="1640131444064"/>
<node TEXT="contextless" LOCALIZED_STYLE_REF="default" ID="ID_882148082" CREATED="1638665756952" MODIFIED="1639962179970"/>
<node TEXT="idempotent (stateless)" LOCALIZED_STYLE_REF="default" ID="ID_173556436" CREATED="1638665924830" MODIFIED="1639962182160"/>
<node TEXT="sequence of usage logic captured in functions" LOCALIZED_STYLE_REF="default" ID="ID_1476496405" CREATED="1638665908670" MODIFIED="1638666983229"/>
<node TEXT="concern: single type" LOCALIZED_STYLE_REF="default" ID="ID_701821042" CREATED="1638665938838" MODIFIED="1639962095065"/>
</node>
</node>
<node TEXT="utility classes" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1938505800" CREATED="1638665948022" MODIFIED="1638737762277">
<node TEXT="Basically, collection of pure utilities with some of the parameters sourced from (internal) class state. State may be publically accessible, as long as it cannot be influenced (to avoid interference/manipulation)." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1963181664" CREATED="1638737770538" MODIFIED="1638737881025">
<node TEXT="class state always immutable?" ID="ID_789328849" CREATED="1638737927278" MODIFIED="1638737941446">
<icon BUILTIN="help"/>
</node>
<node TEXT="class state, if hidden, only to avoid abiility to mutate. (violates idempotence)" ID="ID_1336761660" CREATED="1638737955162" MODIFIED="1638738063428">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="properties" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1398456941" CREATED="1638666111582" MODIFIED="1638722434468">
<node TEXT="for abstract and concrete types" LOCALIZED_STYLE_REF="default" ID="ID_1778915256" CREATED="1640131449734" MODIFIED="1640131454139"/>
<node TEXT="context through initialization (immutable context)" LOCALIZED_STYLE_REF="default" ID="ID_364378873" CREATED="1638665956073" MODIFIED="1642449858766">
<node TEXT="some state with preinitialized values to avoid repeating/need to communicate" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_899000856" CREATED="1638665990126" MODIFIED="1639962038965"/>
</node>
<node TEXT="idempotent (stateless)" LOCALIZED_STYLE_REF="default" ID="ID_862000047" CREATED="1638665974454" MODIFIED="1639962187565"/>
<node TEXT="sequence of usage logic captured in functions" LOCALIZED_STYLE_REF="default" ID="ID_773879208" CREATED="1638665977303" MODIFIED="1638666983258"/>
<node TEXT="concern: single type" LOCALIZED_STYLE_REF="default" ID="ID_1573185572" CREATED="1639962101912" MODIFIED="1639962105234"/>
</node>
<node TEXT="Any class with fields directly initialized through constructor are utility classes?" ID="ID_945930011" CREATED="1638737521308" MODIFIED="1638737748086">
<icon BUILTIN="help"/>
</node>
<node TEXT="presuppose a certain way of working (i.e. initialiization of context with certain user-specified values) such that the API can be simplified to the benefit of repeated use, while maintaining further benefits of pure utility functions." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_900017396" CREATED="1642450422465" MODIFIED="1642450487255"/>
<node TEXT="Might have multiple classes for varying combinations of parameters for initializing (partial) context. Especially if initialization is sensitive to which parameters and interdependency/-consistency." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_91350909" CREATED="1642448897706" MODIFIED="1642449045921"/>
</node>
<node TEXT="converters" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_694217857" CREATED="1638666015430" MODIFIED="1638722444621">
<node TEXT="properties" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1592982139" CREATED="1638666104704" MODIFIED="1638722434471">
<node TEXT="only for concrete types?" LOCALIZED_STYLE_REF="default" ID="ID_45495817" CREATED="1640131458253" MODIFIED="1640131482024"/>
<node TEXT="initialized (context)" LOCALIZED_STYLE_REF="default" ID="ID_1766892760" CREATED="1639962212790" MODIFIED="1639962220508"/>
<node TEXT="stateful" LOCALIZED_STYLE_REF="default" ID="ID_1830401457" CREATED="1639962220871" MODIFIED="1639962223714"/>
<node TEXT="concern: 2 types (one is source, one is target)" LOCALIZED_STYLE_REF="default" ID="ID_819410916" CREATED="1638666022434" MODIFIED="1639962430130"/>
</node>
<node TEXT="encoders" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1200863501" CREATED="1638666137534" MODIFIED="1638722723578">
<node TEXT="Encoding exists to have a form for data that can be handled in a environment that is not tuned specifically for this type of data. The encoded format is often very elementary, therefore almost universally suitable." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1098315001" CREATED="1639092506182" MODIFIED="1639092598536"/>
<node TEXT="any type T &lt;--&gt; bytes" LOCALIZED_STYLE_REF="default" ID="ID_774573433" CREATED="1638666149687" MODIFIED="1638666983317">
<node TEXT="Based on a source type and bytes as destination type. Both exposed." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_441434272" CREATED="1639960829227" MODIFIED="1639960855086"/>
</node>
<node TEXT="examples" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1532955661" CREATED="1638666197031" MODIFIED="1639092622530">
<node TEXT="UTF-8" LOCALIZED_STYLE_REF="default" ID="ID_1215448953" CREATED="1638666200296" MODIFIED="1638666983327"/>
<node TEXT="base64" LOCALIZED_STYLE_REF="default" ID="ID_1204115821" CREATED="1638666204510" MODIFIED="1638666983332"/>
<node TEXT="numbers in two&apos;s complement" LOCALIZED_STYLE_REF="default" ID="ID_857986718" CREATED="1638666293073" MODIFIED="1638666983338"/>
<node TEXT="arbitrary-length numbers in array of bytes (BE/LE)" LOCALIZED_STYLE_REF="default" ID="ID_611643301" CREATED="1638666350224" MODIFIED="1638666983344"/>
<node TEXT="..." LOCALIZED_STYLE_REF="default" ID="ID_786813037" CREATED="1638666299591" MODIFIED="1638666983350"/>
</node>
<node TEXT="Encoders pass target type back to user as result. (Either bytes, or T if applied in reverse)" LOCALIZED_STYLE_REF="styles.important" ID="ID_33793358" CREATED="1639092967932" MODIFIED="1639093002425"/>
<node TEXT="When is a representation an encoder? Any representation can be dumped to disk as-is. Does it make sense to make this distinction?" LOCALIZED_STYLE_REF="styles.important" ID="ID_1384114379" CREATED="1640131512916" MODIFIED="1640131536577"/>
</node>
<node TEXT="representations" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_682996631" CREATED="1638666172118" MODIFIED="1638722727851">
<node TEXT="So all of these optimizations?" LOCALIZED_STYLE_REF="styles.important" ID="ID_687592068" CREATED="1639961403528" MODIFIED="1639961426638">
<icon BUILTIN="help"/>
</node>
<node TEXT="Representation is often &quot;convenient for processing&quot; but not &quot;convenient for reasoning&quot;, therefore hidden in the implementation (internals)." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1360872274" CREATED="1639092479301" MODIFIED="1639959942198"/>
<node TEXT="any type T &lt;--&gt; simpler type/primitive" LOCALIZED_STYLE_REF="default" ID="ID_1769748348" CREATED="1638666176873" MODIFIED="1638666983376">
<node TEXT="Based on an arbitrary interface type, and an internally used implementation type. Only interface type is exposed." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1895393911" CREATED="1639960874353" MODIFIED="1639960913478"/>
</node>
<node TEXT="examples" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1861418198" CREATED="1638666307871" MODIFIED="1639092164066">
<node TEXT="arbitrary-length numbers in array of 64-bit integers for easy of computing" LOCALIZED_STYLE_REF="default" ID="ID_914484329" CREATED="1638666374604" MODIFIED="1638666983389"/>
</node>
<node TEXT="goals" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1418887760" CREATED="1639092155232" MODIFIED="1639092168675">
<node TEXT="satisfy limited space due to constraints" ID="ID_1012729093" CREATED="1639092199478" MODIFIED="1639092211592">
<node TEXT="two&apos;s complement for CPU registers" ID="ID_689005903" CREATED="1639092225231" MODIFIED="1639092234016"/>
<node TEXT="floating point representation of decimals" ID="ID_317738746" CREATED="1639092234767" MODIFIED="1639092252074"/>
</node>
<node TEXT="maximize efficiency in limited space due to constraints" ID="ID_1236704986" CREATED="1639092215438" MODIFIED="1639092222544"/>
<node TEXT="effective processing of data due to design (constraints) of the language" ID="ID_1738271421" CREATED="1639092175432" MODIFIED="1639092423115"/>
</node>
<node TEXT="Representations keep target type internalized, i.e. not exposed to user." LOCALIZED_STYLE_REF="styles.important" ID="ID_87108837" CREATED="1639092928838" MODIFIED="1639092962051"/>
</node>
<node TEXT="This classifications is probably extremely inconsistent and has certainly room for improvement." LOCALIZED_STYLE_REF="styles.important" ID="ID_750045915" CREATED="1638666412623" MODIFIED="1638723809080"/>
</node>
<node TEXT="distinguishers" LOCALIZED_STYLE_REF="styles.topic" ID="ID_771330465" CREATED="1642450604672" MODIFIED="1642450607990">
<node TEXT="various flavours of context-initialization" ID="ID_20985310" CREATED="1642450610832" MODIFIED="1642450641983">
<node TEXT="any prescribed combination (i.e. constructor/class) of initialization parameters that makes sense" ID="ID_588573729" CREATED="1642450760783" MODIFIED="1642450800993"/>
</node>
<node TEXT="various approaches to error handling" ID="ID_932103582" CREATED="1642450646434" MODIFIED="1642450653480">
<node TEXT="panic/runtime-exception" ID="ID_1088004616" CREATED="1642450654578" MODIFIED="1642450699386"/>
<node TEXT="error-return/checked-exception" ID="ID_940826498" CREATED="1642450664524" MODIFIED="1642450701610"/>
<node TEXT="best-effort result (only)" ID="ID_1180629287" CREATED="1642450703508" MODIFIED="1642450713172"/>
<node TEXT="(...)" ID="ID_42813942" CREATED="1642450746263" MODIFIED="1642450754621"/>
</node>
<node TEXT="safety of input" ID="ID_1519654836" CREATED="1642451311986" MODIFIED="1642451317102">
<node TEXT="unsafe (untrusted/unverified source)" ID="ID_1249180663" CREATED="1642451317506" MODIFIED="1642451443649">
<node TEXT="gracefully handle errors" ID="ID_1730545424" CREATED="1642451418938" MODIFIED="1642451421738">
<node TEXT="prevent abusing error handling (intentionally introducing errors)" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1276620677" CREATED="1642451478366" MODIFIED="1642451511151"/>
</node>
<node TEXT="optional: give feedback on nature of error" ID="ID_1715039356" CREATED="1642451421968" MODIFIED="1642451438135">
<node TEXT="you might want to indicate protocol violations on other participants side" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_887970932" CREATED="1642451513089" MODIFIED="1642451537828"/>
</node>
</node>
<node TEXT="safe (own application logic)" ID="ID_1650649205" CREATED="1642451331592" MODIFIED="1642451460248">
<node TEXT="cannot (should not) go wrong?" ID="ID_1372725604" CREATED="1642451575065" MODIFIED="1642451580871"/>
<node TEXT="fail on deviation to uncover errors early" ID="ID_1865480680" CREATED="1642451448484" MODIFIED="1642451548106">
<node TEXT="this could result in DoS but in any case within your control to fix things for the better" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1722904548" CREATED="1642451381810" MODIFIED="1642451405471"/>
</node>
</node>
</node>
<node TEXT="use-cases/variations" ID="ID_420871838" CREATED="1642452671971" MODIFIED="1642452679933">
<node TEXT="Little-endian conversion for big-endian-based utilities/values" ID="ID_1128359935" CREATED="1642452681086" MODIFIED="1642452702870"/>
<node TEXT="mitigate malicious errors" ID="ID_1982624680" CREATED="1642453453605" MODIFIED="1642453461719">
<node TEXT="carefully crafted input at endpoint" ID="ID_250866775" CREATED="1642453476774" MODIFIED="1642453499106"/>
<node TEXT="injected/changed data in transit" ID="ID_856391613" CREATED="1642453471613" MODIFIED="1642453503118"/>
</node>
<node TEXT="mitigate accidental errors" ID="ID_979222996" CREATED="1642453461965" MODIFIED="1642453470939"/>
</node>
</node>
<node TEXT="sources" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1535407896" CREATED="1640131196654" MODIFIED="1640131211475">
<node TEXT=" bytes, strings: add Cut #46336 (GO)" ID="ID_1893871422" CREATED="1640131229124" MODIFIED="1640131329483" LINK="https://github.com/golang/go/issues/46336#issue-899222755">
<node TEXT="Elaborates on the unreasonably effective/pervasive use of the newly defined &apos;Cut&apos; function, basically just by identifying and implementing a usage sequence that is often encountered in multiple variants for multiple representations." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1245553915" CREATED="1640131246382" MODIFIED="1640131307912"/>
</node>
</node>
<node TEXT="idea is that we are considerate to how we divide utilities, such that they are easily composable, easily findable (i.e. one clear target therefore one clear location), etc." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_986531353" CREATED="1642448991911" MODIFIED="1642449041591"/>
</node>
<node TEXT="refactoring/improving" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="right" ID="ID_436014802" CREATED="1638665671942" MODIFIED="1638720583514">
<edge COLOR="#ff00ff"/>
<node TEXT="There are plenty of improvements possible in existing code bases. However, the area for improvement should be chosen for immediate benefit." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1260199558" CREATED="1638722129888" MODIFIED="1638722202380"/>
<node TEXT="search for sequences of usage logic" LOCALIZED_STYLE_REF="default" ID="ID_1300939458" CREATED="1638665684230" MODIFIED="1638666983433">
<node TEXT="This is the default &quot;extract method&quot; operation. This is not the distinguishing factor. The distinction is in ensuring minimal implementation, such that maximum benefit of reuse *can be* from utilities." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1181055606" CREATED="1638723940700" MODIFIED="1638724067230"/>
<node TEXT="identify and look up or create one or more utility functions" LOCALIZED_STYLE_REF="default" ID="ID_52758109" CREATED="1638665706031" MODIFIED="1638666983435"/>
</node>
</node>
<node TEXT="definitions" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="left" ID="ID_147277614" CREATED="1639093126936" MODIFIED="1639093134172">
<edge COLOR="#007c7c"/>
<node TEXT="simplicity" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1197881344" CREATED="1638664674321" MODIFIED="1639093158398">
<node TEXT="reduced (expansion)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_603669311" CREATED="1638664995375" MODIFIED="1639687790183">
<node TEXT="Minimal number of &quot;moving parts&quot;, be it active elements or prescribed rules and conditions that are preconditions" LOCALIZED_STYLE_REF="default" ID="ID_515555683" CREATED="1638665149643" MODIFIED="1638666983117"/>
</node>
<node TEXT="specialized (generalization)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_460841109" CREATED="1638665001834" MODIFIED="1639687794177">
<node TEXT="implementation targeted at the one specific question/problem statement, as opposed to generalizing for a whole class of similar problems" LOCALIZED_STYLE_REF="default" ID="ID_312471772" CREATED="1638665107310" MODIFIED="1638666983146"/>
</node>
<node TEXT="unoptimized (optimization)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1723200762" CREATED="1638665004611" MODIFIED="1639687797286">
<node TEXT="implementation/semantics on same level of abstraction as the question/input/problem statement" LOCALIZED_STYLE_REF="default" ID="ID_1729778904" CREATED="1638665010371" MODIFIED="1638666983164"/>
<node TEXT="no platform-specific shortcuts" LOCALIZED_STYLE_REF="default" ID="ID_1409072442" CREATED="1638665048826" MODIFIED="1638666983176"/>
</node>
<node TEXT="Follow-up" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1171841686" CREATED="1640132480210" MODIFIED="1640132486119">
<node TEXT="need to verify/validate definition with other people/other fields" ID="ID_273615438" CREATED="1640132490026" MODIFIED="1640132503288">
<node TEXT="candidates?" ID="ID_793157990" CREATED="1640132507002" MODIFIED="1640132530465">
<node TEXT="John D. Cook" ID="ID_231961366" CREATED="1640132511098" MODIFIED="1640132515724"/>
<node TEXT="Rob Pike" ID="ID_1423972605" CREATED="1640132509514" MODIFIED="1640132510959"/>
<node TEXT="Alexander Serebrenik" ID="ID_1958489707" CREATED="1640132519514" MODIFIED="1640132524089">
<node TEXT="Am I correct to think that his focus is on Software Engineering aspects of CS?" ID="ID_1205883724" CREATED="1640132560264" MODIFIED="1640132577607">
<icon BUILTIN="help"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="concerns/investigation" LOCALIZED_STYLE_REF="styles.topic" ID="ID_337113021" CREATED="1639694685361" MODIFIED="1639694761845">
<node TEXT="Complete?" ID="ID_1384753251" CREATED="1639694703697" MODIFIED="1639694707856"/>
<node TEXT="Overlap?" ID="ID_320800943" CREATED="1639694709017" MODIFIED="1639694712203"/>
<node TEXT="General or unintentionally biased towards computer science?" ID="ID_1821789047" CREATED="1639694717657" MODIFIED="1639694736735"/>
<node TEXT="are there always 2 perspectives concerned: implementation/usage or internal/external or ..." ID="ID_1830727687" CREATED="1639694768638" MODIFIED="1639694797202">
<node TEXT="distinction between implementation and use" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1892875278" CREATED="1639694798497" MODIFIED="1639694862084"/>
</node>
<node TEXT="simplicity = perfection?" ID="ID_1490200614" CREATED="1640809872008" MODIFIED="1640809879370">
<node TEXT="Quote about perfection: &quot;... when there is nothing left to take away&quot;. Corresponds with definition and approach for simplicity/complexity-duality." ID="ID_1652621625" CREATED="1640809880235" MODIFIED="1640809918407"/>
</node>
</node>
<node TEXT="consequences" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1288701661" CREATED="1640129276499" MODIFIED="1640129278449">
<node TEXT="all variants of complexity metrics classifiable in any of the three dimensions" ID="ID_863678283" CREATED="1640129282290" MODIFIED="1640129390096">
<icon BUILTIN="help"/>
</node>
<node TEXT="complexity metrics separable into their (orthogonal?) components based on complexity dimensions" ID="ID_1121780709" CREATED="1640129348002" MODIFIED="1640129384226">
<icon BUILTIN="help"/>
</node>
<node TEXT="concrete, but limited, evaluation of simplicity/complexity possible" ID="ID_303776319" CREATED="1640129423401" MODIFIED="1640129448701">
<icon BUILTIN="help"/>
</node>
<node TEXT="use concretes (dimensions, mapping of properties to these theoretical dimensions) as a way to make things actionable" ID="ID_1995340303" CREATED="1640129492353" MODIFIED="1640129918261">
<icon BUILTIN="help"/>
</node>
<node TEXT="given that simplicity is the lower-bound, use it as a signaling mechanism" ID="ID_1953778939" CREATED="1640130304857" MODIFIED="1640130338904">
<icon BUILTIN="help"/>
</node>
<node TEXT="one can define many (&quot;more refined&quot;) metrics that are positioned similarly in the three-dimensional classification of complexity, but that look at different specifics" ID="ID_482647355" CREATED="1640130368864" MODIFIED="1640130999971">
<icon BUILTIN="help"/>
<node TEXT="how do these metrics differ?" ID="ID_730803576" CREATED="1640130437167" MODIFIED="1640130509268">
<icon BUILTIN="help"/>
</node>
<node TEXT="what do these metrics mean?" ID="ID_1390408393" CREATED="1640130468943" MODIFIED="1640130522117">
<icon BUILTIN="help"/>
</node>
<node TEXT="are these specific differences within the same complexity-concerns mean that different aspects/properties are highlighted?" ID="ID_481385465" CREATED="1640130474599" MODIFIED="1640130515079">
<icon BUILTIN="help"/>
</node>
<node TEXT="these different aspects is what makes the metric domain-specific" ID="ID_534138050" CREATED="1640130581878" MODIFIED="1640130603264">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="make the three dimensions for a complete definition of simplicity/complexity?" ID="ID_1029362980" CREATED="1640131001710" MODIFIED="1640131029354">
<icon BUILTIN="help"/>
</node>
<node TEXT="can this (very general) definition of simplicity/complexity help to prove the completeness/soundness/overlap/missing parts in other metrics?" ID="ID_1627982944" CREATED="1640131032810" MODIFIED="1640131067678">
<icon BUILTIN="help"/>
</node>
<node TEXT="Take more time to describe the actual distinctions between the (orthogonal) dimensions. E.g. &apos;optimization&apos; dimension is unique in that it concerns itself with implementation such that it cannot be identified based on syntax alone. Semantic meaning is necessary.)" ID="ID_1086356643" CREATED="1640131599700" MODIFIED="1640131666566">
<icon BUILTIN="help"/>
</node>
</node>
</node>
<node TEXT="Object Oriented Programming (OOP)" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_63763402" CREATED="1638664692528" MODIFIED="1639095086049">
<node TEXT="definition" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1122075732" CREATED="1638665235127" MODIFIED="1638721901330">
<node TEXT="There are 2 mutually exclusive (non-overlapping) definitions that cover different concerns." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_464973724" CREATED="1638721824816" MODIFIED="1641863807660"/>
<node TEXT="model as objects, i.e. &quot;living cells&quot; referring to concurrent parts" LOCALIZED_STYLE_REF="default" ID="ID_1106482383" CREATED="1638665244008" MODIFIED="1638721593740">
<icon BUILTIN="full-1"/>
<node TEXT="primary concern: control flow/interacting systems/co-routines" LOCALIZED_STYLE_REF="styles.important" ID="ID_1084446509" CREATED="1643591348536" MODIFIED="1643593313405"/>
<node TEXT="concurrency (concurrent design)" ID="ID_1371823261" CREATED="1641863661144" MODIFIED="1643593701826">
<node TEXT="coroutines" ID="ID_1267057499" CREATED="1643593622800" MODIFIED="1643593625339"/>
<node TEXT="generators" ID="ID_306956306" CREATED="1643593625648" MODIFIED="1643593626968"/>
<node TEXT="go-routines" ID="ID_1839100037" CREATED="1643593627240" MODIFIED="1643593629099"/>
<node TEXT="fibers" ID="ID_444491232" CREATED="1643593631840" MODIFIED="1643593632896"/>
<node TEXT="green threads" ID="ID_1342785674" CREATED="1643593633256" MODIFIED="1643593635357"/>
<node TEXT="userthreads" ID="ID_1603014415" CREATED="1643593635672" MODIFIED="1643593637184"/>
<node TEXT="userspace threads" ID="ID_581003694" CREATED="1643593640963" MODIFIED="1643593643109"/>
</node>
<node TEXT="process composition" ID="ID_1782615627" CREATED="1641863774600" MODIFIED="1641863777494"/>
<node TEXT="(in)determinism" ID="ID_1515134093" CREATED="1641863681088" MODIFIED="1641863685472"/>
<node TEXT="control flow" ID="ID_1377155312" CREATED="1641863669496" MODIFIED="1641863674515"/>
<node TEXT="parallel execution" ID="ID_900164023" CREATED="1641863693032" MODIFIED="1641863697604"/>
<node TEXT="state machines/petrinets" ID="ID_1028157724" CREATED="1641863746298" MODIFIED="1641863753926"/>
<node TEXT="multiple control flows-scope" ID="ID_922930117" CREATED="1641863877513" MODIFIED="1641863919645"/>
<node TEXT="illustrate by example" ID="ID_1470552907" CREATED="1641864090185" MODIFIED="1641864093717">
<node TEXT="multiple traffic lights, software automatically adjusting to states (through communication channels) ... the classic example of process composition" ID="ID_1040747647" CREATED="1641864094641" MODIFIED="1641864127817"/>
</node>
<node TEXT="Realize that &quot;concurrent systems&quot; is the unoptimized situation, while typical single-thread (single control flow) procedural programming is *(premature) optimization* from design perspective, for most cases. (???)" LOCALIZED_STYLE_REF="styles.important" ID="ID_1671658185" CREATED="1644064083968" MODIFIED="1644073347510"/>
</node>
<node TEXT="object as struct with private part used to encapsulate management of single concept in isolation (invariants, state, logic)" LOCALIZED_STYLE_REF="default" FOLDED="true" ID="ID_1301705882" CREATED="1638665307679" MODIFIED="1638721595868">
<icon BUILTIN="full-2"/>
<node TEXT="primary concern: soundness/concistency/invariants" LOCALIZED_STYLE_REF="styles.important" ID="ID_1791835027" CREATED="1643591376927" MODIFIED="1643591425172"/>
<node TEXT="data management" ID="ID_1824400146" CREATED="1641863705847" MODIFIED="1641863710054"/>
<node TEXT="encapsulation" ID="ID_1968486455" CREATED="1641863710392" MODIFIED="1641863712320"/>
<node TEXT="invariants" ID="ID_344384166" CREATED="1641863712648" MODIFIED="1641863718158"/>
<node TEXT="protection from inconsistency/corruption" ID="ID_244326764" CREATED="1641863718472" MODIFIED="1641863728162"/>
<node TEXT="localized knowledge" ID="ID_1893218663" CREATED="1641863766221" MODIFIED="1641863769575"/>
<node TEXT="abstracting away consistency rules from user" ID="ID_1015770807" CREATED="1641863842024" MODIFIED="1641863852725"/>
<node TEXT="single control-flow, localized scope" ID="ID_1602726274" CREATED="1641863922176" MODIFIED="1641863933238"/>
<node TEXT="illustrate by example" ID="ID_103319443" CREATED="1641864132817" MODIFIED="1641864138860">
<node TEXT="preservation of (correct) state consistency through invariants realized in method logic. Emphasize only minimal logic necessary to ensure appropriate data after each finished call of a public method." ID="ID_1064940217" CREATED="1641864139281" MODIFIED="1641864190881"/>
</node>
</node>
<node TEXT="any value-type/reference-type &quot;as an object&quot;, meaning that you can attach methods; use it in any way you would an object." LOCALIZED_STYLE_REF="default" FOLDED="true" ID="ID_1049547568" CREATED="1643333529383" MODIFIED="1643419000109">
<icon BUILTIN="full-3"/>
<node TEXT="primary concern: correctness/type-system" LOCALIZED_STYLE_REF="styles.important" ID="ID_1673596926" CREATED="1643591428278" MODIFIED="1643591465658"/>
<node TEXT="primitives/value-types are often treated/manipulated by their &quot;raw&quot; memory-area, in which the value is stored and there is no associated memory-address/pointer that needs to be dereferenced." ID="ID_248242" CREATED="1643418928365" MODIFIED="1643418984812"/>
<node TEXT="you can deifne methods on any type, as you would for objects (hence &quot;everything is an object&quot;)" ID="ID_1182625545" CREATED="1643418774291" MODIFIED="1643418804453"/>
<node TEXT="you can generalize over both objects and primitives/&quot;value-types&quot;, so if you want to store something in a map/dictionary, you can be oblivious about *anything* of the entry&apos;s value except its existence. Anything from an object to an integer to a boolean is reasonable to put in there." ID="ID_1424531418" CREATED="1643418807377" MODIFIED="1643418919793"/>
<node TEXT="&quot;everything is an object&quot; does not imply basic functions no longer exist, instead it implies that these functions are also objects." ID="ID_110318880" CREATED="1643593343587" MODIFIED="1643593375591"/>
</node>
<node TEXT="Can we map these three definitions to different aspects of &quot;simplicity&quot;?" LOCALIZED_STYLE_REF="styles.important" ID="ID_865196966" CREATED="1643593394909" MODIFIED="1643593413862">
<node TEXT="invariants/consistency as expansion-dimension" ID="ID_25400937" CREATED="1643593418789" MODIFIED="1643593507769"/>
<node TEXT="correctness/type-system as generalization-dimension?" ID="ID_552649035" CREATED="1643593447437" MODIFIED="1643593510663"/>
<node TEXT="systems/coroutines as &quot;optimization&quot;-dimension?" ID="ID_1378215712" CREATED="1643593466278" MODIFIED="1643593515898">
<node TEXT="Is it reasonable to say that this is a different (higher) level of abstraction?" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1434148646" CREATED="1643593516438" MODIFIED="1643593526306"/>
<node TEXT="I feel like this is pushing &quot;definition&quot; into the &quot;I want this to be true so I&apos;ll stretch as far as possible&quot;-category. Probably just wishful thinking." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1285011414" CREATED="1643593535030" MODIFIED="1643593576329"/>
</node>
</node>
</node>
<node TEXT="properties" LOCALIZED_STYLE_REF="styles.topic" ID="ID_375706796" CREATED="1638721467008" MODIFIED="1638721901343">
<node TEXT="encapsulation" ID="ID_1769855950" CREATED="1638721011700" MODIFIED="1638721862797"/>
<node TEXT="polymorphism" ID="ID_1735234451" CREATED="1638721014146" MODIFIED="1638721865292"/>
<node TEXT="inheritance" ID="ID_951897452" CREATED="1638721062536" MODIFIED="1638721866899"/>
</node>
<node TEXT="access" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1858927845" CREATED="1639093103588" MODIFIED="1639597776377">
<node TEXT="privileged" LOCALIZED_STYLE_REF="AutomaticLayout.level,4" ID="ID_1230054237" CREATED="1639093329799" MODIFIED="1639688096818">
<node TEXT="access to internals (i.e. unrestricted) that encapsulate, to be hidden away from common usage logic." ID="ID_103726953" CREATED="1639093189021" MODIFIED="1639093349166"/>
</node>
<node TEXT="usage" LOCALIZED_STYLE_REF="AutomaticLayout.level,4" ID="ID_859914639" CREATED="1639093350875" MODIFIED="1639597763689">
<node TEXT="the common logic of the application that uses a class. It does not require any knowledge of internals or its invariants, therefore is not allowed access to its internals and operations are controlled/managed by privileged logic (methods)." ID="ID_1094438233" CREATED="1639093222812" MODIFIED="1639093364458"/>
</node>
</node>
</node>
<node TEXT="Minimal-OO" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1855491034" CREATED="1638664679744" MODIFIED="1640128827441">
<node TEXT="Strictly following minimal-OO may lead to extra effort to start out with, or be interfered with by design of dependent libraries.&#xa;Most benefit if followed for domain types. 1.) will be (re)used most. 2.) utilities allow for fast/efficient use of self-constructed domain-universe." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_110725822" CREATED="1638724111859" MODIFIED="1638724246854"/>
<node TEXT="properties" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1065925071" CREATED="1638665375408" MODIFIED="1638722426204">
<node TEXT="simple" LOCALIZED_STYLE_REF="default" ID="ID_670683828" CREATED="1638665430800" MODIFIED="1638666983424">
<node TEXT="implementation (internals)" LOCALIZED_STYLE_REF="default" ID="ID_201877714" CREATED="1638665378843" MODIFIED="1638666983426">
<node TEXT="fields in state are inseparable, i.e. no partitioning is possible without losing ability to preserve invariants" ID="ID_966549515" CREATED="1639697114570" MODIFIED="1639697153799"/>
<node TEXT="method body contains logic necessary to make directed state modifications without leaving invariants violated" ID="ID_174806821" CREATED="1639696549267" MODIFIED="1639696590060"/>
<node TEXT="method body contains logic accessing privileged content or is reduced high-expressive logic (e.g. utility calls)" ID="ID_1828714863" CREATED="1639696671969" MODIFIED="1639696793763"/>
<node TEXT="method body contains logic for managing single unit (single concern)" ID="ID_599817390" CREATED="1639696866249" MODIFIED="1639697113468"/>
</node>
<node TEXT="interface (method signature)" LOCALIZED_STYLE_REF="default" ID="ID_1344287041" CREATED="1638665402463" MODIFIED="1638666983428"/>
</node>
<node TEXT="minimal object" ID="ID_1479714986" CREATED="1638723663781" MODIFIED="1638723758356">
<node TEXT="maximum room for identifying utilities based on usage logic" ID="ID_1655014560" CREATED="1638723760324" MODIFIED="1638723769369"/>
</node>
</node>
<node TEXT="references" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1167485668" CREATED="1640705413120" MODIFIED="1640705414495">
<node TEXT="Quote by John Carmack: &quot;... sometimes a function is the right solution.&quot;" ID="ID_854573602" CREATED="1640705379066" MODIFIED="1640705400288"/>
</node>
</node>
<node TEXT="simplicity = perfection?" ID="ID_1765092857" CREATED="1640705328699" MODIFIED="1640705334741">
<node TEXT="... outdated ... probably missing lots of small details/thoughts" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_924296523" CREATED="1641864348713" MODIFIED="1641864363781"/>
<node TEXT="quotes regarding perfection: &quot;... when there is nothing left to take away&quot; ==&gt; implies simplicity according to this definition." ID="ID_485516395" CREATED="1640705344122" MODIFIED="1640705376574"/>
<node TEXT="simplicity is perfection when the goal of perfection is not otherwise defined, i.e. no special priority to high/full optimization." ID="ID_149442784" CREATED="1641864205449" MODIFIED="1641864238621">
<icon BUILTIN="help"/>
</node>
<node TEXT="if no deviating goals, `simplicity = perfection` explains why it is so hard to create simple programs, because chasing simplicity means chasing perfection." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_729212735" CREATED="1641864249321" MODIFIED="1641864372418"/>
<node TEXT="(it also explains why arguing for simplicity is hard, because oftentimes arguing for simplicity also means arguing for perfection, making it a very difficult feat to accomplish especially without a concrete definition." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1443589075" CREATED="1641864298772" MODIFIED="1641864372476"/>
</node>
</node>
<node TEXT="guidelines" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="left" ID="ID_1618505922" CREATED="1638719069762" MODIFIED="1638719429288">
<edge COLOR="#7c007c"/>
<node TEXT="There is going to be overlap, there are going to be missing/unclear definitions. Need to be clarified/corrected." LOCALIZED_STYLE_REF="styles.important" ID="ID_450831048" CREATED="1638719103783" MODIFIED="1638719230393"/>
<node TEXT="Design Patterns" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1783586886" CREATED="1638721645088" MODIFIED="1638721652466">
<node TEXT="properties" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1132172145" CREATED="1638721660707" MODIFIED="1638722602395">
<node TEXT="match properties with simplicity/minimal-OO" LOCALIZED_STYLE_REF="styles.important" ID="ID_508857881" CREATED="1638721676200" MODIFIED="1638721688558"/>
</node>
</node>
<node TEXT="SOLID" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_306159666" CREATED="1638719083304" MODIFIED="1638720726330">
<node TEXT="single-reponsibility" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_876660490" CREATED="1638719503413" MODIFIED="1644443911273">
<icon BUILTIN="button_ok"/>
<node TEXT="&quot;There should never be more than one reason for a class to change.&quot;" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1910912766" CREATED="1638720911795" MODIFIED="1638720915344"/>
</node>
<node TEXT="open/closed" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1341910780" CREATED="1638719448126" MODIFIED="1638722624533">
<node TEXT="&quot;Software entities ... should be open for extension, but closed for modification.&quot;" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1473972568" CREATED="1638720892089" MODIFIED="1638720896335"/>
</node>
<node TEXT="liskov-substitution" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_393897330" CREATED="1638719452190" MODIFIED="1638722624540">
<node TEXT="&quot;Functions that use pointers or references to base classes must be able to use objects of derived classes without knowing it.&quot;" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1229906574" CREATED="1638720857950" MODIFIED="1638720863689"/>
</node>
<node TEXT="interface segregation" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1552799187" CREATED="1638719473389" MODIFIED="1638722624547">
<node TEXT="&quot;Many client-specific interfaces are better than one general-purpose interface.&quot;" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_524531610" CREATED="1638720875075" MODIFIED="1638720879463"/>
</node>
<node TEXT="dependency inversion" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_95925498" CREATED="1638719495413" MODIFIED="1644443907327">
<icon BUILTIN="button_ok"/>
<node TEXT="&quot;Depend on abstractions, not concretions&quot;" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1429926906" CREATED="1638719551237" MODIFIED="1638720749460"/>
</node>
</node>
<node TEXT="GRASP" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1347780135" CREATED="1638719089368" MODIFIED="1644442961982">
<node TEXT="high cohesion" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_507221885" CREATED="1638719885246" MODIFIED="1644443885240">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="low coupling" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_853723762" CREATED="1638719888628" MODIFIED="1644443897299">
<icon BUILTIN="hourglass"/>
</node>
<node TEXT="polymorphism" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_854981893" CREATED="1638721990753" MODIFIED="1638722624572"/>
<node TEXT="..." LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1457891304" CREATED="1638719891164" MODIFIED="1644442961980"/>
</node>
<node TEXT="DRY" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_985412457" CREATED="1638719090824" MODIFIED="1644442161746">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="YAGNI" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1383245710" CREATED="1638719091976" MODIFIED="1644442163778">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="TDD" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1835115625" CREATED="1638719432342" MODIFIED="1638720743778"/>
<<<<<<< HEAD
<node TEXT="KISS" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1660517794" CREATED="1638722705288" MODIFIED="1638722710359"/>
<node TEXT="RAII" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_992494297" CREATED="1644502226536" MODIFIED="1644502227644">
<node TEXT="Resource Acquisition Is Initialization" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_600054390" CREATED="1644502258216" MODIFIED="1644502269237"/>
<node TEXT="Inject dependency at construction-time of instance if intending to use this resource during its lifetime." ID="ID_1153076287" CREATED="1644503974397" MODIFIED="1644504117806"/>
<node TEXT="The interface type indicates what is expected." ID="ID_1824871487" CREATED="1644504132788" MODIFIED="1644504133934"/>
<node TEXT="These injects can often be assigned to immutable/final fields as they are only assigned once at time of instantiation." ID="ID_1526221512" CREATED="1644504119389" MODIFIED="1644504130591"/>
</node>
<node TEXT="code reuse/reusability" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1752811617" CREATED="1638722837224" MODIFIED="1638723017252">
=======
<node TEXT="KISS" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1660517794" CREATED="1638722705288" MODIFIED="1644442168345">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="code reuse/reusability" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1752811617" CREATED="1638722837224" MODIFIED="1644442170378">
<icon BUILTIN="button_ok"/>
>>>>>>> 330177b (left-over notes)
<node TEXT="requirements" LOCALIZED_STYLE_REF="styles.topic" ID="ID_146604199" CREATED="1638723020044" MODIFIED="1639349641826">
<node TEXT="locatable" ID="ID_255308737" CREATED="1638723025344" MODIFIED="1638723031207"/>
<node TEXT="accessible" ID="ID_448446905" CREATED="1638723031447" MODIFIED="1638723043369"/>
<node TEXT="low threshold to adoption" ID="ID_1334416293" CREATED="1638723043543" MODIFIED="1638723046321">
<node TEXT="preconditions" ID="ID_1719131962" CREATED="1639349664245" MODIFIED="1639349673717"/>
</node>
</node>
</node>
<node TEXT="Types reflect intention" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1122096833" CREATED="1644502320273" MODIFIED="1644502324914">
<node TEXT="if parameter asks for a Closeable, the item will eventually be closed. Otherwise the Closeable type is incorrect." ID="ID_1981272127" CREATED="1644502327336" MODIFIED="1644502371521"/>
<node TEXT="This is about communication of intention as much as it is about minimalism of requirement." ID="ID_1748023350" CREATED="1644502372169" MODIFIED="1644502373495"/>
</node>
</node>
<node TEXT="mapping" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="right" ID="ID_147728565" CREATED="1638724816376" MODIFIED="1638738115097">
<edge COLOR="#7c0000"/>
<node ID="ID_15380090" CREATED="1638725103463" MODIFIED="1638736346510"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>simplicity</b>: as defined
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1288612509" CREATED="1638724986072" MODIFIED="1639693624400"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>premature optimization</b>&nbsp;== simplicity (invalidation of)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1303521723" CREATED="1638725021456" MODIFIED="1639693627961"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>premature expansion</b>&nbsp;== simplicity (invalidation of)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_440834134" CREATED="1638725026872" MODIFIED="1639693632598"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>premature generalization</b>&nbsp;== simplicity (invalidation of)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_212980323" CREATED="1638725048527" MODIFIED="1638737215022"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Minimal-OO</b>&nbsp;== rules of simplicity + design-scoped
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1781182661" CREATED="1638724831932" MODIFIED="1638727313035"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>SRP</b>&nbsp;== inseparable state + minimal logic (minimal-OO)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_849950601" CREATED="1638736240463" MODIFIED="1638736283536"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Tell don't ask</b>: not an issue, as minimal necessary logic ensures that there is no consideration necessary, i.e. already 'tell', no room for 'ask'.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1381333884" CREATED="1638724895792" MODIFIED="1638727310714"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>high cohesion</b>&nbsp;== inseparable state (minimal-OO) + minimal necessary logic(?)
    </p>
  </body>
</html>
</richcontent>
<node TEXT="The idea is that if fields are &quot;provably&quot; inseparable because any changes would result in impossible to maintain class invariants, then they basically form a single coherent unit." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1990155674" CREATED="1638738537494" MODIFIED="1638738591949"/>
</node>
<node TEXT="low coupling" LOCALIZED_STYLE_REF="styles.important" ID="ID_830635902" CREATED="1638724918648" MODIFIED="1638724930375"/>
<node ID="ID_299340035" CREATED="1638727168134" MODIFIED="1639693574494"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>open/closed</b>: minimal-OO ensures implementation is aimed at single concern (SRP). Will not need changes as long as the theoretical concept does not change. If the concept evolves a new implementation can be created.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1961295618" CREATED="1638736595702" MODIFIED="1638736670452"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>DRY</b>: easy to identify for single-concern implementations, domain types need to be easy to find/identify, utilities should have predictable locations.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_867450892" CREATED="1638727274446" MODIFIED="1638737173160"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>YAGNI</b>: design for something other than present time + actual+verified+confirmed requirements. (E.g. anticipate future requirements or perceived need.)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="goals" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="right" ID="ID_1873486569" CREATED="1638725217840" MODIFIED="1638725222771">
<edge COLOR="#00007c"/>
<node TEXT="definition of simplicity" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1224596900" CREATED="1638725958394" MODIFIED="1638739625651">
<node TEXT="universally applicable, or accidentally biased towards OO concerns?" ID="ID_424238112" CREATED="1638737229133" MODIFIED="1638737309153">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="clarify use of minimal-OO" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_963201046" CREATED="1638725976544" MODIFIED="1638739629003">
<node TEXT=" to satisfy simplicity" ID="ID_599728374" CREATED="1638726009815" MODIFIED="1638726016342">
<node TEXT="state benefits of satisfying simplicity" LOCALIZED_STYLE_REF="styles.important" ID="ID_1340862440" CREATED="1639091974486" MODIFIED="1639091990515"/>
</node>
<node TEXT="development benefits" ID="ID_1065588702" CREATED="1638726016631" MODIFIED="1638726017326"/>
</node>
<node TEXT="guidelines for minimal-OO" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1833175927" CREATED="1638725225152" MODIFIED="1638739631694">
<node TEXT="applying minimal-OO from scratch" ID="ID_1459187336" CREATED="1639092073069" MODIFIED="1639092083942"/>
<node TEXT="modifying an existing codebase towards minimal-OO-satisfying" ID="ID_941204587" CREATED="1639092084223" MODIFIED="1639092111238"/>
</node>
<node TEXT="utility/automation to identify improvements towards Minimal-OO" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_446316331" CREATED="1638737424561" MODIFIED="1638739635013"/>
<node TEXT="guidelines for identification/separation of utilities" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1029299890" CREATED="1638725232727" MODIFIED="1638739637639">
<node TEXT="use cases" ID="ID_1037612883" CREATED="1638726082655" MODIFIED="1638726085350"/>
<node TEXT="distinguishing factors" ID="ID_1226722558" CREATED="1638726160655" MODIFIED="1638726166352">
<node TEXT="single-type focus" ID="ID_1030045053" CREATED="1638726169047" MODIFIED="1638726172399"/>
<node TEXT="dual-type focus" ID="ID_1615261157" CREATED="1638726172553" MODIFIED="1638726176575"/>
</node>
<node TEXT="implementation classes" ID="ID_710588694" CREATED="1638726180335" MODIFIED="1638726196298">
<node TEXT="general/common" ID="ID_1673953373" CREATED="1638726199071" MODIFIED="1638726201306"/>
<node TEXT="subtle (constant-time)" ID="ID_798286461" CREATED="1638726201399" MODIFIED="1638726206568"/>
<node TEXT="platform-specific (optimized)" ID="ID_1546203084" CREATED="1638726209623" MODIFIED="1638726221931"/>
</node>
</node>
<node TEXT="tooling to identify simplifiable classes (e.g. pmd rules)" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1596100850" CREATED="1638725250721" MODIFIED="1638739640549">
<node TEXT="identify usage logic, extract utilities" ID="ID_19336842" CREATED="1638725421015" MODIFIED="1638725456713"/>
<node TEXT="set utility boundaries to identify multiple utilities" ID="ID_999238294" CREATED="1638725457118" MODIFIED="1638725460703"/>
</node>
<node TEXT="quantification" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1550725140" CREATED="1639686683247" MODIFIED="1639686694510">
<node TEXT="Given the defined three components and a concrete context, such as a programming language, the components can be defined concretely. This means that it becomes possible to set a number to simplicity/complexity as some (as of yet undefined) aggregate number based on quanitification of each individual component." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_328016149" CREATED="1639686698720" MODIFIED="1639688320554"/>
<node TEXT="examples" LOCALIZED_STYLE_REF="styles.topic" ID="ID_114030654" CREATED="1639686819687" MODIFIED="1639686825481">
<node TEXT="Java" LOCALIZED_STYLE_REF="styles.subtopic" ID="ID_1864370014" CREATED="1639686799094" MODIFIED="1639686837378">
<node TEXT="2 out of 3 components reasonably quantifiable." LOCALIZED_STYLE_REF="styles.important" ID="ID_1315080902" CREATED="1639687449340" MODIFIED="1639687499255"/>
<node TEXT="What to do with third component (optimization)?" ID="ID_259023264" CREATED="1639687501601" MODIFIED="1639687525153">
<icon BUILTIN="help"/>
</node>
<node TEXT="optimization" LOCALIZED_STYLE_REF="styles.subsubtopic" ID="ID_363159928" CREATED="1639686842971" MODIFIED="1639687419688">
<node TEXT="Limited possibilities. Can detect low-level language constructs, maybe special implementations, maybe use of primitive types." ID="ID_980240253" CREATED="1639686871145" MODIFIED="1639686927754"/>
<node TEXT="Limited possibilities. Cannot detect specially-chosen field composition for efficient implementations. For example, java.lang.BigInteger keeps separate field `signum` so detecting negative/neutral/positive value is O(1). This can be done in a simpler way which does not require the `signum` field, but may not be easy to discover through static analysis, especially without requirements." ID="ID_828615920" CREATED="1639686929936" MODIFIED="1639687097907"/>
</node>
<node TEXT="generalization" LOCALIZED_STYLE_REF="styles.subsubtopic" ID="ID_502960493" CREATED="1639686856009" MODIFIED="1639687424142">
<node TEXT="Number of parameters, and number of steps away from most-specialized implementation." ID="ID_985842443" CREATED="1639687111846" MODIFIED="1639687154861">
<node TEXT="can specialized type be determined? (in all cases)&#xa;in some cases derived from internally-used types" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_696345851" CREATED="1639687211127" MODIFIED="1639687591596"/>
</node>
</node>
<node TEXT="expansion" LOCALIZED_STYLE_REF="styles.subsubtopic" ID="ID_1010117830" CREATED="1639686851208" MODIFIED="1639687427080">
<node TEXT="Both existence of and correlated use are indicators, because if any of these are removed, things become simpler." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_287623608" CREATED="1639687303466" MODIFIED="1639687388412"/>
<node ID="ID_1873055038" CREATED="1639687231488" MODIFIED="1639687565177"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>entities</b>&nbsp;the parameters (and return type?) themselves
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_475689726" CREATED="1639687250535" MODIFIED="1639687403202"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>rules</b>&nbsp;conditionals for parameters, simultaneous use of two parameters (correlation)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Math" LOCALIZED_STYLE_REF="styles.subtopic" ID="ID_679533772" CREATED="1639688403632" MODIFIED="1639688404978">
<node TEXT="optimization" LOCALIZED_STYLE_REF="styles.subsubtopic" ID="ID_1276522480" CREATED="1639686842971" MODIFIED="1639687419688">
<node TEXT="FIXME: ..." ID="ID_961441424" CREATED="1639688626098" MODIFIED="1639688639166"/>
</node>
<node TEXT="generalization" LOCALIZED_STYLE_REF="styles.subsubtopic" ID="ID_291107491" CREATED="1639686856009" MODIFIED="1639687424142">
<node TEXT="use more general class than the class used in expressing the problem" ID="ID_583567459" CREATED="1639688496290" MODIFIED="1639688514457"/>
</node>
<node TEXT="expansion" LOCALIZED_STYLE_REF="styles.subsubtopic" ID="ID_1106143476" CREATED="1639686851208" MODIFIED="1639687427080">
<node TEXT="allow more variation through parameters, constants, etc." ID="ID_757029184" CREATED="1639688516971" MODIFIED="1639688548169"/>
</node>
</node>
</node>
</node>
<node TEXT="qualification" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_559496029" CREATED="1639686687998" MODIFIED="1639686694481">
<node TEXT="in a purely theoretical context, evaluating each of the three components can help to determine whether or not something is &quot;simple&quot; based on whether or not further simplification is possible. Although not immediately quantifiable, it does allow for insight into the current state." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1918255676" CREATED="1639692680867" MODIFIED="1639692770039"/>
</node>
</node>
<node TEXT="tooling" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="right" ID="ID_222504070" CREATED="1638739229049" MODIFIED="1638739233946">
<edge COLOR="#007c00"/>
<node TEXT="cohesion of class state (analysis)" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_197295287" CREATED="1638739238326" MODIFIED="1638739593339">
<node TEXT="conditions indicate relationship" ID="ID_1037762836" CREATED="1638739315504" MODIFIED="1638739341481"/>
<node TEXT="simultaneous mutation" ID="ID_1280116508" CREATED="1638739342040" MODIFIED="1638739356627"/>
<node TEXT="simultaneous reads (irrelevant by itself)" ID="ID_241355711" CREATED="1638739376903" MODIFIED="1638739398399">
<font STRIKETHROUGH="true"/>
</node>
<node TEXT="how to identify reads?" ID="ID_174144803" CREATED="1638739447293" MODIFIED="1638739457666">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="coupling" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1964186767" CREATED="1639693051429" MODIFIED="1639693056516">
<node TEXT="guaranteed minimal for minimal-OO logic?" ID="ID_66414621" CREATED="1639693058683" MODIFIED="1639693091103">
<icon BUILTIN="help"/>
</node>
<node TEXT="determining coupling should transparently process utilities?" ID="ID_336775787" CREATED="1639693096429" MODIFIED="1639693153382">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="minimalism of class logic (analysis)" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_222466228" CREATED="1638739273851" MODIFIED="1638739596881">
<node TEXT="heading (usage) logic" ID="ID_316148539" CREATED="1638739485049" MODIFIED="1639692790959"/>
<node TEXT="trailing (usage) logic" ID="ID_972678798" CREATED="1638739490798" MODIFIED="1639692793788"/>
<node TEXT="logic without receiving privileged calls/accesses" ID="ID_1684939489" CREATED="1638739495158" MODIFIED="1638739577247"/>
<node TEXT="sequences (in one or more lines) of usage calls/accesses" ID="ID_1481752065" CREATED="1638739548920" MODIFIED="1638739571797"/>
</node>
</node>
<node TEXT="misc/unrelated" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="right" ID="ID_910009551" CREATED="1642002688688" MODIFIED="1642002694119">
<edge COLOR="#007c00"/>
<node TEXT="tweaking" ID="ID_356489053" CREATED="1642002694813" MODIFIED="1642002698373">
<node TEXT="process of taking singular (or multiple if for same effort/cost) steps towards improving, either towards simplicity or a prioritized goal." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1085054369" CREATED="1642002698950" MODIFIED="1642002743241"/>
</node>
<node TEXT="rules" ID="ID_954638397" CREATED="1644076101564" MODIFIED="1644076102554">
<node TEXT="function parameters should &quot;not increase coupling degree&quot;? (not fully possible if you want to refer to common interface)" ID="ID_579569233" CREATED="1644076103436" MODIFIED="1644076137087">
<node TEXT="requires defining granularity of &quot;coupling degree&quot;" ID="ID_572786142" CREATED="1644076141243" MODIFIED="1644076156774"/>
</node>
<<<<<<< HEAD
</node>
</node>
=======
>>>>>>> 330177b (left-over notes)
<node TEXT="concerns" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="left" ID="ID_629431347" CREATED="1638722647936" MODIFIED="1638722825211">
<edge COLOR="#00ffff"/>
<node TEXT="theory of regions to identify minimal inseparable state (reliable? viable?)" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1977533439" CREATED="1638725338895" MODIFIED="1644504211670"/>
<node TEXT="fast/efficient development" ID="ID_1752141271" CREATED="1638724491114" MODIFIED="1638724494316">
<node TEXT="minimal-OO" ID="ID_491877883" CREATED="1638724496921" MODIFIED="1638724519308"/>
<node TEXT="maximum access to utilities" ID="ID_1456356721" CREATED="1638724519873" MODIFIED="1638724520524"/>
<node TEXT="minimal threshold to use of utilities  (reuse)" ID="ID_1594423348" CREATED="1638736377494" MODIFIED="1644505920046"/>
<node TEXT="go-to location for utilities" ID="ID_343557545" CREATED="1638724520738" MODIFIED="1638724532860"/>
<node TEXT="code reading as high-level, few statements, accomplish things quickly, straight-forward to read, ..." ID="ID_1775495231" CREATED="1638736385382" MODIFIED="1638736443487"/>
<node TEXT="clear boundaries to ensure composability" ID="ID_1071936393" CREATED="1638724533426" MODIFIED="1638724544991">
<node TEXT="no banana-monkey-jungle-problem" ID="ID_667770253" CREATED="1641864407681" MODIFIED="1641864414675"/>
</node>
<node TEXT="..." ID="ID_1354970789" CREATED="1638724550522" MODIFIED="1638724551132"/>
</node>
<node TEXT="single concern" FOLDED="true" ID="ID_472626979" CREATED="1641831917884" MODIFIED="1644505860003">
<icon BUILTIN="button_ok"/>
<node TEXT="here distinct from &quot;single responsibility&quot;" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_627771172" CREATED="1641832007038" MODIFIED="1641832026008"/>
<node TEXT="Concern determined by (inseparable) set of fields representing a single concept. If fields are separable, means there are more concepts and therefore class manages more than a single concept, hence has multiple concerns." ID="ID_1028493460" CREATED="1641832044213" MODIFIED="1641832122272"/>
<node TEXT="single concern is pretty much defined, i.e. minimal, inseparable/partitionable set of fields, i.e. representing a concept that isn&apos;t otherwise divisible." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_255890985" CREATED="1641864549449" MODIFIED="1641864596022"/>
</node>
<node TEXT="single responsibility" FOLDED="true" ID="ID_1897308023" CREATED="1641831913887" MODIFIED="1644505859061">
<icon BUILTIN="button_ok"/>
<node TEXT="here distinct from &quot;single concern&quot; which might be diluted due to low cohesion in class, i.e. many data representing more than a single concern." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1306838631" CREATED="1641831949644" MODIFIED="1641831997863"/>
<node TEXT="Responsibility is always single, a class is responsible for its invariants (state, corresponding logic) ONLY" ID="ID_288767543" CREATED="1641831921396" MODIFIED="1641832124772">
<node TEXT="also evident by existence of pattern for Dependency Injection" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1485767576" CREATED="1644074339928" MODIFIED="1644074515823"/>
<node TEXT="also implied by DI-principle stating that interfaces exist to decouple from other implementations" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_27643556" CREATED="1644074366199" MODIFIED="1644074515880"/>
<node TEXT="also evident from dynamic solutions, like CDI, which automatically provide &quot;context&quot; and &quot;dependency&quot; injection" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1444310728" CREATED="1644074411120" MODIFIED="1644074515905"/>
</node>
<node TEXT="(correctly) reasoning from class&apos; single responsibility" ID="ID_497463660" CREATED="1641860264789" MODIFIED="1641860287502">
<node TEXT="Interfaces are a way of saying that the class can do something if you provide a suitable object." ID="ID_191991719" CREATED="1641860288789" MODIFIED="1641860354150">
<node TEXT="provide a callback-mechanism to allow signaling back to the *unkonwn* caller that we&apos;ve reached a certain situation" ID="ID_1026340712" CREATED="1641860354876" MODIFIED="1641860363546"/>
<node TEXT="to demand something is done *for* the class in order for the class to fulfil its obligation. Often something like, we&apos;ve transitioned to a certain state and *then* something has to happen, but what needs to happen is outside class&apos; scope." ID="ID_1501231503" CREATED="1641860367676" MODIFIED="1641860409852"/>
<node TEXT="This connects Interface Segregation!" ID="ID_146637914" CREATED="1641860497699" MODIFIED="1641860512680">
<font BOLD="true"/>
</node>
<node TEXT="This depends on Liskov Substitution" ID="ID_1526374267" CREATED="1641860567891" MODIFIED="1641860579664">
<font BOLD="true"/>
</node>
<node TEXT="appropriate demarcation of scope of class -- define up to and including the interface and nothing more than that" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1479233451" CREATED="1641860590994" MODIFIED="1641860620165">
<font BOLD="false"/>
</node>
<node TEXT="this is only needed if the class&apos; function is useless without the (intermediate) callback. Typical pattern is having a state machine and something needs to happen as part of the state transition, in order for a successful (and useful) transition to occur." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1186571599" CREATED="1641860674322" MODIFIED="1641860727355"/>
</node>
<node TEXT="if responsibility (correctly) only goes inwards (maintaining internals), there cannot be a &quot;banana-and-the-monkey-and-the-rest-of-the-jungle&quot; case scenario." ID="ID_518271704" CREATED="1641860439139" MODIFIED="1641860486838">
<node TEXT="low coupling?" ID="ID_1779083974" CREATED="1641860528826" MODIFIED="1641860543180">
<icon BUILTIN="help"/>
<font BOLD="true"/>
</node>
</node>
</node>
</node>
<node TEXT="simplicity" ID="ID_1260552357" CREATED="1638722692592" MODIFIED="1642014235405">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="optimization" ID="ID_66545894" CREATED="1638722668928" MODIFIED="1641864471963">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="premature optimization" ID="ID_976956436" CREATED="1638722675160" MODIFIED="1641864478171">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="premature expansion" ID="ID_494864159" CREATED="1638722678840" MODIFIED="1641864482741">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="premature generalization" ID="ID_365802679" CREATED="1638722681776" MODIFIED="1641864487127">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="cohesion" ID="ID_615933137" CREATED="1638722664320" MODIFIED="1644504627081">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="coupling" ID="ID_927951284" CREATED="1638722667360" MODIFIED="1644504634252">
<icon BUILTIN="hourglass"/>
</node>
<node TEXT="indirection" ID="ID_510798126" CREATED="1638722690296" MODIFIED="1644504665020"/>
<node TEXT="abstraction" ID="ID_74787527" CREATED="1638722797864" MODIFIED="1638722799689"/>
<node TEXT="technical debt" ID="ID_408974440" CREATED="1638722658432" MODIFIED="1638722779442">
<node TEXT="define technical debt in terms of simplicity (or lack thereof) and its consequences" ID="ID_139342658" CREATED="1644505266249" MODIFIED="1644505293561"/>
<node ID="ID_927768604" CREATED="1638724272283" MODIFIED="1638724724670"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>implementation</b>: refactoring to fix, low effort
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_710751219" CREATED="1638724676057" MODIFIED="1638724782809"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>design</b>: logic/behavior not backed by design/requirement for <i>present</i>&nbsp;&nbsp;need
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="..., minimal-OO not followed (???), behavior not backed by design/requirement, not designed for *current* implementation, simplification possible, ..." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1282029127" CREATED="1638724282995" MODIFIED="1638724714205"/>
</node>
<node TEXT="design patterns" ID="ID_1721269166" CREATED="1638722661320" MODIFIED="1638722780592">
<node TEXT="solves a single specific design problem" ID="ID_1169325375" CREATED="1644505243050" MODIFIED="1644505252790"/>
<node TEXT="compliant with minimal-OO: yes(?)" ID="ID_285681729" CREATED="1641864633586" MODIFIED="1641864658870">
<icon BUILTIN="help"/>
</node>
<node TEXT="free of technical debt" ID="ID_166622719" CREATED="1638724797195" MODIFIED="1638724802783"/>
<node TEXT="..." ID="ID_1009850759" CREATED="1638724807024" MODIFIED="1638724807885"/>
</node>
<<<<<<< HEAD
<node TEXT="theory of regions to identify simplifiable classes?" ID="ID_1283213868" CREATED="1638725320831" MODIFIED="1638725334650">
=======
<node TEXT="cohesion" ID="ID_615933137" CREATED="1638722664320" MODIFIED="1644441140449">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="coupling" ID="ID_927951284" CREATED="1638722667360" MODIFIED="1638722786244"/>
<node TEXT="indirection" ID="ID_510798126" CREATED="1638722690296" MODIFIED="1638722794464"/>
<node TEXT="abstraction" ID="ID_74787527" CREATED="1638722797864" MODIFIED="1638722799689"/>
<node TEXT="single responsibility" ID="ID_1897308023" CREATED="1641831913887" MODIFIED="1641864541156">
<icon BUILTIN="help"/>
<icon BUILTIN="button_ok"/>
<node TEXT="here distinct from &quot;single concern&quot; which might be diluted due to low cohesion in class, i.e. many data representing more than a single concern." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1306838631" CREATED="1641831949644" MODIFIED="1641831997863"/>
<node TEXT="Responsibility is always single, a class is responsible for its invariants (state, corresponding logic) ONLY" ID="ID_288767543" CREATED="1641831921396" MODIFIED="1641832124772"/>
<node TEXT="(correctly) reasoning from class&apos; single responsibility" ID="ID_497463660" CREATED="1641860264789" MODIFIED="1641860287502">
<node TEXT="Interfaces are a way of saying that the class can do something if you provide a suitable object." ID="ID_191991719" CREATED="1641860288789" MODIFIED="1641860354150">
<node TEXT="provide a callback-mechanism to allow signaling back to the *unkonwn* caller that we&apos;ve reached a certain situation" ID="ID_1026340712" CREATED="1641860354876" MODIFIED="1641860363546"/>
<node TEXT="to demand something is done *for* the class in order for the class to fulfil its obligation. Often something like, we&apos;ve transitioned to a certain state and *then* something has to happen, but what needs to happen is outside class&apos; scope." ID="ID_1501231503" CREATED="1641860367676" MODIFIED="1641860409852"/>
<node TEXT="This connects Interface Segregation!" ID="ID_146637914" CREATED="1641860497699" MODIFIED="1641860512680">
<font BOLD="true"/>
</node>
<node TEXT="This depends on Liskov Substitution" ID="ID_1526374267" CREATED="1641860567891" MODIFIED="1641860579664">
<font BOLD="true"/>
</node>
<node TEXT="appropriate demarcation of scope of class -- define up to and including the interface and nothing more than that" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1479233451" CREATED="1641860590994" MODIFIED="1641860620165">
<font BOLD="false"/>
</node>
<node TEXT="this is only needed if the class&apos; function is useless without the (intermediate) callback. Typical pattern is having a state machine and something needs to happen as part of the state transition, in order for a successful (and useful) transition to occur." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1186571599" CREATED="1641860674322" MODIFIED="1641860727355"/>
</node>
<node TEXT="if responsibility (correctly) only goes inwards (maintaining internals), there cannot be a &quot;banana-and-the-monkey-and-the-rest-of-the-jungle&quot; case scenario." ID="ID_518271704" CREATED="1641860439139" MODIFIED="1641860486838">
<node TEXT="low coupling?" ID="ID_1779083974" CREATED="1641860528826" MODIFIED="1641860543180">
<icon BUILTIN="help"/>
<font BOLD="true"/>
</node>
</node>
</node>
</node>
<node TEXT="single concern" ID="ID_472626979" CREATED="1641831917884" MODIFIED="1641864527340">
<icon BUILTIN="help"/>
<icon BUILTIN="button_ok"/>
<node TEXT="here distinct from &quot;single responsibility&quot;" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_627771172" CREATED="1641832007038" MODIFIED="1641832026008"/>
<node TEXT="Concern determined by (inseparable) set of fields representing a single concept. If fields are separable, means there are more concepts and therefore class manages more than a single concept, hence has multiple concerns." ID="ID_1028493460" CREATED="1641832044213" MODIFIED="1641832122272"/>
<node TEXT="single concern is pretty much defined, i.e. minimal, inseparable/partitionable set of fields, i.e. representing a concept that isn&apos;t otherwise divisible." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_255890985" CREATED="1641864549449" MODIFIED="1641864596022"/>
</node>
<node TEXT="theory of regions to identify minimal inseparable state (reliable? viable?)" ID="ID_1977533439" CREATED="1638725338895" MODIFIED="1638725398032"/>
<node TEXT="theory of regions to identify simplifiable classes?" ID="ID_1283213868" CREATED="1638725320831" MODIFIED="1638725334650"/>
>>>>>>> 330177b (left-over notes)
<node TEXT="Theory of regions cannot identify simplicifications that can be applied to optimized implementation, but it can identify unrelated fields just put together in same class." LOCALIZED_STYLE_REF="styles.important" ID="ID_1604848549" CREATED="1638726259279" MODIFIED="1638726319622"/>
</node>
</node>
<node TEXT="analysis rules" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="left" ID="ID_993338005" CREATED="1639353678588" MODIFIED="1639353691316">
<edge COLOR="#7c7c00"/>
<node TEXT="sudo" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_139124570" CREATED="1639355781651" MODIFIED="1644439718272"/>
<node TEXT="algorithm" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_414543359" CREATED="1642450922060" MODIFIED="1642450925181">
<node TEXT="1.) find all fields" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1383615640" CREATED="1639353699365" MODIFIED="1640132924150"/>
<node TEXT="2.) find all privilege levels + mutability for fields" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1520551781" CREATED="1639353712962" MODIFIED="1640132926832">
<node TEXT="private/non-private" ID="ID_585437937" CREATED="1640131724396" MODIFIED="1640131742119">
<node TEXT="package/public is equal for encapsulation as both allow mutability out of control of the class, therefore must enforce invariants. (Invariants are the distinguishing factor for maintaining consistency.)" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_8526548" CREATED="1640131742788" MODIFIED="1640131799708"/>
</node>
<node TEXT="mutable/immutable (e.g. final)" ID="ID_758188603" CREATED="1640131716092" MODIFIED="1640131722035">
<node TEXT="immutable fields pose less/no risk in violating invariants, causing inconcistency" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_632173660" CREATED="1640131804771" MODIFIED="1640131840445"/>
</node>
</node>
<node TEXT="3.) adjust privilege levels in case of unconditional getters/setters" LOCALIZED_STYLE_REF="styles.topic" ID="ID_279162220" CREATED="1639353730649" MODIFIED="1640132929494">
<node TEXT="compensate for (misguided) best-practices" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_788252473" CREATED="1639353750091" MODIFIED="1639355891376"/>
</node>
<node TEXT="4.) use index of field privilege levels to identify privileged access in methods" LOCALIZED_STYLE_REF="styles.topic" ID="ID_794894217" CREATED="1639353767951" MODIFIED="1640132932522">
<node TEXT="no privileged access means method must move out of class" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_504544692" CREATED="1639353798446" MODIFIED="1639355899597"/>
</node>
<node TEXT="5.) use remaining methods to identify cohesion between fields, i.e. are all fields bi-directionally dependent, i.e. inseparable." LOCALIZED_STYLE_REF="styles.topic" ID="ID_1957588145" CREATED="1639353822357" MODIFIED="1640132943445"/>
<node TEXT="6.) identify/extract utility logic from methods" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1335085299" CREATED="1639353863480" MODIFIED="1640132945851"/>
<node TEXT="7.) identify and separate methods that only act on some subset of fields." LOCALIZED_STYLE_REF="styles.topic" ID="ID_1031497298" CREATED="1639355708204" MODIFIED="1640132949353"/>
</node>
<node TEXT="undefined" FOLDED="true" ID="ID_1054210383" CREATED="1640131879892" MODIFIED="1640131882031">
<node TEXT="ignore methods that do not use privileged access" ID="ID_1211427720" CREATED="1640131882836" MODIFIED="1640132002128"/>
<node TEXT="ignore methods that only read privileged data" ID="ID_1257828254" CREATED="1640132002851" MODIFIED="1640132021327">
<icon BUILTIN="help"/>
<node TEXT="no mutation is no risk of inconsistency" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_66559177" CREATED="1640132025931" MODIFIED="1640132043238"/>
<node TEXT="what about risk of exposing privileged data to public?" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_911938755" CREATED="1640132048643" MODIFIED="1640132062915">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="read before write" ID="ID_175000347" CREATED="1640131959788" MODIFIED="1640131965970">
<node TEXT="indicates conditional relationship therefore dependency?" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_432893861" CREATED="1640131968011" MODIFIED="1640131986143">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="writing multiple fields" ID="ID_886247029" CREATED="1640132068622" MODIFIED="1640132079232">
<node TEXT="assume data dependency" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_315989250" CREATED="1640132098738" MODIFIED="1640132115062"/>
</node>
<node TEXT="what to do with privileged method calls?" ID="ID_1213760278" CREATED="1640132080211" MODIFIED="1640132092387">
<node TEXT="perform nested analysis and classify accordingly for repeated use?" ID="ID_839824165" CREATED="1640132142531" MODIFIED="1640132167347">
<icon BUILTIN="help"/>
</node>
</node>
</node>
<node TEXT="dependency-analysis" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_184715729" CREATED="1642292724005" MODIFIED="1642382714170">
<node TEXT="access/use of instance is undetermined: can be changed/accessed/side-effects/pure" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1877449112" CREATED="1642293024175" MODIFIED="1642293113819"/>
<node TEXT="this analysis can likely be more efficient/effective in rust as it has memory-area-wide mutability modifier, as opposed to java/go/etc." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1611768111" CREATED="1642382018874" MODIFIED="1642382059448"/>
<node TEXT="temporal order is significant" ID="ID_1142756210" CREATED="1642292772788" MODIFIED="1642292803526">
<icon BUILTIN="idea"/>
</node>
<node TEXT="access patterns" ID="ID_585957650" CREATED="1642382092803" MODIFIED="1642382100213">
<icon BUILTIN="idea"/>
<node TEXT="repeats" ID="ID_355289186" CREATED="1642382104275" MODIFIED="1642382107463"/>
<node TEXT="paired use" ID="ID_383411753" CREATED="1642382108227" MODIFIED="1642382111703"/>
<node TEXT="alternating use (a-b-a-b)" ID="ID_82592093" CREATED="1642382112204" MODIFIED="1642382137042"/>
<node TEXT="wrapped use (a-b-a)" ID="ID_734302831" CREATED="1642382116995" MODIFIED="1642382132132"/>
<node TEXT="patterns (repeat, alternate, wrapped/start-end, etc." ID="ID_652876534" CREATED="1642381586213" MODIFIED="1642381630878">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="read/write for primitives" ID="ID_894162444" CREATED="1642292818605" MODIFIED="1642292834367">
<icon BUILTIN="idea"/>
<node TEXT="read/dereference/call-methods/access-fields" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_362547036" CREATED="1642381799524" MODIFIED="1642381844511"/>
<node TEXT="primitives" ID="ID_868700754" CREATED="1642292901629" MODIFIED="1642292903131"/>
<node TEXT="instances" ID="ID_244441228" CREATED="1642292903525" MODIFIED="1642292904867"/>
</node>
<node TEXT="immutability" ID="ID_1506074699" CREATED="1642382333283" MODIFIED="1642382340021">
<icon BUILTIN="idea"/>
<node TEXT="`final`" ID="ID_579410315" CREATED="1642292851277" MODIFIED="1642382348975">
<icon BUILTIN="idea"/>
<node TEXT="read/dereference/method-call/field-access" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_356046356" CREATED="1642381849748" MODIFIED="1642381881610"/>
<node TEXT="primitives" ID="ID_1119274712" CREATED="1642292894477" MODIFIED="1642292896683"/>
<node TEXT="instances" ID="ID_1891576715" CREATED="1642292910773" MODIFIED="1642292920526">
<node TEXT="only guarantees not being replaced as a whole" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_386995397" CREATED="1642292921149" MODIFIED="1642292933715"/>
</node>
</node>
<node TEXT="annotations" ID="ID_1529868607" CREATED="1642292870949" MODIFIED="1642382356136">
<icon BUILTIN="idea"/>
<node TEXT="BigInteger" ID="ID_1921194874" CREATED="1642292888412" MODIFIED="1642292890618"/>
<node TEXT="Integer" ID="ID_1537436661" CREATED="1642381512060" MODIFIED="1642381514113"/>
</node>
</node>
<node TEXT="occurs-after" ID="ID_982032053" CREATED="1642292730461" MODIFIED="1642381912953">
<icon BUILTIN="idea"/>
<node TEXT="in single method, b gets accessed after a" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_197946869" CREATED="1642292745389" MODIFIED="1642293885547"/>
<node TEXT="what if b cannot be reached if a in conditional block (throw/return/...)" ID="ID_376040390" CREATED="1642293932239" MODIFIED="1642293958483">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="occurring directions" ID="ID_527970548" CREATED="1642382155794" MODIFIED="1642382178905">
<icon BUILTIN="idea"/>
<node TEXT="indicating strength of the dependency" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_920050235" CREATED="1642382281226" MODIFIED="1642382292671"/>
<node TEXT="ONLY a after b" ID="ID_260779396" CREATED="1642382195018" MODIFIED="1642382200934"/>
<node TEXT="ONLY b after a" ID="ID_661160670" CREATED="1642382201915" MODIFIED="1642382205468"/>
<node TEXT="uni-directional indicates possible nested subclass" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1387374568" CREATED="1642382407763" MODIFIED="1642382426738"/>
<node TEXT="a after b AND b after a" ID="ID_1899147588" CREATED="1642292742332" MODIFIED="1642382194593"/>
<node TEXT="bi-directional may indicate unbreakable dependency without affecting invariants" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_521572320" CREATED="1642382441507" MODIFIED="1642382493639"/>
</node>
<node TEXT="accesses" ID="ID_1538008303" CREATED="1642294052641" MODIFIED="1642382313020">
<icon BUILTIN="idea"/>
<node TEXT="read" ID="ID_1060884009" CREATED="1642294056120" MODIFIED="1642382679368">
<icon BUILTIN="idea"/>
<node TEXT="use" ID="ID_574080656" CREATED="1642294060400" MODIFIED="1642294067305">
<node TEXT="undetermined whether access makes any sort of modification/side-effect" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1168890178" CREATED="1642294070072" MODIFIED="1642294098961"/>
</node>
<node TEXT="method-call" ID="ID_1937686232" CREATED="1642381962210" MODIFIED="1642381964201"/>
<node TEXT="field-access" ID="ID_372333048" CREATED="1642381965035" MODIFIED="1642381967613">
<node TEXT="read (recurse)" ID="ID_591758320" CREATED="1642381972906" MODIFIED="1642381996486"/>
<node TEXT="write" ID="ID_478691577" CREATED="1642381975603" MODIFIED="1642381976289"/>
</node>
</node>
<node TEXT="write" ID="ID_1899302719" CREATED="1642294058539" MODIFIED="1642382683653">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="bi-directional co-location(?) is stronger indicator (higher probability) of dependency" ID="ID_27893202" CREATED="1642293464667" MODIFIED="1642293518951"/>
<node TEXT="occurrences always in same order" ID="ID_1075396699" CREATED="1642293589780" MODIFIED="1642293620484">
<node TEXT="uni-directional dependency - subregion" ID="ID_1309293503" CREATED="1642293620900" MODIFIED="1642293624907"/>
</node>
<node TEXT="assumptions" LOCALIZED_STYLE_REF="styles.important" ID="ID_1888245537" CREATED="1642292974926" MODIFIED="1642293060203">
<node TEXT="there is relevance to the order of field accesses in single method logic" ID="ID_1314251205" CREATED="1642292981014" MODIFIED="1642293413360">
<icon BUILTIN="help"/>
</node>
<node TEXT="logic in method is reasonably focused on single concern" ID="ID_1084586454" CREATED="1642293343618" MODIFIED="1642293417812">
<icon BUILTIN="help"/>
<node TEXT="this definitely is an issue for long methods in large class" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1545344360" CREATED="1642293367882" MODIFIED="1642293398303"/>
</node>
<node TEXT="if one field is in conditional and another follows in either branch, there is a relevant relationship between the two." ID="ID_1559624383" CREATED="1642382537795" MODIFIED="1642382601652">
<icon BUILTIN="help"/>
</node>
</node>
</node>
</node>
<node TEXT="sanity checks" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="left" ID="ID_1763020242" CREATED="1644501041825" MODIFIED="1644501045996">
<edge COLOR="#7c7c00"/>
<node TEXT="The banana-monkey-jungle problem&#xa;indicates bad design (decisions)" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_977587144" CREATED="1644501046775" MODIFIED="1644504317808">
<node ID="ID_1687046382" CREATED="1644501108612" MODIFIED="1644501765138"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>primarily</b>: violation of single responsibility of class
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_728598272" CREATED="1644502158671" MODIFIED="1644502167286"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>primarily</b>: violation of single concern of class
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_1455521118" CREATED="1644501172602" MODIFIED="1644501781954"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>secondarily</b>: min requirements for input,
    </p>
    <p>
      min guarantees for output
    </p>
    <p>
      (indirectly: dependency inversion)
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="resolutions" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_827003357" CREATED="1644501985538" MODIFIED="1644501994371">
<node TEXT="consider carefully if the &quot;single concern&quot; property of the class is violated, if so reduce concern of class by splitting off other concerns" ID="ID_1869455078" CREATED="1644502170183" MODIFIED="1644502208164"/>
<node TEXT="define or select interfaces that are applicable with minimum excess for use in parameters" ID="ID_827403103" CREATED="1644501997654" MODIFIED="1644502023725"/>
<node TEXT="select return types (and other output types) with exactly those properties that you want to guarantee. (Anything not guaranteed cannot interfere with internal changes to implementation.)" ID="ID_1724207664" CREATED="1644502024003" MODIFIED="1644502127323"/>
</node>
</node>
<node TEXT="Overly large and/or complicated class" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1578973327" CREATED="1644504377666" MODIFIED="1644504389159">
<node TEXT="..." ID="ID_1240550972" CREATED="1644504395602" MODIFIED="1644504396676"/>
</node>
</node>
</node>
</map>
