```
TODO:
- check rationale and rules if the match with the theory. Confirm that no mistakes were made and/or misremembering.
- cleanly separated subjective from absolute?
```

# Introduction

In this article, I will focus on software programming and design concerns. Contrary to many books on this subject, I aim to introduce you to rationale, rules and guidelines that are not based on arbitrary numbers for boundaries and subjective "experiences", but rather to natural boundaries inherent in technology and rationale that are based on and work _with_ fundamental theory in computer science. I will try to translate the theoretical basis into pragmatic rules if this is necessary to make the information actionable.

Far too often are we spending time on fixing "violations" of arbitrary rules. Why should a function always be 10 lines or fewer? Some go on to explain that logic is easier to understand if you can read everything on a single screen. However, nowadays there are very high resolution screens, so why stop at 10 instead of going for a full 60 lines? And whichever number you pick, there will always be situations where the value is off by one or two. This leads to frustration and possibly bad code structure and/or style, if you are forced to refactor.

It is very likely that none of the information in the document is new. I am attempting to gather all the necessary relevant information in a single document such that we can establish a base-line for software engineering as a whole. The intention is not to invent "the one true solution to software engineering" but rather to compose a reliable foundation with corresponding rationale and possibly under which conditions to deviate if needed.

To establish a common ground of knowledge to work with, the first sections go into the various aspects of computer science that are somewhat related and are good to have as a basis. None of these topics will be discussed exhaustively. Instead, we will touch on the parts that are immediately relevant. I will try to give an impression of the other aspects, even if they are not discussed.

I will sometimes refer to articles of past events, such as incident resolution or problem solving situations, if they are applicable to a certain topic. These are often about events with some kind of problem, conflict or dichotomy. The examples are there to illustrate a certain situation and highlight the topic and decision making process using a real-life situation.

## Concerns

- OOP, as used by Java/C# and most other modern languages, is misunderstood; not a paradigm.
- Coding guidelines are relying on arbitrary numbers of bounds, results in irrational penalties in code.
- Spaghetti or otherwise mess of statement organization / code structure.
- Missing "best practice" on code structure and organizing.
- Missing practices on quantification and qualification.
- ...

On writing code - programming - in a structured way.

```
TODO:

Sections:

- understanding the core essence of programming
  - variations in programming guarantees (weak/strong typing, static/dynamic typing)
  - The endless search for LEGO-like programming and its weak-nesses
- identifying and measuring software quality
  - arbitrary boundaries and rules
  - clear and/or natural boundaries
- ...

Topics:

- functional programming
- procedural programming
- OOP-structure (object as encapsulation unit, ADT, as in Java/C#/C++/etc.)
- OOP-paradigm (concurrency, objects as thread of execution)
- metrics
```
