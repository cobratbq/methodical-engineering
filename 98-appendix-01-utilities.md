# Appendix: Utilities

Common terms used in managing available space vs reserved capacity (memory) in data types.

- `index`: the current position in a sequence/collection, to indicate target section for current operation.
- `length`/`size`: (resp. sequences/collections) actual size/length currently occupying the data type.
- `capacity`: maximum size currently accounted for, i.e. memory reserved, but may not be fully utilized yet.
- `view` vs fully allocated memory range.
- `partitions`: non-overlapping parts

Qualifications of utilities:

- with-context/contextless
- stateless/stateful

Classes:

- static function consisting of sequence of usage logic
- initializable context with sequence of usage logic (typically calling static functions)
  - benefit: no need to repeat same values every time for establishing the context once
- converters: usage logic operating in the space between two types
  - encoders: from T:any to bytes and vice-versa
  - converters: from T1:any to T2:any with T1 != T2
  

## Sequences of values

Sequences such as arrays, strings of characters.

- `concatenate`: concatenate `two or more values of same type`. (May be in new memory area.)
- `append`: add `second and subsequent value of same type` to end of `first value`. (Primarily, in memory area of first value if capacity allows. Swap values to prepend.)
- `join`: join two or more `values of same type` with specified `separator`.
- `substring`/`subsequence`/`subarray`: ranged cut out of full sequence. (`TODO: typically only view so changes reflected in underlying (backed) full sequence`)
- `split`: break up existing sequence into multiple partitions (mutually-exclusive parts).

## Collections of values

Collections of values of a type, such as abstract collection, set, list, map, and concrete implementations, such as arrays, deques, queues, stacks, lists, maps, etc.

- `extend`: add second and subsequent values of same type to collection. Expect collection to be grown if current capacity is insufficient.
- `shrink`: deliberate action to shrink _capacity_ of collection to match (closer to) actual usage (_length_).

Particular characteristics and assumptions:

- `TODO: views using same underlying/backing sequence memory area, or (partial) copy into new memory area.`
- `TODO: conveniences of automatic (vs manual) memory management.`
- `TODO: allow for automatic reallocation of memory, moving, etc. if capacity does not suffice?`
- `TODO: carefully define matching to capacity or (current) usage.`
- `TODO: what happens if going over capacity? (error or truncation or extension such as reallocation)`

## Handling incomplete/partially successful operations

Expectations of operation:

- Always succeeds. There are no possible failure condition.
- Must succeed. If it fails, abort the application. Further execution cannot be trusted.
- Succeeds or fails. Operation provides workable values in both the successful and the failure case.
- Always fails. Used to flag reaching undesirable case/state. (E.g. unsupported use case.)

Treatment of operation:

- Failure
- Error indicator
- Success (silent)

Results of operation:

- Completed: result fully completed.
- Truncated: result partially completed, partially lost/not executed. (Possibly inconsistent.)
- Undone: No changes. Any partially performed operation is dropped/undone.
